# JRT-Webserver for broadcasting

This image bundles the files needed for a broadcast webserver for use with [Joel Real Timing](http://joel-real-timing.com/index_en.html).    
See http://joel-real-timing.com/broadcast_en.html for more details.

## Changes 
The passwords were removed from the PHP files. Instead the password has to be provided by environment variable JRT_PASSWORD and must match the one in the JRT-Broadcast-settings page.

## How to start
A sample call would look like

```docker run -e JRT_PASSWORD=some-fancy-password mpapenbrock/jrt-web ```
