function str_heure() {
    var dt = new Date();
    hrs=dt.getHours();
    min=dt.getMinutes();
    sec=dt.getSeconds();
    tm=(((hrs<10)?"0":"") +hrs)+":";
    tm+=((min<10)?"0":"")+min+":";
    tm+=((sec<10)?"0":"")+sec;
    return tm;
}

function update_dashboard() {

    //console.log(donnees.typ);

    disp_sel = "_" + advanced["display_selected"];

    if (donnees["expired"] == 1) {
        $("#expired").css("display", "block");
    }

    if (donnees.styp == "Race" && f3_mode_in_race_dashboard == 0) {
        _f3 = "";
        _f3_tag = "";
    } else {
        _f3 = "_f3";
        _f3_tag = ".";
    }

    if (donnees.typ == 31 || donnees.typ == 1) {

        //
        if (advanced["disp_" + "sof" + disp_sel]) {
            if (donnees.sof_me != undefined) {
                document.getElementById("sof").innerHTML = donnees.sof_me;
            }
        }

        if (advanced["disp_" + "drs" + disp_sel]) {
            if ((carname in car_with_drs) && (carname != "formularenault35")) {
                document.getElementById("drs").innerHTML = "DRS";
            }
        }

    }

    if (donnees.typ <= 32) {

        if (donnees.u != undefined) {
            speedfactor = donnees.u == 1 ? 1 : 1 / 1.609344;
            if (donnees.carname == "lotus79" || donnees.carname == "lotus49") {
                fuelfactor = donnees.u == 1 ? 1 : 1 / 4.54609;
            } else {
                fuelfactor = donnees.u == 1 ? 1 : 1 / 3.78541178;
            }
        }

        if (donnees["pre_cc" + _f3] != undefined && advanced["disp_" + "pre_cpos" + disp_sel])
            document.getElementById("pre_cpos").style.color = cc(donnees["pre_cc" + _f3]);
        if (donnees.me_cc != undefined && advanced["disp_" + "me_cpos" + disp_sel])
            document.getElementById("me_cpos").style.color = cc(donnees.me_cc);
        if (donnees["post_cc" + _f3] != undefined && advanced["disp_" + "post_cpos" + disp_sel])
            document.getElementById("post_cpos").style.color = cc(donnees["post_cc" + _f3]);

        cache_pre = 0;
        cache_post = 0;
        /*if (donnees["pre_rc" + _f3] != undefined) {
            if (donnees["pre_rc" + _f3] < 0) {
                cache_pre = 1;
            }
        }
        if (donnees["post_rc" + _f3] != undefined) {
            if (donnees["post_rc" + _f3] > 0) {
                cache_post = 1;
            }
        }*/
        if (donnees.styp == "Race" && f3_mode_in_race_dashboard == 0) {
            if (donnees.pre_rc != undefined)
                if (donnees.pre_rc <= 0) {
                    cache_pre = 1;
                }
            if (donnees.post_rc != undefined)
                if (donnees.post_rc >= 0) {
                    cache_post = 1;
                }
        } else {
            // En dehors des courses, on affiche l'�cart sur la piste
            if (donnees["pre_rcf3" + _f3] != undefined)
                if (donnees["pre_rcf3" + _f3] <= 0) {
                    cache_pre = 1;
                }
            if (donnees["post_rcf3" + _f3] != undefined)
                if (donnees["post_rcf3" + _f3] >= 0) {
                    cache_post = 1;
                }
        }

        //console.log(donnees["pre_rcf3" + _f3])

        if (donnees["pre_eq_me" + _f3] == 1 || cache_pre) {
            donnees["pre_name" + _f3] = "&nbsp";
            donnees["pre_pos" + _f3] = "&nbsp";
            donnees["pre_posb" + _f3] = "&nbsp";
            donnees["pre_cpos" + _f3] = "&nbsp";
            donnees["pre_cposb" + _f3] = "&nbsp";
            donnees["pre_gain" + _f3] = "&nbsp";
            donnees["pre_cgain" + _f3] = "&nbsp";
            donnees["pre_b" + _f3] = 0;
            donnees["pre_l" + _f3] = 0;
            donnees["pre_rc" + _f3] = 0;
            donnees["pre_rcf3" + _f3] = 0;
            donnees["pre_lc" + _f3] = "&nbsp";
            donnees["pre_sti" + _f3] = "&nbsp";
        }
        if (donnees["post_eq_me" + _f3] == 1 || cache_post) {
            donnees["post_name" + _f3] = "&nbsp";
            donnees["post_pos" + _f3] = "&nbsp";
            donnees["post_posb" + _f3] = "&nbsp";
            donnees["post_cpos" + _f3] = "&nbsp";
            donnees["post_cposb" + _f3] = "&nbsp";
            donnees["post_gain" + _f3] = "&nbsp";
            donnees["post_cgain" + _f3] = "&nbsp";
            donnees["post_b" + _f3] = 0;
            donnees["post_l" + _f3] = 0;
            donnees["post_rc" + _f3] = 0;
            donnees["post_rcf3" + _f3] = 0;
            donnees["post_lc" + _f3] = "&nbsp";
            donnees["post_sti" + _f3] = "&nbsp";
        }

        if (advanced["disp_" + "weather" + disp_sel]) {
            document.getElementById("weather").innerHTML = "&nbsp";

            if (donnees.styp == "Race" && donnees.sname != "RACE") {
                document.getElementById("weather").innerHTML += donnees.sname + ", ";
            } else {
                document.getElementById("weather").innerHTML += donnees.styp + ", ";
            }
            document.getElementById("weather").innerHTML += reformat_skies(donnees.skies);
            if (donnees.u == 1) {
                str_speed = (donnees.windspeed * 3.6).toFixed(1) + " km/h";
            } else {
                str_speed = (donnees.windspeed * 3.6 / 1.609344).toFixed(1) + " MPH";
            }
            if ((donnees.airtemp != undefined) && (donnees.tracktemp != undefined) && (donnees.winddir != undefined) && (donnees.humidity != undefined)) {
                if ((temperature_mode != 2) && ((donnees.u == 1 && temperature_mode == 0) || (temperature_mode == 1))) {  // systeme metric ou forc� en Celsius dans les options
                    document.getElementById("weather").innerHTML += " " + "<span style='font-style: italic; font-weight: bold'>" + donnees.airtemp.toFixed(1) + "&degC</span>";
                    document.getElementById("weather").innerHTML += ", tr " + "<span style='font-style: italic; font-weight: bold'>" + donnees.tracktemp.toFixed(1) + "&degC</span>";
                    document.getElementById("weather").innerHTML += ", " + "<span style='font-style: italic; font-weight: bold'>" + reformat_winddir(donnees.winddir / Math.PI * 180) + " " + ((donnees.winddir / Math.PI * 180) % 360).toFixed(0) + "&deg</span>";
                    document.getElementById("weather").innerHTML += " " + "<span style='font-style: italic; font-weight: bold'>" + str_speed + "</span>";
                } else {
                    document.getElementById("weather").innerHTML += " " + "<span style='font-style: italic; font-weight: bold'>" + (donnees.airtemp * 1.8 + 32).toFixed(1) + "&degF</span>";
                    document.getElementById("weather").innerHTML += ", tr " + "<span style='font-style: italic; font-weight: bold'>" + (donnees.tracktemp * 1.8 + 32).toFixed(1) + "&degF</span>";
                    document.getElementById("weather").innerHTML += ", " + "<span style='font-style: italic; font-weight: bold'>" + reformat_winddir(donnees.winddir / Math.PI * 180) + " " + ((donnees.winddir / Math.PI * 180) % 360).toFixed(0) + "&deg</span>";
                    document.getElementById("weather").innerHTML += " " + "<span style='font-style: italic; font-weight: bold'>" + str_speed + "</span>";
                }
                document.getElementById("weather").innerHTML += ", " + "<span style='font-style: italic; font-weight: bold'>" + (donnees.humidity * 100).toFixed(0) + "%</span>";
            }
            //document.getElementById("weather").innerHTML += ", Pressure " + "<span style='font-style: italic; font-weight: bold'>" + donnees.airpress.toFixed(0) + " Hg</span>";
            //document.getElementById("weather").innerHTML += ", Density " + "<span style='font-style: italic; font-weight: bold'>" + donnees.airdens.toFixed(3) + " kg/m<sup>3</sup></span>";
            //document.getElementById("weather").innerHTML += ", Fog " + "<span style='font-style: italic; font-weight: bold'>" + (donnees.fog * 100).toFixed(0) + "%</span>";
        }

        if (advanced["disp_" + "time_of_day" + disp_sel]) {
            if (donnees.tod != undefined)
                document.getElementById("time_of_day").innerHTML = donnees.tod;
            else
                document.getElementById("time_of_day").innerHTML = "--";
        }

        if (donnees.tr != undefined && donnees.state != undefined && advanced["disp_" + "timeremain" + disp_sel]) {
            if (donnees.state >= 4 && donnees.laps_l == 1 && donnees.tr != -1 && donnees.tr != -2 && donnees.tr != -3 && donnees.tr != "unlimited" && donnees.styp == "Race" && donnees.laps != "unlimited") {
                document.getElementById("timeremain").innerHTML = "<span style='font-size: 0.75em; vertical-align: top; top: 25%;'>" + "Lap " + (donnees.lead_lc + 1) + "/" + donnees.laps + "</span>";
            } else {
                document.getElementById("timeremain").innerHTML = reformat_timeremain(donnees.tr);
            }
        }

        //if (donnees.f != undefined && advanced["disp_" + "tank" + disp_sel])
        //    document.getElementById("tank").innerHTML = (fuelfactor * coef_fuel * donnees.f).toFixed(2);


        conso = 0;
        conso1 = donnees.co;
        conso5 = donnees.co5;
        if (donnees.fuel_use_avg != undefined) {
            calcfuel_mode = donnees.fuel_use_avg;
        }

        if (calcfuel_mode == 0) {
            conso = donnees.co;
            fuelneed = donnees.fn;
            text_conso = "(1L)"
        } else {
            conso = donnees.co5;
            fuelneed = donnees.fn5;
            text_conso = "(5L)"
        }
        fuelneed1 = donnees.fn;
        fuelneed5 = donnees.fn5;

        // Si on peut pitter sans risquer de devoir faire un pit de plus on met le fond en mauve
        // Estimation d'abord du nombre de pits restants
        if (fuelneed > 0 && donnees.tcap > 0) {
            p = ((fuelneed - 2*conso) / donnees.tcap) + 1;
            if (p < 0)
                p = 0;
            p = parseFloat(p).toFixed(2);
        } else {
            p = 0;
        }
        if (advanced["disp_" + "fuelneed" + disp_sel]) {
            if (donnees.estim_status == 0) {
                //document.getElementById("fuelneed_bg0").style.backgroundColor = "#999999";
                change_bg("fuelneed_bg0", "#999999", advanced["bg_" + "fuelneed" + "_" + advanced["display_selected"]]);
                //document.getElementById("fuelneed_bg1").style.backgroundColor = "#999999"
                change_bg("fuelneed_bg1", "#0099ff", advanced["bg_" + "fuelneed" + "_" + advanced["display_selected"]]);
            } else {
                //document.getElementById("fuelneed_bg1").style.backgroundColor = "#0099ff";
                change_bg("fuelneed_bg1", "#0099ff", advanced["bg_" + "fuelneed" + "_" + advanced["display_selected"]]);
                if (Math.floor(p) > 0 && fuelneed < donnees.tcap * Math.floor(p) - donnees.f) {
                    //document.getElementById("fuelneed_bg0").style.backgroundColor = "#ff99ff"
                    change_bg("fuelneed_bg0", "#ff99ff", advanced["bg_" + "fuelneed" + "_" + advanced["display_selected"]]);
                } else {
                    //document.getElementById("fuelneed_bg0").style.backgroundColor = "#ff9900"
                    change_bg("fuelneed_bg0", "#ff9900", advanced["bg_" + "fuelneed" + "_" + advanced["display_selected"]]);
                }
            }
        }
        if (fuelneed1 > 0 && donnees.tcap > 0) {
            p = ((fuelneed1 - 2*conso1) / donnees.tcap) + 1;
            if (p < 0)
                p = 0;
            p = parseFloat(p).toFixed(2);
        } else {
            p = 0;
        }
        if (advanced["disp_" + "fuelneed1" + disp_sel]) {
            if (donnees.estim_status == 0) {
                change_bg("fuelneed1_bg0", "#999999", advanced["bg_" + "fuelneed1" + "_" + advanced["display_selected"]]);
                change_bg("fuelneed1_bg1", "#0099ff", advanced["bg_" + "fuelneed1" + "_" + advanced["display_selected"]]);
            } else {
                change_bg("fuelneed1_bg1", "#0099ff", advanced["bg_" + "fuelneed1" + "_" + advanced["display_selected"]]);
                if (Math.floor(p) > 0 && fuelneed1 < donnees.tcap * Math.floor(p) - donnees.f) {
                    change_bg("fuelneed1_bg0", "#ff99ff", advanced["bg_" + "fuelneed1" + "_" + advanced["display_selected"]]);
                } else {
                    change_bg("fuelneed1_bg0", "#ff9900", advanced["bg_" + "fuelneed1" + "_" + advanced["display_selected"]]);
                }
            }
        }
        if (fuelneed5 > 0 && donnees.tcap > 0) {
            p = ((fuelneed5 - 2*conso5) / donnees.tcap) + 1;
            if (p < 0)
                p = 0;
            p = parseFloat(p).toFixed(2);
        } else {
            p = 0;
        }
        if (advanced["disp_" + "fuelneed5" + disp_sel]) {
            if (donnees.estim_status == 0) {
                change_bg("fuelneed5_bg0", "#999999", advanced["bg_" + "fuelneed5" + "_" + advanced["display_selected"]]);
                change_bg("fuelneed5_bg1", "#0099ff", advanced["bg_" + "fuelneed5" + "_" + advanced["display_selected"]]);
            } else {
                change_bg("fuelneed5_bg1", "#0099ff", advanced["bg_" + "fuelneed" + "_" + advanced["display_selected"]]);
                if (Math.floor(p) > 0 && fuelneed5 < donnees.tcap * Math.floor(p) - donnees.f) {
                    change_bg("fuelneed5_bg0", "#ff99ff", advanced["bg_" + "fuelneed5" + "_" + advanced["display_selected"]]);
                } else {
                    change_bg("fuelneed5_bg0", "#ff9900", advanced["bg_" + "fuelneed5" + "_" + advanced["display_selected"]]);
                }
            }
        }

        if (advanced["disp_" + "timeremain" + disp_sel]) {
            if (donnees.estim_status == 2 && donnees.tr >= 0 && donnees.laps_l != 1) {
                document.getElementById("timeremain").style.color = "#333333";
            } else {
                document.getElementById("timeremain").style.color = "#ffffff";
            }
        }

        if (donnees.refuelspeed == 0) {
            donnees.refuelspeed = 1
        }

        if (fuelneed >= 5*donnees.refuelspeed) {
            fuelneed_bg1_pct = 1;
        } else if (fuelneed <= 0) {
            fuelneed_bg1_pct = 0;
        } else {
            fuelneed_bg1_pct = fuelneed / (5 * donnees.refuelspeed);
        }
        if (advanced["disp_" + "fuelneed" + disp_sel]) {
            //set("fuelneed_bg1", 1024, 560 + Math.floor(fuelneed_bg1_pct * 96), 256, 96 - Math.floor(fuelneed_bg1_pct * 96), 0.075);
            set("fuelneed_bg1", advanced["x_" + "fuelneed" + disp_sel], advanced["y_" + "fuelneed" + disp_sel] + Math.floor(fuelneed_bg1_pct * advanced["h_" + "fuelneed" + disp_sel]), Math.floor(advanced["w_" + "fuelneed" + disp_sel]), advanced["h_" + "fuelneed" + disp_sel] - Math.floor(fuelneed_bg1_pct * advanced["h_" + "fuelneed" + disp_sel]), advanced["f_" + "fuelneed" + disp_sel] / dashboard_ref_w);

            if (donnees.estim_status == 0 || donnees.fuel_accurate != 1) {
                document.getElementById("fuelneed").style.color = "#666666"
            } else {
                if (fuelneed_bg1_pct == 0) {
                    document.getElementById("fuelneed").style.color = "#ffffff"
                } else {
                    document.getElementById("fuelneed").style.color = "#000000"
                }
            }
        }

        if (fuelneed1 >= 5*donnees.refuelspeed) {
            fuelneed1_bg1_pct = 1;
        } else if (fuelneed1 <= 0) {
            fuelneed1_bg1_pct = 0;
        } else {
            fuelneed1_bg1_pct = fuelneed1 / (5 * donnees.refuelspeed);
        }
        if (advanced["disp_" + "fuelneed1" + disp_sel]) {
            set("fuelneed1_bg1", advanced["x_" + "fuelneed1" + disp_sel], advanced["y_" + "fuelneed1" + disp_sel] + Math.floor(fuelneed1_bg1_pct * advanced["h_" + "fuelneed1" + disp_sel]), Math.floor(advanced["w_" + "fuelneed1" + disp_sel]), advanced["h_" + "fuelneed1" + disp_sel] - Math.floor(fuelneed1_bg1_pct * advanced["h_" + "fuelneed1" + disp_sel]), advanced["f_" + "fuelneed1" + disp_sel] / dashboard_ref_w);

            if (donnees.estim_status == 0 || donnees.fuel_accurate != 1) {
                document.getElementById("fuelneed1").style.color = "#666666"
            } else {
                if (fuelneed1_bg1_pct == 0) {
                    document.getElementById("fuelneed1").style.color = "#ffffff"
                } else {
                    document.getElementById("fuelneed1").style.color = "#000000"
                }
            }
        }
        if (fuelneed5 >= 5*donnees.refuelspeed) {
            fuelneed5_bg1_pct = 1;
        } else if (fuelneed5 <= 0) {
            fuelneed5_bg1_pct = 0;
        } else {
            fuelneed5_bg1_pct = fuelneed5 / (5 * donnees.refuelspeed);
        }
        if (advanced["disp_" + "fuelneed5" + disp_sel]) {
            set("fuelneed5_bg1", advanced["x_" + "fuelneed5" + disp_sel], advanced["y_" + "fuelneed5" + disp_sel] + Math.floor(fuelneed5_bg1_pct * advanced["h_" + "fuelneed5" + disp_sel]), Math.floor(advanced["w_" + "fuelneed5" + disp_sel]), advanced["h_" + "fuelneed5" + disp_sel] - Math.floor(fuelneed5_bg1_pct * advanced["h_" + "fuelneed5" + disp_sel]), advanced["f_" + "fuelneed5" + disp_sel] / dashboard_ref_w);

            if (donnees.estim_status == 0 || donnees.fuel_accurate != 1) {
                document.getElementById("fuelneed5").style.color = "#666666"
            } else {
                if (fuelneed5_bg1_pct == 0) {
                    document.getElementById("fuelneed5").style.color = "#ffffff"
                } else {
                    document.getElementById("fuelneed5").style.color = "#000000"
                }
            }
        }

        if (conso > 0) {
            // On rajoute les tours de marge
            //fuelneed += fuel_spare_nblaps * conso;

            if (advanced["disp_" + "conso" + disp_sel]) {
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    document.getElementById("conso").innerHTML = (fuelfactor * coef_fuel * conso).toFixed(3) + " " + "<span style='font-weight:normal'>" + text_conso + "</span>";
                    //document.getElementById("conso5").innerHTML = (fuelfactor * coef_fuel * conso5).toFixed(3) + " " + "<span style='font-weight:normal'>(5L)</span>";
                } else {
                    document.getElementById("conso").innerHTML = "buy pro";
                }
                if (donnees.fuel_accurate != 1) {
                    document.getElementById("conso").style.color = "#bbbbbb";
                } else {
                    document.getElementById("conso").style.color = "#ffffff";
                }
            }

            if (advanced["disp_" + "estlaps" + disp_sel]) {
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    //document.getElementById("estlaps").innerHTML = (donnees.f / conso).toFixed(1);
                    document.getElementById("estlaps").innerHTML = donnees.estlaps.toFixed(1);
                } else {
                    document.getElementById("estlaps").innerHTML = "buy pro"
                }
                if (donnees.fuel_accurate != 1) {
                    document.getElementById("estlaps").style.color = "#555555";
                } else {
                    document.getElementById("estlaps").style.color = "#000000";
                }

                estlaps_bg1_pct = donnees.estlaps_bg1_pct;
                //set("estlaps_bg1", 256, 560, Math.floor(256 * estlaps_bg1_pct), 96, 0.075);
                set("estlaps_bg1", advanced["x_" + "estlaps" + disp_sel], advanced["y_" + "estlaps" + disp_sel], Math.floor(advanced["w_" + "estlaps" + disp_sel] * estlaps_bg1_pct), advanced["h_" + "estlaps" + disp_sel], advanced["f_" + "estlaps" + disp_sel] / dashboard_ref_w);
            }

            //if (donnees.f / conso < 2) {
            if (donnees.f_alert == 1) {
                //document.getElementById("estlaps_bg0").style.backgroundColor = "#ee0000";
                change_bg("estlaps_bg0", "#ee0000", advanced["bg_" + "estlaps" + "_" + advanced["display_selected"]]);
                //document.getElementById("tank").style.backgroundColor = "#cc0000";
                change_bg("tank", "#cc0000", advanced["bg_" + "tank" + "_" + advanced["display_selected"]]);
                //document.getElementById("conso").style.backgroundColor = "#880000";
                change_bg("conso", "#880000", advanced["bg_" + "conso" + "_" + advanced["display_selected"]]);
                //document.getElementById("conso5").style.backgroundColor = "#880000";
            } else {
                if (advanced["disp_" + "estlaps" + disp_sel]) {
                    //document.getElementById("estlaps_bg0").style.backgroundColor = "#00aa00";
                    change_bg("estlaps_bg0", "#00aa00", advanced["bg_" + "estlaps" + "_" + advanced["display_selected"]]);
                }
                if (advanced["disp_" + "tank" + disp_sel]) {
                    //document.getElementById("tank").style.backgroundColor = "#008800";
                    change_bg("tank", "#008800", advanced["bg_" + "tank" + "_" + advanced["display_selected"]]);
                }
                if (advanced["disp_" + "conso" + disp_sel]) {
                    //document.getElementById("conso").style.backgroundColor = "#005500";
                    change_bg("conso", "#005500", advanced["bg_" + "conso" + "_" + advanced["display_selected"]]);
                    //document.getElementById("conso5").style.backgroundColor = "#005500";
                }
            }
            if (advanced["disp_" + "nbpits" + disp_sel]) {
                if (p > 1)
                    document.getElementById("nbpits").innerHTML = p + "";
                else
                    document.getElementById("nbpits").innerHTML = p + "";
                if (donnees.fuel_accurate != 1) {
                    document.getElementById("nbpits").style.color = "#666666";
                } else {
                    document.getElementById("nbpits").style.color = "#ffbb00";
                }
            }
        } else {
            if (advanced["disp_" + "conso" + disp_sel]) {
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    document.getElementById("conso").innerHTML = "-- " + "<span style='font-weight:normal'>" + text_conso + "</span>";
                    //document.getElementById("conso5").innerHTML = "-- " + "<span style='font-weight:normal'>(5L)</span>";
                } else {
                    document.getElementById("conso").innerHTML = "buy pro";
                }
            }
            if (advanced["disp_" + "estlaps" + disp_sel]) {
                document.getElementById("estlaps").innerHTML = "--";
            }
            if (advanced["disp_" + "nbpits" + disp_sel]) {
                document.getElementById("nbpits").innerHTML = "--";
            }
        }

        if (advanced["disp_" + "lapsremain" + disp_sel]) {
            if (donnees.lr != undefined) {
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    document.getElementById("lapsremain").innerHTML = reformat_lapsremain(donnees.lr);
                } else {
                    document.getElementById("lapsremain").innerHTML = "buy pro";
                }
                /*if (donnees.fuel_accurate != 1) {
                    document.getElementById("lapsremain").style.color = "#666666";
                } else {
                    document.getElementById("lapsremain").style.color = "#ff9900";
                }*/
            }
            if (donnees.lapsremain_bg1_pct != undefined) {
                lapsremain_bg1_pct = donnees.lapsremain_bg1_pct;
                set("lapsremain_bg1", advanced["x_" + "lapsremain" + disp_sel], advanced["y_" + "lapsremain" + disp_sel], Math.floor(advanced["w_" + "lapsremain" + disp_sel] * lapsremain_bg1_pct), advanced["h_" + "lapsremain" + disp_sel], advanced["f_" + "lapsremain" + disp_sel] / dashboard_ref_w);
            }
            //donnees.gap_pct_lastlap = 0.001
            if (donnees.gap_pct_lastlap != undefined && donnees.lead_lap != undefined) {
                gap_pct_lastlap = donnees.gap_pct_lastlap;
                //gap_pct_lastlap = 0.0001
                tmp_x = Math.floor(advanced["w_" + "lapsremain" + disp_sel] * gap_pct_lastlap);
                // On �vite de sortir du cadre sinon la gold barre devient invisible
                if (tmp_x > Math.floor(advanced["w_" + "lapsremain" + disp_sel] - 2)) {
                    tmp_x = Math.floor(advanced["w_" + "lapsremain" + disp_sel] - 2)
                }
                if (tmp_x < 0) {
                    tmp_x = 0;
                }
                //tmp_x = 128
                //console.log(tmp_x)
                set("lapsremain_bg2", advanced["x_" + "lapsremain" + disp_sel] + tmp_x, advanced["y_" + "lapsremain" + disp_sel], 1, advanced["h_" + "lapsremain" + disp_sel], advanced["f_" + "lapsremain" + disp_sel] / dashboard_ref_w);
                //if (gap_pct_lastlap == 0) {
                    //document.getElementById("lapsremain_bg2").style.width = 0 + "px";
                //} else {
                    document.getElementById("lapsremain_bg2").style.width = 2 + "px";
                //}
                // Si on doit finir dans le m�me tour que le leader alors on n'affiche la gold line d'une autre couleur
                if (donnees.lead_lap == 1) {
                    document.getElementById("lapsremain_bg2").style.backgroundColor = "#0088ff";
                } else {
                    document.getElementById("lapsremain_bg2").style.backgroundColor = "#ffd700";
                }
            }
        }

        if (advanced["disp_" + "fuelneed" + disp_sel]) {
            if (conso > 0.1)
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    if (fuelfactor * coef_fuel * fuelneed > 9999) {
                        document.getElementById("fuelneed").innerHTML = "9999";
                    } else if (fuelfactor * coef_fuel * fuelneed > 999) {
                        document.getElementById("fuelneed").innerHTML = (fuelfactor * coef_fuel * fuelneed).toFixed(0);
                    } else {
                        document.getElementById("fuelneed").innerHTML = (fuelfactor * coef_fuel * fuelneed).toFixed(1);
                    }
                } else {
                    document.getElementById("fuelneed").innerHTML = "buy pro";
                }
            else
                document.getElementById("fuelneed").innerHTML = "--";
        }
        if (advanced["disp_" + "fuelneed1" + disp_sel]) {
            if (conso1 > 0.1)
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    if (fuelfactor * coef_fuel * fuelneed1 > 9999) {
                        document.getElementById("fuelneed1").innerHTML = "9999";
                    } else if (fuelfactor * coef_fuel * fuelneed1 > 999) {
                        document.getElementById("fuelneed1").innerHTML = (fuelfactor * coef_fuel * fuelneed1).toFixed(0);
                    } else {
                        document.getElementById("fuelneed1").innerHTML = (fuelfactor * coef_fuel * fuelneed1).toFixed(1);
                    }
                } else {
                    document.getElementById("fuelneed1").innerHTML = "buy pro";
                }
            else
                document.getElementById("fuelneed1").innerHTML = "--";
        }
        if (advanced["disp_" + "fuelneed5" + disp_sel]) {
            if (conso5 > 0.1)
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    if (fuelfactor * coef_fuel * fuelneed5 > 9999) {
                        document.getElementById("fuelneed5").innerHTML = "9999";
                    } else if (fuelfactor * coef_fuel * fuelneed5 > 999) {
                        document.getElementById("fuelneed5").innerHTML = (fuelfactor * coef_fuel * fuelneed5).toFixed(0);
                    } else {
                        document.getElementById("fuelneed5").innerHTML = (fuelfactor * coef_fuel * fuelneed5).toFixed(1);
                    }
                } else {
                    document.getElementById("fuelneed5").innerHTML = "buy pro"
                }
            else
                document.getElementById("fuelneed5").innerHTML = "--";
        }
        if (advanced["disp_" + "refuel_min" + disp_sel]) {
            if (conso > 0.1)
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    if (refuel_min > 0) {
                        document.getElementById("refuel_min").innerHTML = (fuelfactor * coef_fuel * refuel_min).toFixed(1);
                    } else {
                        document.getElementById("refuel_min").innerHTML = "--";
                    }
                } else {
                    document.getElementById("refuel_min").innerHTML = "buy pro";
                }
            else
                document.getElementById("refuel_min").innerHTML = "--";
        }
        if (advanced["disp_" + "refuel_avg" + disp_sel]) {
            if (conso > 0.1)
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    if (refuel_avg > 0) {
                        document.getElementById("refuel_avg").innerHTML = (fuelfactor * coef_fuel * refuel_avg).toFixed(1);
                    } else {
                        document.getElementById("refuel_avg").innerHTML = "--";
                    }
                } else {
                    document.getElementById("refuel_avg").innerHTML = "buy pro";
                }
            else
                document.getElementById("refuel_avg").innerHTML = "--";
        }
        if (advanced["disp_" + "refuel_avg_now" + disp_sel]) {
            if (conso > 0.1)
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    if (refuel_avg_now > 0) {
                        document.getElementById("refuel_avg_now").innerHTML = (fuelfactor * coef_fuel * refuel_avg_now).toFixed(1);
                    } else {
                        document.getElementById("refuel_avg_now").innerHTML = "--";
                    }
                } else {
                    document.getElementById("refuel_avg_now").innerHTML = "buy pro";
                }
            else
                document.getElementById("refuel_avg_now").innerHTML = "--";
        }

        if (advanced["disp_" + "oil" + disp_sel] || advanced["disp_" + "water" + disp_sel]) {
            if (donnees.oil != undefined && donnees.w != undefined) {
                if ((temperature_mode != 2) && ((donnees.u == 1 && temperature_mode == 0) || (temperature_mode == 1))) {  // systeme metric ou forc� en Celsius dans les options
                    //document.getElementById("oil").innerHTML = "O " + "<span style='color:#ffffff'>" + donnees.oil.toFixed(1) + "</span>&degC";
                    //document.getElementById("water").innerHTML = "W " + "<span style='color:#ffffff'>"+ donnees.w.toFixed(1) + "</span>&degC";
                    document.getElementById("oil").innerHTML = "O " + donnees.oil.toFixed(1) + "&degC";
                    document.getElementById("water").innerHTML = "W " + donnees.w.toFixed(1) + "&degC";
                } else {
                    //document.getElementById("oil").innerHTML = "O " + "<span style='color:#ffffff'>"+ (donnees.oil * 1.8 + 32).toFixed(1) + "</span>&degF";
                    //document.getElementById("water").innerHTML = "W " + "<span style='color:#ffffff'>"+ (donnees.w * 1.8 + 32).toFixed(1) + "</span>&degF";
                    document.getElementById("oil").innerHTML = "O " + (donnees.oil * 1.8 + 32).toFixed(1) + "&degF";
                    document.getElementById("water").innerHTML = "W " + (donnees.w * 1.8 + 32).toFixed(1) + "&degF";
                }
            }
        }

        if (donnees.styp == "Race") {
            if (advanced["disp_" + "pre_pos" + disp_sel] || advanced["disp_" + "me_pos" + disp_sel] || advanced["disp_" + "post_pos" + disp_sel] || advanced["disp_" + "pre_cpos" + disp_sel] || advanced["disp_" + "me_cpos" + disp_sel] || advanced["disp_" + "post_cpos" + disp_sel]) {
                if (donnees["pre_pos" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                    document.getElementById("pre_pos").innerHTML = donnees["pre_pos" + _f3] + ".";
                else
                    document.getElementById("pre_pos").innerHTML = "";
                if (donnees.me_pos != undefined)
                    document.getElementById("me_pos").innerHTML = donnees.me_pos + ".";
                if (donnees["post_pos" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                    document.getElementById("post_pos").innerHTML = donnees["post_pos" + _f3] + ".";
                else
                    document.getElementById("post_pos").innerHTML = "";
                if (donnees["pre_cpos" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                    document.getElementById("pre_cpos").innerHTML = donnees["pre_cpos" + _f3] + ".";
                else
                    document.getElementById("pre_cpos").innerHTML = "";
                if (donnees.me_cpos != undefined)
                    document.getElementById("me_cpos").innerHTML = donnees.me_cpos + ".";
                if (donnees["post_cpos" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                    document.getElementById("post_cpos").innerHTML = donnees["post_cpos" + _f3] + ".";
                else
                    document.getElementById("post_cpos").innerHTML = "";
            }

            if (advanced["disp_" + "pre_gain" + disp_sel] || advanced["disp_" + "me_gain" + disp_sel] || advanced["disp_" + "post_gain" + disp_sel] || advanced["disp_" + "pre_cgain" + disp_sel] || advanced["disp_" + "me_cgain" + disp_sel] || advanced["disp_" + "post_cgain" + disp_sel]) {
                if (donnees["pre_gain" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                    document.getElementById("pre_gain").innerHTML = reformat_gain(donnees["pre_gain" + _f3]);
                else
                    document.getElementById("pre_gain").innerHTML = "";
                if (donnees.me_gain != undefined)
                    document.getElementById("me_gain").innerHTML = reformat_gain(donnees.me_gain);
                if (donnees["post_gain" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                    document.getElementById("post_gain").innerHTML = reformat_gain(donnees["post_gain" + _f3]);
                else
                    document.getElementById("post_gain").innerHTML = "";
                if (donnees["pre_cgain" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                    document.getElementById("pre_cgain").innerHTML = reformat_gain(donnees["pre_cgain" + _f3]);
                else
                    document.getElementById("pre_cgain").innerHTML = "";
                if (donnees.me_cgain != undefined)
                    document.getElementById("me_cgain").innerHTML = reformat_gain(donnees.me_cgain);
                if (donnees["post_cgain" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                    document.getElementById("post_cgain").innerHTML = reformat_gain(donnees["post_cgain" + _f3]);
                else
                    document.getElementById("post_cgain").innerHTML = "";
            }
        } else {
            if (advanced["disp_" + "pre_pos" + disp_sel] || advanced["disp_" + "me_pos" + disp_sel] || advanced["disp_" + "post_pos" + disp_sel] || advanced["disp_" + "pre_cpos" + disp_sel] || advanced["disp_" + "me_cpos" + disp_sel] || advanced["disp_" + "post_cpos" + disp_sel]) {
                // En dehors des courses, on affiche le classement en fonction du meilleur temps
                if (donnees["pre_pos" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                    document.getElementById("pre_pos").innerHTML = donnees["pre_posb" + _f3] + ".";
                else
                    document.getElementById("pre_pos").innerHTML = "";
                if (donnees.me_pos != undefined)
                    document.getElementById("me_pos").innerHTML = donnees.me_posb + ".";
                if (donnees["post_pos" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                    document.getElementById("post_pos").innerHTML = donnees["post_posb" + _f3] + ".";
                else
                    document.getElementById("post_pos").innerHTML = "";
                if (donnees["pre_cpos" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                    document.getElementById("pre_cpos").innerHTML = donnees["pre_cposb" + _f3] + ".";
                else
                    document.getElementById("pre_cpos").innerHTML = "";
                if (donnees.me_cpos != undefined)
                    document.getElementById("me_cpos").innerHTML = donnees.me_cposb + ".";
                if (donnees["post_cpos" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                    document.getElementById("post_cpos").innerHTML = donnees["post_cposb" + _f3] + ".";
                else
                    document.getElementById("post_cpos").innerHTML = "";
            }

            if (advanced["disp_" + "pre_gain" + disp_sel] || advanced["disp_" + "me_gain" + disp_sel] || advanced["disp_" + "post_gain" + disp_sel] || advanced["disp_" + "pre_cgain" + disp_sel] || advanced["disp_" + "me_cgain" + disp_sel] || advanced["disp_" + "post_cgain" + disp_sel]) {
                // En dehors des courses, on n'affiche pas le gain des positions
                document.getElementById("pre_gain").innerHTML = "&nbsp";
                document.getElementById("me_gain").innerHTML = "&nbsp";
                document.getElementById("post_gain").innerHTML = "&nbsp";
                document.getElementById("pre_cgain").innerHTML = "&nbsp";
                document.getElementById("me_cgain").innerHTML = "&nbsp";
                document.getElementById("post_cgain").innerHTML = "&nbsp";
            }
        }

        if (advanced["disp_" + "pre_best" + disp_sel] || advanced["disp_" + "pre_last" + disp_sel] || advanced["disp_" + "me_best" + disp_sel] || advanced["disp_" + "me_last" + disp_sel] || advanced["disp_" + "post_best" + disp_sel] || advanced["disp_" + "post_last" + disp_sel]) {
            if (donnees["pre_b" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                document.getElementById("pre_best").innerHTML = reformat_laptime(donnees["pre_b" + _f3]);
            else
                document.getElementById("pre_best").innerHTML = "";
            if (donnees["pre_l" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                document.getElementById("pre_last").innerHTML = reformat_laptime(donnees["pre_l" + _f3]);
            else
                document.getElementById("pre_last").innerHTML = "";
            if (donnees.me_b != undefined)
                document.getElementById("me_best").innerHTML = reformat_laptime(donnees.me_b);
            if (donnees.me_l != undefined)
                document.getElementById("me_last").innerHTML = reformat_laptime(donnees.me_l);
            if (donnees["post_b" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                document.getElementById("post_best").innerHTML = reformat_laptime(donnees["post_b" + _f3]);
            else
                document.getElementById("post_best").innerHTML = "";
            if (donnees["post_l" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                document.getElementById("post_last").innerHTML = reformat_laptime(donnees["post_l" + _f3]);
            else
                document.getElementById("post_last").innerHTML = "";
        }

        if (advanced["disp_" + "pre_stint" + disp_sel] || advanced["disp_" + "me_stint" + disp_sel] || advanced["disp_" + "post_stint" + disp_sel]) {
            if (donnees["pre_sti" + _f3] != undefined && donnees_new["pre_rc" + _f3] != undefined)
                document.getElementById("pre_stint").innerHTML = donnees["pre_sti" + _f3];
            else
                document.getElementById("pre_stint").innerHTML = "";
            if (donnees.me_sti != undefined)
                document.getElementById("me_stint").innerHTML = donnees.me_sti;
            if (donnees["post_sti" + _f3] != undefined && donnees_new["post_rc" + _f3] != undefined)
                document.getElementById("post_stint").innerHTML = donnees["post_sti" + _f3];
            else
                document.getElementById("post_stint").innerHTML = "";
        }

        if (donnees.me_lc != undefined && advanced["disp_" + "me_lc" + disp_sel])
            document.getElementById("me_lc").innerHTML = donnees.me_lc ;

        if (donnees["pre_name" + _f3] != undefined && advanced["disp_" + "pre_name" + disp_sel] && donnees_new["pre_rc" + _f3] != undefined)
            document.getElementById("pre_name").innerHTML = "&nbsp" + _f3_tag + donnees["pre_name" + _f3];
        else
            document.getElementById("pre_name").innerHTML = "&nbsp";
        if (donnees["post_name" + _f3] != undefined && advanced["disp_" + "post_name" + disp_sel] && donnees_new["post_rc" + _f3] != undefined)
            document.getElementById("post_name").innerHTML = "&nbsp" + _f3_tag + donnees["post_name" + _f3];
        else
            document.getElementById("post_name").innerHTML = "&nbsp";

        if (donnees.isontrack != 1 || donnees.cts > 2 || advanced["pitbox_bar_on"] == 0) {  // Si on est pas dans les pits on n'affiche pas l'indicateur de pits
            document.getElementById("pitbar8").style.display = "none";
            document.getElementById("pitbar16").style.display = "none";
            document.getElementById("pitbar32").style.display = "none";
            document.getElementById("pitbar64").style.display = "none";
        } else {
            document.getElementById("pitbar8").style.display = "block";
            document.getElementById("pitbar16").style.display = "block";
            document.getElementById("pitbar32").style.display = "block";
            document.getElementById("pitbar64").style.display = "block";
        }

        if (donnees.f_sf != undefined && donnees.lr != undefined && donnees.lr != 0 && advanced["disp_" + "target_conso" + disp_sel])
            document.getElementById("target_conso").innerHTML = (fuelfactor * coef_fuel * donnees.f_sf / (Math.floor(donnees.lr) + 1)).toFixed(3);

        if (donnees.f_sf != undefined && donnees.lr != undefined && advanced["disp_" + "fuel_end" + disp_sel])
            document.getElementById("fuel_end").innerHTML = (fuelfactor * coef_fuel * (donnees.f_sf - conso * (Math.floor(donnees.lr) + 1))).toFixed(1);

        if (advanced["disp_" + "time" + disp_sel]) {
            document.getElementById("time").innerHTML = str_heure();
        }

        if (advanced["disp_" + "perfs" + disp_sel]) {
            if (donnees.pss != undefined) {
                document.getElementById("perfs_dist").innerHTML = donnees.pss;
            }
            if (donnees.p100 != undefined && donnees.p400 != undefined && donnees.p1000 != undefined) {
                document.getElementById("perfs_100kmh").innerHTML = donnees.p100.toFixed(1);
                document.getElementById("perfs_400m").innerHTML = donnees.p400.toFixed(1);
                document.getElementById("perfs_1000m").innerHTML = donnees.p1000.toFixed(1);
                if (donnees.p100d != undefined && donnees.p400s != undefined && donnees.p1000s != undefined) {
                    document.getElementById("perfs_100kmh_dist").innerHTML = donnees.p100d.toFixed(0) + " m";
                    if (speedfactor == 1) {
                        tmp_unit = " km/h";
                    } else {
                        tmp_unit = " MPH";
                    }
                    document.getElementById("perfs_400m_speed").innerHTML = (speedfactor * donnees.p400s * 3.6).toFixed(0) + tmp_unit;
                    document.getElementById("perfs_1000m_speed").innerHTML = (speedfactor * donnees.p1000s * 3.6).toFixed(0) + tmp_unit;
                }
            }
        }

        if (advanced["disp_" + "inc" + disp_sel]) {
            if (donnees.inc_limit == "unlimited") {
                document.getElementById("inc").innerHTML = donnees.inc + " / " + "&infin;";
            } else {
                document.getElementById("inc").innerHTML = donnees.inc + " / " + donnees.inc_limit;
            }
        }
        if (advanced["disp_" + "nextpittimelost" + disp_sel]) {
            if (donnees.plost != undefined) {
                if (donnees.pro_v == 1 || donnees.try_v == 1) {
                    document.getElementById("nextpittimelost").innerHTML = (donnees.plost).toFixed(1) + "s";
                } else {
                    document.getElementById("nextpittimelost").innerHTML = "buy pro";
                }
            }
        }

        if (advanced["disp_" + "conso1" + disp_sel]) {
            if (donnees.pro_v == 1 || donnees.try_v == 1) {
                document.getElementById("conso1").innerHTML = (fuelfactor * coef_fuel * donnees.co).toFixed(3);
            } else {
                document.getElementById("conso1").innerHTML = "buy pro";
            }
            if (donnees.fuel_accurate != 1) {
                document.getElementById("conso1").style.color = "#bbbbbb";
            } else {
                document.getElementById("conso1").style.color = "#ffffff";
            }
        }
        if (advanced["disp_" + "conso5" + disp_sel]) {
            if (donnees.pro_v == 1 || donnees.try_v == 1) {
                document.getElementById("conso5").innerHTML = (fuelfactor * coef_fuel * donnees.co5).toFixed(3);
            } else {
                document.getElementById("conso5").innerHTML = "buy pro";
            }
            if (donnees.fuel_accurate != 1) {
                document.getElementById("conso5").style.color = "#bbbbbb";
            } else {
                document.getElementById("conso5").style.color = "#ffffff";
            }
        }
        if (advanced["disp_" + "points" + disp_sel]) {
            if (donnees.pts_me != undefined) {
                if (donnees.styp == "Race") {
                    document.getElementById("points").innerHTML = donnees.pts_me;
                }else {
                    document.getElementById("points").innerHTML = "--";
                }
            }
        }
        if (advanced["disp_" + "delta_avg" + disp_sel]) {
            if (donnees.d_a != undefined)
                document.getElementById("delta_avg").innerHTML = reformat_delta(donnees.d_a);
            else
                document.getElementById("delta_avg").innerHTML = "--";
        }
        if (advanced["disp_" + "delta_tot" + disp_sel]) {
            if (donnees.d_tot != undefined && donnees.st_avg != undefined && donnees.st_ref != undefined && donnees.tot_ref != undefined) {
                if (donnees.new_tot) {
                    tot_color = "#ff00ff";
                } else {
                    tot_color = "#000000";
                }
                document.getElementById("delta_tot").innerHTML = reformat_delta(donnees.d_tot) + " <span style='vertical-align: top; color:#0088ff'>" + donnees.st_avg + "/" + donnees.st_ref + "</span> <span style='vertical-align: top; font-size:75%;color:" + tot_color + ";'>" + donnees.tot_ref + "</span>";
            } else {
                document.getElementById("delta_tot").innerHTML = "&nbsp;";
            }
        }

        if (advanced["disp_" + "traffic" + disp_sel]) {
            if (donnees.catch_0 != undefined && donnees.catch_1 != undefined && donnees.catch_2 != undefined) {
                c0 = donnees.catch_0;
                c1 = donnees.catch_1;
                c2 = donnees.catch_2;
                if (donnees.cc_0.slice(0,1) == "+") ccs_0 = "";
                else ccs_0 = "<span style='color: #ff0000; vertical-align: top; '>'</span>";
                if (donnees.cc_1.slice(0,1) == "+") ccs_1 = "";
                else ccs_1 = "<span style='color: #ff0000; vertical-align: top; '>'</span>";
                if (donnees.cc_2.slice(0,1) == "+") ccs_2 = "";
                else ccs_2 = "<span style='color: #ff0000; vertical-align: top; '>'</span>";
                if (c0 < 10) c0 = c0.toFixed(1);
                if (c1 < 10) c1 = c1.toFixed(1);
                if (c2 < 10) c2 = c2.toFixed(1);
                if (c0 == 9999) c0 = ".";
                if (c1 == 9999) c1 = ".";
                if (c2 == 9999) c2 = ".";
                document.getElementById("traffic").innerHTML = "<span class='shadow' style='color: " + donnees.cc_0.slice(1,8) + "; vertical-align: top; font-size:100%'> " + c0 + ccs_0 + "</span><span class='shadow' style='opacity:1; color: " + donnees.cc_1.slice(1,8) + "; vertical-align: top; font-size:75%'> " + c1 + ccs_1 + "</span><span class='shadow' style='opacity: 1; color: " + donnees.cc_2.slice(1,8) + "; vertical-align: top; font-size:50%'> " + c2 + ccs_2 + "</span>";
            }
        }
        if (advanced["disp_" + "traffic_pit" + disp_sel]) {
            if (donnees.catchpit_0 != undefined && donnees.catchpit_1 != undefined && donnees.catchpit_2 != undefined) {
                c0 = donnees.catchpit_0;
                c1 = donnees.catchpit_1;
                c2 = donnees.catchpit_2;
                if (donnees.ccpit_0.slice(0,1) == "+") ccs_0 = "";
                else ccs_0 = "<span style='color: #ff0000; vertical-align: top; '>'</span>";
                if (donnees.ccpit_1.slice(0,1) == "+") ccs_1 = "";
                else ccs_1 = "<span style='color: #ff0000; vertical-align: top; '>'</span>";
                if (donnees.ccpit_2.slice(0,1) == "+") ccs_2 = "";
                else ccs_2 = "<span style='color: #ff0000; vertical-align: top; '>'</span>";
                if (c0 < 10) c0 = c0.toFixed(1);
                if (c1 < 10) c1 = c1.toFixed(1);
                if (c2 < 10) c2 = c2.toFixed(1);
                if (c0 == 9999) c0 = ".";
                if (c1 == 9999) c1 = ".";
                if (c2 == 9999) c2 = ".";
                document.getElementById("traffic_pit").innerHTML = "<span class='shadow' style='color: " + donnees.ccpit_0.slice(1,8) + "; vertical-align: top; font-size:100%'> " + c0 + ccs_0 + "</span><span class='shadow' style='opacity:1; color: " + donnees.ccpit_1.slice(1,8) + "; vertical-align: top; font-size:75%'> " + c1 + ccs_1 + "</span><span class='shadow' style='opacity:1; color: " + donnees.ccpit_2.slice(1,8) + "; vertical-align: top; font-size:50%'> " + c2 + ccs_2 + "</span>";
            }
        }

        if (advanced["disp_" + "regen_lap" + disp_sel]) {
            if (donnees.regen_lap != undefined) {
                signe = "";
                if (donnees.regen_lap >= 0) {
                    signe = "+";
                }
                document.getElementById("regen_lap").innerHTML = signe + donnees.regen_lap.toFixed(1);
                if (donnees.regen_status != regen_status_old) {
                    if (donnees.regen_status == 1) {
                        change_bg("regen_lap", "#ffffff", advanced["bg_" + "regen_lap" + "_" + advanced["display_selected"]]);
                        document.getElementById("regen_lap").style.color = "#ff0000";
                    } else {
                        change_bg("regen_lap", "#999999", advanced["bg_" + "regen_lap" + "_" + advanced["display_selected"]]);
                        document.getElementById("regen_lap").style.color = "#ff00ff";
                    }
                }
                regen_status_old = donnees.regen_status;
            }
        }

        if (advanced["disp_" + "regen_turn" + disp_sel]) {
            if (donnees.regen_turn != undefined) {
                signe = "";
                if (donnees.regen_turn >= 0) {
                    signe = "+";
                }
                document.getElementById("regen_turn").innerHTML = signe + donnees.regen_turn.toFixed(1);
            }
        }

    }


    if (donnees.typ <= 33) {

        conso = 0;
        conso1 = donnees.co;
        conso5 = donnees.co5;
        if (donnees.fuel_use_avg != undefined) {
            calcfuel_mode = donnees.fuel_use_avg;
        }

        if (calcfuel_mode == 0) {
            conso = donnees.co;
            fuelneed = donnees.fn;
        } else {
            conso = donnees.co5;
            fuelneed = donnees.fn5;
        }
        fuelneed1 = donnees.fn;
        fuelneed5 = donnees.fn5;

        refuel_min = donnees.rf_m;
        refuel_avg = donnees.rf_a;
        refuel_avg_now = donnees.rf_an;

        if (donnees.refuelspeed == 0) {
            donnees.refuelspeed = 1
        }
        if (fuelneed >= 5*donnees.refuelspeed) {
            fuelneed_bg1_pct = 1;
        } else if (fuelneed <= 0) {
            fuelneed_bg1_pct = 0;
        } else {
            fuelneed_bg1_pct = fuelneed / (5 * donnees.refuelspeed);
        }

        if (fuelneed1 >= 5*donnees.refuelspeed) {
            fuelneed1_bg1_pct = 1;
        } else if (fuelneed1 <= 0) {
            fuelneed1_bg1_pct = 0;
        } else {
            fuelneed1_bg1_pct = fuelneed1 / (5 * donnees.refuelspeed);
        }

        if (fuelneed5 >= 5*donnees.refuelspeed) {
            fuelneed5_bg1_pct = 1;
        } else if (fuelneed5 <= 0) {
            fuelneed5_bg1_pct = 0;
        } else {
            fuelneed5_bg1_pct = fuelneed5 / (5 * donnees.refuelspeed);
        }

        if (donnees.f != undefined && advanced["disp_" + "tank" + disp_sel]) {
            document.getElementById("tank").innerHTML = (fuelfactor * coef_fuel * donnees.f).toFixed(2);
            if (donnees.fuel_accurate != 1) {
                document.getElementById("tank").style.color = "#bbbbbb";
            } else {
                document.getElementById("tank").style.color = "#ffffff";
            }
        }

        if (advanced["disp_" + "fuelneed" + disp_sel]) {
            //set("fuelneed_bg1", 1024, 560 + Math.floor(fuelneed_bg1_pct * 96), 256, 96 - Math.floor(fuelneed_bg1_pct * 96), 0.075);
            set("fuelneed_bg1", advanced["x_" + "fuelneed" + disp_sel], advanced["y_" + "fuelneed" + disp_sel] + Math.floor(fuelneed_bg1_pct * advanced["h_" + "fuelneed" + disp_sel]), Math.floor(advanced["w_" + "fuelneed" + disp_sel]), advanced["h_" + "fuelneed" + disp_sel] - Math.floor(fuelneed_bg1_pct * advanced["h_" + "fuelneed" + disp_sel]), advanced["f_" + "fuelneed" + disp_sel] / dashboard_ref_w);

            if (donnees.estim_status == 0 || donnees.fuel_accurate != 1) {
                document.getElementById("fuelneed").style.color = "#666666"
            } else {
                if (fuelneed_bg1_pct == 0) {
                    document.getElementById("fuelneed").style.color = "#ffffff"
                } else {
                    document.getElementById("fuelneed").style.color = "#000000"
                }
            }
        }

        if (advanced["disp_" + "fuelneed1" + disp_sel]) {
            set("fuelneed1_bg1", advanced["x_" + "fuelneed1" + disp_sel], advanced["y_" + "fuelneed1" + disp_sel] + Math.floor(fuelneed1_bg1_pct * advanced["h_" + "fuelneed1" + disp_sel]), Math.floor(advanced["w_" + "fuelneed1" + disp_sel]), advanced["h_" + "fuelneed1" + disp_sel] - Math.floor(fuelneed1_bg1_pct * advanced["h_" + "fuelneed1" + disp_sel]), advanced["f_" + "fuelneed1" + disp_sel] / dashboard_ref_w);

            if (donnees.estim_status == 0 || donnees.fuel_accurate != 1) {
                document.getElementById("fuelneed1").style.color = "#666666"
            } else {
                if (fuelneed1_bg1_pct == 0) {
                    document.getElementById("fuelneed1").style.color = "#ffffff"
                } else {
                    document.getElementById("fuelneed1").style.color = "#000000"
                }
            }
        }

        if (donnees.pro_v == 1 || donnees.try_v == 1) {
            if (advanced["disp_" + "fuelneed5" + disp_sel]) {
                set("fuelneed5_bg1", advanced["x_" + "fuelneed5" + disp_sel], advanced["y_" + "fuelneed5" + disp_sel] + Math.floor(fuelneed5_bg1_pct * advanced["h_" + "fuelneed5" + disp_sel]), Math.floor(advanced["w_" + "fuelneed5" + disp_sel]), advanced["h_" + "fuelneed5" + disp_sel] - Math.floor(fuelneed5_bg1_pct * advanced["h_" + "fuelneed5" + disp_sel]), advanced["f_" + "fuelneed5" + disp_sel] / dashboard_ref_w);

                if (donnees.estim_status == 0 || donnees.fuel_accurate != 1) {
                    document.getElementById("fuelneed5").style.color = "#666666"
                } else {
                    if (fuelneed5_bg1_pct == 0) {
                        document.getElementById("fuelneed5").style.color = "#ffffff"
                    } else {
                        document.getElementById("fuelneed5").style.color = "#000000"
                    }
                }
            }

            if (advanced["disp_" + "fuelneed" + disp_sel]) {
                if (conso > 0.1)
                    if (fuelfactor * coef_fuel * fuelneed > 9999) {
                        document.getElementById("fuelneed").innerHTML = "9999";
                    } else if (fuelfactor * coef_fuel * fuelneed > 999) {
                        document.getElementById("fuelneed").innerHTML = (fuelfactor * coef_fuel * fuelneed).toFixed(0);
                    } else {
                        document.getElementById("fuelneed").innerHTML = (fuelfactor * coef_fuel * fuelneed).toFixed(1);
                    }
                else
                    document.getElementById("fuelneed").innerHTML = "--";
            }
            if (advanced["disp_" + "fuelneed1" + disp_sel]) {
                if (conso1 > 0.1)
                    if (fuelfactor * coef_fuel * fuelneed1 > 9999) {
                        document.getElementById("fuelneed1").innerHTML = "9999";
                    } else if (fuelfactor * coef_fuel * fuelneed1 > 999) {
                        document.getElementById("fuelneed1").innerHTML = (fuelfactor * coef_fuel * fuelneed1).toFixed(0);
                    } else {
                        document.getElementById("fuelneed1").innerHTML = (fuelfactor * coef_fuel * fuelneed1).toFixed(1);
                    }
                else
                    document.getElementById("fuelneed1").innerHTML = "--";
            }
            if (advanced["disp_" + "fuelneed5" + disp_sel]) {
                if (conso5 > 0.1)
                    if (fuelfactor * coef_fuel * fuelneed5 > 9999) {
                        document.getElementById("fuelneed5").innerHTML = "9999";
                    } else if (fuelfactor * coef_fuel * fuelneed5 > 999) {
                        document.getElementById("fuelneed5").innerHTML = (fuelfactor * coef_fuel * fuelneed5).toFixed(0);
                    } else {
                        document.getElementById("fuelneed5").innerHTML = (fuelfactor * coef_fuel * fuelneed5).toFixed(1);
                    }
                else
                    document.getElementById("fuelneed5").innerHTML = "--";
            }
        } else {
            document.getElementById("fuelneed").innerHTML = "buy pro";
            document.getElementById("fuelneed1").innerHTML = "buy pro";
            document.getElementById("fuelneed5").innerHTML = "buy pro";
        }

        if (donnees.b != undefined && advanced["disp_" + "brake2" + disp_sel]) {
            document.getElementById("brake2_").style.right = (100 - donnees.b) + "%";
            if (donnees.b == 0) {
                document.getElementById("brake2_text").style.color = "#000000"
            } else if (donnees.b == 100) {
                document.getElementById("brake2_text").style.color = "#c80000"
            } else {
                document.getElementById("brake2_text").style.color = "#ffffff"
            }
        }
        if (donnees.t != undefined && advanced["disp_" + "throttle2" + disp_sel]) {
            document.getElementById("throttle2_").style.right = (100 - donnees.t) + "%";
            if (donnees.t == 0) {
                document.getElementById("throttle2_text").style.color = "#000000"
            } else if (donnees.t == 100) {
                document.getElementById("throttle2_text").style.color = "#00a800"
            } else {
                document.getElementById("throttle2_text").style.color = "#ffffff"
            }
        }
        if (donnees.ffb != undefined && donnees.ffbpct != undefined && advanced["disp_" + "ffb2" + disp_sel]) {
            document.getElementById("ffb2_").style.right = (100 - donnees.ffbpct) + "%";
            document.getElementById("ffb2_text").innerHTML = donnees.ffb + " Nm&nbsp;";
            if (donnees.ffbpct == 0) {
                document.getElementById("ffb2_text").style.color = "#000000"
            } else if (donnees.ffbpct == 100) {
                document.getElementById("ffb2_text").style.color = "#1757ee"
            } else {
                document.getElementById("ffb2_text").style.color = "#ffffff"
            }
        }

        if (donnees.pss != undefined && advanced["disp_" + "perfs" + disp_sel]) {
            document.getElementById("perfs_dist").innerHTML = donnees.pss;
        }

        // Indication des pits
        if (donnees.pitpct != undefined) {
            dp_diff = (((donnees.dp + 2) % 1) - donnees.pitpct) * donnees.dtrack;
            document.getElementById("pitbar8").style.top = (1 - 0.111 - 0.222) * window.innerHeight * (1 + dp_diff / 12.5) + "px";
            document.getElementById("pitbar16").style.top = (1 - 0.111 - 0.222) * window.innerHeight * (1 + dp_diff / 25) + "px";
            document.getElementById("pitbar32").style.top = (1 - 0.111 - 0.222) * window.innerHeight * (1 + dp_diff / 50) + "px";
            document.getElementById("pitbar64").style.top = (1 - 0.111 - 0.222) * window.innerHeight * (1 + dp_diff / 100) + "px";
            document.getElementById("pitbar64").innerHTML = dp_diff.toFixed(2);
        } else {
            document.getElementById("pitbar8").style.top = window.innerHeight + 1 + "px";
            document.getElementById("pitbar16").style.top = window.innerHeight + 1 + "px";
            document.getElementById("pitbar32").style.top = window.innerHeight + 1 + "px";
            document.getElementById("pitbar64").style.top = window.innerHeight + 1 + "px";
            document.getElementById("pitbar64").innerHTML = "";
        }
        /*if (1 + dp_diff / 5 > 1) {
            document.getElementById("pitbar0").style.backgroundColor = "#ffff00"
        } else {
            document.getElementById("pitbar0").style.backgroundColor = "#ffffff"
        }
        if ((1 - 0.111 - 0.222) * (1 + dp_diff / 5) > 1 ) {
            document.getElementById("pitbar0").style.display = "none";
        } else {
            //document.getElementById("pitbar0").style.display = "block";
            document.getElementById("pitbar0").style.display = "none";
        }*/

        // Si on a chang� de voiture, on r�initialise les donn�es d'optimisations de rapport de boite
        // Et on affiche ou efface certains blocs en fonction de la voiture
        if (donnees.carname != carname) {
            carname = donnees.carname;
            gear_ = {};
            maxspeed_ = {};
            for (i in donnees.gear_) {
                gear_[i] = donnees.gear_[i]
            }

            if (carname in car_with_drs) {
                tmp_list = ["drs"];
                d = "_" + advanced["display_selected"];
                for (i in tmp_list) {
                    name = tmp_list[i];
                    if (advanced["disp_" + name + disp_sel]) {
                        document.getElementById(name).style.display = "inline-block";
                    }
                }
            } else {
                document.getElementById("drs").style.display = "none";
            }
            if (carname in car_with_ers_drs) {
                tmp_list = ["ers", "ersco", "ers_margin", "mgul", "mgu", "mgua", "mguf", "regen_lap", "regen_turn"];
                d = "_" + advanced["display_selected"];
                for (i in tmp_list) {
                    name = tmp_list[i];
                    if (advanced["disp_" + name + disp_sel]) {
                        document.getElementById(name).style.display = "inline-block";
                    }
                }
                document.getElementById("ers_bar").style.display = "inline-block";
                document.getElementById("ers_margin_min_bar").style.display = "inline-block";
                document.getElementById("mgu_margin_max_bar").style.display = "inline-block";
            } else {
                document.getElementById("ers").style.display = "none";
                document.getElementById("ers_bar").style.display = "none";
                document.getElementById("ersco").style.display = "none";
                document.getElementById("ers_margin").style.display = "none";
                document.getElementById("ers_margin_min_bar").style.display = "none";
                document.getElementById("mgu_margin_max_bar").style.display = "none";
                document.getElementById("mgul").style.display = "none";
                document.getElementById("mgu").style.display = "none";
                document.getElementById("mgua").style.display = "none";
                document.getElementById("mguf").style.display = "none";
                document.getElementById("regen_lap").style.display = "none";
                document.getElementById("regen_turn").style.display = "none";
            }

            responsive_dim();
        }

        // Engine Warnings
        water_warn = 0;
        oil_warn = 0;
        pit_speed_limiter = 0;
        if (donnees.warn != undefined) {
            //if (donnees.warn.slice(-1) == "1") water_warn = 1;
            //if (donnees.warn.slice(-3, -2) == "1") oil_warn = 1;
            if (donnees.warn.slice(-5, -4) == "1") pit_speed_limiter = 1;
        }
        // OIL > 140� C et WATER > 130� C
        if (donnees.oil >= 140 && (Date.now() % 500) <= 250) {
            oil_warn = 1;
        }
        if (donnees.w >= 130 && Math.abs((Date.now() % 500) - 375) <= 125) {
            water_warn = 1;
        }

        // Boussole
        compass_w = w * 128 / 1280;
        context_compass.clearRect(0, 0, compass_w, compass_w);

        if(donnees.north != 99 && advanced["disp_" + "compass" + disp_sel]) {
            // Dessin de la fl�che indiquant la direction du nord
            compass = coord_compass(donnees.yaw + donnees.north, compass_w/2, "northL");
            draw(compass, "#ff0000", 0);
            compass = coord_compass(donnees.yaw + donnees.north, compass_w/2, "northR");
            draw(compass, "#ff0000", 0);

            // Dessin de la fl�che indiquant la direction du vent
            compass = coord_compass(donnees.yaw + donnees.north + donnees.winddir, compass_w/2, "wind");
            draw(compass, "rgba(0,128,255,0.75)", 0);
        }

        if (advanced["disp_" + "delta_pre" + disp_sel] || advanced["disp_" + "delta_post" + disp_sel] || advanced["disp_" + "pre_rel" + disp_sel] || advanced["disp_" + "post_rel" + disp_sel]) {
            // Update des delta graphs
            deltas_and_gapcolor();
        }


        if (donnees.rpm != undefined && advanced["disp_" + "rpm" + disp_sel])
            document.getElementById("rpm").innerHTML = donnees.rpm.toFixed(0);


        if (donnees.gr != undefined && advanced["disp_" + "gear" + disp_sel]) {
            if (donnees.gr == -1)
                donnees.gr = "R";
            if (donnees.gr == 0)
                donnees.gr = "N";
            document.getElementById("gear").innerHTML = donnees.gr;
        }

        // Couleur du drapeau
        if (donnees.flag != undefined) {
            col_flag = "#ffffff";
            if (donnees.flag.slice(-4, -3) == "1") {
                bg = "#ffff00";  // yellow
                col_flag = "#000000"
            }
            else if (donnees.flag.slice(-9, -8) == "1") {
                bg = "#ffff00";  // yellow waving
                col_flag = "#000000"
            }
            else if (donnees.flag.slice(-15, -14) == "1") {
                bg = "#ffff00";  // caution
                col_flag = "#000000"
            }
            else if (donnees.flag.slice(-16, -15) == "1") {
                bg = "#ffff00";  // caution waving
                col_flag = "#000000"
            }
            else if (donnees.flag.slice(-6, -5) == "1") {
                bg = "#0000ff";  // blue
            }
            else if (donnees.flag.slice(-2, -1) == "1") {
                bg = "#ffffff";  // white
                col_flag = "#000000"
            }
            else if (donnees.flag.slice(-3, -2) == "1") {
                bg = "#00ff00";  // green
            }
            else if (donnees.flag.slice(-11, -10) == "1") {
                bg = "#00ff00";  // green held
            }
            else {
                bg = "#bbbbbb";  // autres
            }

            if (bg != bg_flag_old) {  // si on a chang� de couleur de drapeau
                bg_flag_start_time = Date.now() / 1000;
            }
            if (Date.now() / 1000 - bg_flag_start_time < 5) {
                bg_flag = bg;
            } else {
                bg_flag = "#bbbbbb";
            }
            bg_flag_old = bg;

            //document.getElementById("weather").style.backgroundColor = bg;
            if (advanced["disp_" + "weather" + disp_sel]) {
                change_bg("weather", bg, advanced["bg_" + "weather" + "_" + advanced["display_selected"]]);
            }
        }

        if (donnees.shift != undefined) {
            if (shiftlight_mode == 1) {
                shift = donnees.shift[1];
            } else {
                shift = donnees.shift[0];
            }

            // On d�termine le rpm optimal pour chaque vitesse
            if (shift_old != 1 && donnees.shift[1] == 1 && donnees.gr == gear_old && donnees.rpm > rpm_old && donnees.s > speed_old) {
                gear_[donnees.gr] = rpm_old;
            }

            if (donnees.shift[1] == 1 || (donnees.shift[1] == 0 && donnees.s < speed_old)) {
                shift_old = donnees.shift[1];
            }
        }

        // On d�termine la vitesse maximale atteinte dans un rapport
        if (donnees.gr in maxspeed_) {
            if (donnees.s > maxspeed_[donnees.gr]) {
                maxspeed_[donnees.gr] = donnees.s
            }
        } else {
            maxspeed_[donnees.gr] = donnees.s
        }

        rpm_old = donnees.rpm;
        gear_old = donnees.gr;
        if (shiftlight_mode == 1) {
            if (donnees.rpm >= gear_[donnees.gr]) {
                shift2 = 1;
            } else {
                shift2 = 0;
            }
        } else {
            shift2 = shift
        }


        if (donnees.rpm != undefined && advanced["disp_" + "rpm_leds" + disp_sel]) {
            //red_rpm = donnees.rpm_first;
            if (donnees.gear_ != undefined) {
                if(donnees.gr >= 1) {
                    red_rpm = donnees.gear_[donnees.gr];
                } else {
                    red_rpm = donnees.gear_[1];
                }
            }
            //console.log(red_rpm)
            // On v�rifie que ce n'est pas trop bas par rapport au max
            //if (red_rpm / max_rpm < 0.8) {
            //    red_rpm = 0.8 * max_rpm;
            //}
            //rpm_coef_a = 4 / (max_rpm - red_rpm);

            if (red_rpm >= max_rpm) {
                max_rpm = red_rpm / 0.80;
            }

            // La 5�me led s'allume quand on atteint le red_rpm
            rpm_coef_a = 7.5 / (max_rpm - red_rpm);
            rpm_coef_b = 12 - rpm_coef_a * max_rpm;
            num_led = (rpm_coef_a * donnees.rpm + rpm_coef_b).toFixed(0);

            if (pit_speed_limiter == 1) {
                // Si pit limiter activ�
                if(donnees.p) {
                    for (i = 1; i <= 12; i++) {
                        document.getElementById("led" + i).style.backgroundColor = "#ff0088";
                    }
                } else {  // D�s qu'on sort des pits on affiche les leds en vert
                    for (i = 1; i <= 12; i++) {
                        document.getElementById("led" + i).style.backgroundColor = "#00ff00";
                    }
                }
            } else {
                if(donnees.p) {  // Dans les pits, on indique notre vitesse par rapport � la vitesse limite
                    tmp = 6 + (donnees.s - donnees.pitspeedlimit) * 3.6;
                    num_led = Math.floor(tmp);
                    led_col_intensite = tmp - num_led;
                    tmp = Math.floor(200 - led_col_intensite * 200);
                    for (i = 1; i <= 12; i++) {
                        if (num_led <= 6) {
                            if (i <= num_led) {
                                document.getElementById("led" + i).style.backgroundColor = "#00ff00";
                            } else {
                                if (i <= 6) {
                                    if (i == num_led + 1) {
                                        document.getElementById("led" + i).style.backgroundColor = 'rgb(' + tmp + ', 255,' + tmp + ')';
                                    } else {
                                        document.getElementById("led" + i).style.backgroundColor = "#c8ffc8";
                                    }
                                } else {
                                    if (i == num_led + 1) {
                                        document.getElementById("led" + i).style.backgroundColor = 'rgb(255, ' + tmp + ',' + tmp + ')';
                                    } else {
                                        document.getElementById("led" + i).style.backgroundColor = "#ffc8c8";
                                    }
                                }
                            }
                        } else {
                            if (i <= num_led && i > 6) {
                                document.getElementById("led" + i).style.backgroundColor = "#ff0000";
                            } else {
                                if (i <= 6) {
                                    document.getElementById("led" + i).style.backgroundColor = "#00ff00";
                                } else {
                                    if (i == num_led + 1) {
                                        document.getElementById("led" + i).style.backgroundColor = 'rgb(255, ' + tmp + ',' + tmp + ')';
                                    } else {
                                        document.getElementById("led" + i).style.backgroundColor = "#ffc8c8";
                                    }
                                }
                            }
                        }

                    }
                /*} else if (bg_flag != "#bbbbbb") {
                    // Signalement du Drapeau
                    for (i = 1; i <= 12; i++) {
                        document.getElementById("led" + i).style.backgroundColor = bg_flag;
                    }
                } else if (oil_warn) {
                    for (i = 1; i <= 12; i++) {
                        document.getElementById("led" + i).style.backgroundColor = "#df49d8";
                    }
                } else if (water_warn) {
                    for (i = 1; i <= 12; i++) {
                        document.getElementById("led" + i).style.backgroundColor = "#3c9ffb";
                    }*/
                } else {
                    for (i = 1; i <= 12; i++) {
                        if (i < num_led) {
                            if (bg_flag != "#bbbbbb") {
                                document.getElementById("led" + i).style.backgroundColor = bg_flag;
                            } else if (oil_warn) {
                                document.getElementById("led" + i).style.backgroundColor = "#df49d8";
                            } else if (water_warn) {
                                document.getElementById("led" + i).style.backgroundColor = "#3c9ffb";
                            } else {
                                document.getElementById("led" + i).style.backgroundColor = led_col_on[i - 1];
                            }
                        } else if (i == num_led) {
                            document.getElementById("led" + i).style.backgroundColor = led_col_on[i - 1];
                        } else {
                            if (bg_flag != "#bbbbbb") {
                                document.getElementById("led" + i).style.backgroundColor = bg_flag;
                            } else if (oil_warn) {
                                document.getElementById("led" + i).style.backgroundColor = "#df49d8";
                            } else if (water_warn) {
                                document.getElementById("led" + i).style.backgroundColor = "#3c9ffb";
                            } else {
                                document.getElementById("led" + i).style.backgroundColor = led_col_off[i - 1];
                            }
                        }
                    }
                }
            }

        }
        // else
        if ((shift2 == 1 && donnees.gr > 0) || pit_speed_limiter == 1) {
        //if (((shift2 == 1 && donnees.gr > 0) || pit_speed_limiter == 1) && (!advanced["disp_" + "rpm_leds" + disp_sel])) {  // On n'affiche pas le shiftlight si les leds sont utilis�es
            if(donnees.p || pit_speed_limiter == 0) {
                light("#ff0088", "#ffffff");
            } else {
                light("#00ff00", "#ffffff");
            }
        } else {
            if (shift == 2 && donnees.gr > 1) {
            //    light("#00ff88", "#ffffff");
            } else if (bg_flag != "#bbbbbb") {
                light(bg_flag, col_flag);
            } else if (oil_warn) {
                light("#df49d8", "#ffffff");
            } else if (water_warn) {
                light("#3c9ffb", "#ffffff");
            } else if ((donnees.styp == "Open Qualify" || donnees.styp == "Lone Qualify") && (donnees.qinv)) {
                light("#ff0000", "#ffffff");  // On allume le dashboard en rouge si le temps de qualif est invalid� suite � un off-track
                // On indique qu'il faut passer le rapport sup�rieur pour l'�tannolage
            } else if (donnees.gr > 1 && shiftlight_mode == 1) {
                if (donnees.s > maxspeed_[donnees.gr - 1]) {
                    //light("#00ff88", "#ffffff");
                } else {
                    light_off();
                }
            } else {
                light_off();
            }
        }

        if (advanced["disp_" + "oil" + disp_sel] || advanced["disp_" + "water" + disp_sel]) {
            if (oil_warn) {
                //document.getElementById("oil").style.backgroundColor = "#df49d8";
                change_bg("oil", "#df49d8", advanced["bg_" + "oil" + "_" + advanced["display_selected"]]);
                document.getElementById("oil").style.color = "#000000"
            } else {
                //document.getElementById("oil").style.backgroundColor = "#6f0968";
                change_bg("oil", "#6f0968", advanced["bg_" + "oil" + "_" + advanced["display_selected"]]);
                document.getElementById("oil").style.color = "#ffffff"
            }

            if (water_warn) {
                //document.getElementById("water").style.backgroundColor = "#3c9ffb";
                change_bg("water", "#3c9ffb", advanced["bg_" + "water" + "_" + advanced["display_selected"]]);
                document.getElementById("water").style.color = "#000000"
            } else {
                //document.getElementById("water").style.backgroundColor = "#0c4f8b";
                change_bg("water", "#0c4f8b", advanced["bg_" + "water" + "_" + advanced["display_selected"]]);
                document.getElementById("water").style.color = "#ffffff"
            }
        }

        if (donnees.s != undefined && advanced["disp_" + "speed" + disp_sel]) {
            document.getElementById("speed").innerHTML = (speedfactor * donnees.s * 3.6).toFixed(0);
            if (donnees.u) {
                $("#speed_unit").html("km/h")
            } else {
                $("#speed_unit").html("m/h")
            }
        }

        /*if (donnees["pre_rc" + _f3] != undefined)
            document.getElementById("pre_rel").innerHTML = reformat_gap(donnees["pre_rc" + _f3]) ;
        if (donnees["post_rc" + _f3] != undefined)
            document.getElementById("post_rel").innerHTML = reformat_gap(donnees["post_rc" + _f3]) ;*/

        if (advanced["disp_" + "pre_rel" + disp_sel] || advanced["disp_" + "post_rel" + disp_sel]) {
            if (donnees.styp == "Race" && f3_mode_in_race_dashboard == 0) {
                if (donnees_new.pre_rc != undefined)
                    document.getElementById("pre_rel").innerHTML = reformat_gap(donnees.pre_rc);
                else
                    document.getElementById("pre_rel").innerHTML = "";
                if (donnees_new.post_rc != undefined)
                    document.getElementById("post_rel").innerHTML = reformat_gap(donnees.post_rc);
                else
                    document.getElementById("post_rel").innerHTML = "";
            } else {
                // En dehors des courses, on affiche l'�cart sur la piste
                if (donnees_new.pre_rcf3_f3 != undefined)
                    document.getElementById("pre_rel").innerHTML = reformat_gap(donnees.pre_rcf3_f3);
                else
                    document.getElementById("pre_rel").innerHTML = "";
                if (donnees_new.post_rcf3_f3 != undefined)
                    document.getElementById("post_rel").innerHTML = reformat_gap(donnees.post_rcf3_f3);
                else
                    document.getElementById("post_rel").innerHTML = "";
            }
        }

        if (donnees.d_b != undefined && advanced["disp_" + "delta_best" + disp_sel])
            document.getElementById("delta_best").innerHTML = reformat_delta(donnees.d_b) ;
        if (donnees.d_l != undefined && advanced["disp_" + "delta_last" + disp_sel])
            document.getElementById("delta_last").innerHTML = reformat_delta(donnees.d_l) ;

        if (donnees.bb != undefined && advanced["disp_" + "bb" + disp_sel])
            document.getElementById("bb").innerHTML = donnees.bb ;
        if (donnees.tc != undefined && advanced["disp_" + "tc" + disp_sel])
            document.getElementById("tc").innerHTML = donnees.tc ;
        if (donnees.tc2 != undefined && advanced["disp_" + "tc2" + disp_sel])
            document.getElementById("tc2").innerHTML = donnees.tc2 ;
        if (donnees.ffb != undefined && advanced["disp_" + "ffb" + disp_sel])
            document.getElementById("ffb").innerHTML = donnees.ffb ;

        if (donnees.b != undefined && advanced["disp_" + "b_cont" + disp_sel])
            document.getElementById("b").style.top = (100 - donnees.b) + "%" ;
        if (donnees.t != undefined && advanced["disp_" + "t_cont" + disp_sel])
            document.getElementById("t").style.top = (100 - donnees.t) + "%" ;
        if (donnees.ffbpct != undefined && advanced["disp_" + "ffbpct_cont" + disp_sel])
            document.getElementById("ffbpct").style.top = (100 - donnees.ffbpct) + "%" ;

        if (carname in car_with_ers_drs) {
            if (advanced["disp_" + "mgua" + disp_sel] || advanced["disp_" + "mguf" + disp_sel] || advanced["disp_" + "ers" + disp_sel] || advanced["disp_" + "ersco" + disp_sel] || advanced["disp_" + "ers_margin" + disp_sel] || advanced["disp_" + "mgul" + disp_sel] || advanced["disp_" + "mgu" + disp_sel] || advanced["disp_" + "drs" + disp_sel]) {
                //if (donnees.mgua != undefined && donnees.mguf != undefined) {
                if (donnees.mguf != undefined && advanced["disp_" + "mguf" + disp_sel]) {
                    //mgua = donnees.mgua;
                    mguf = donnees.mguf;
                    //if (mgua < 10) mgua = "0" + mgua;
                    //if (mguf < 10) mguf = "0" + mguf;
                    if (mguf != mguf_old) {
                        document.getElementById("mguf").innerHTML = mguf;
                    }
                    if (donnees.mgum != mgum_old) {
                        if (donnees.mgum == 0) {  // on est en manuel
                            document.getElementById("mgua").innerHTML = "Man.";
                        } else if (donnees.mgum == 2) {  // on est en Qualy
                            document.getElementById("mgua").innerHTML = "Qual";
                        } else {  // on est en auto
                            document.getElementById("mgua").innerHTML = "Auto";
                        }
                        if (donnees.mgum != undefined) {
                            if (donnees.mgum == 0) {  // si on est en manuel
                                change_bg("mguf", "#ffffff", advanced["bg_" + "mguf" + "_" + advanced["display_selected"]]);
                            } else {  // si on est en auto
                                change_bg("mguf", "#555555", advanced["bg_" + "mguf" + "_" + advanced["display_selected"]]);
                            }
                        }
                    }
                    mguf_old = mguf;
                    mgum_old = donnees.mgum;
                }
                if (donnees.ers != undefined && advanced["disp_" + "ers" + disp_sel]) {
                    document.getElementById("ers").innerHTML = donnees.ers.toFixed(1);
                    document.getElementById("ers_bar_").style.top = (100 - donnees.ers) + "%";
                    if (donnees.ers != ers_old) {
                        if (donnees.nee) {  // si pas assez d'energie
                            ers_bg = "#888888";
                        } else if (donnees.s <= speed_old && donnees.nee == 0 && donnees.ers > 99.9) {
                            ers_bg = "#ff00ff";
                        } else {
                            ers_bg = "#ff0000";
                        }
                        if (ers_bg != ers_bg_old) {
                            document.getElementById("ers").style.backgroundColor = ers_bg;
                            if (ers_bg != "888888") {
                                regen_col = ers_bg;
                            } else {
                                regen_col = "#ff0000";
                            }
                            document.getElementById("regen_turn").style.color = regen_col;
                        }
                        ers_bg_old = ers_bg;
                    }
                    ers_old = donnees.ers;
                }
                if (donnees.ersco != undefined) {
                    signe = "";
                    if (donnees.ersco >= 0) {
                        signe = "+";
                    }
                    document.getElementById("ersco").innerHTML = signe + donnees.ersco.toFixed(1);
                }
                if (donnees.ers_margin != undefined && advanced["disp_" + "ers_margin" + disp_sel]) {
                    signe = "";
                    if (donnees.ers_delta >= 0) {
                        signe = "+";
                    }
                    //donnees.ers_margin = 50;
                    //donnees.ers_margin_free = 25;
                    //donnees.mgu_margin = 50;
                    //donnees.ers_delta = -99.9;
                    if (donnees.ers_margin > -100) {
                        // Cette mini barre indique � combien de % l'ers sera au mini par rapport au dernier tour
                        if (donnees.ers_margin_free > 0) {
                            tmp_ers_margin = donnees.ers_margin;
                            if (tmp_ers_margin < 0) tmp_ers_margin = 0;
                            tmp_ers_margin_tot = tmp_ers_margin + donnees.ers_margin_free;
                            if (tmp_ers_margin_tot > 100) tmp_ers_margin_tot = 100;
                            document.getElementById("ers_margin_free_bar_").style.left = tmp_ers_margin + "%";
                            document.getElementById("ers_margin_free_bar_").style.right = (100 - tmp_ers_margin_tot) + "%";
                        } else {
                            document.getElementById("ers_margin_free_bar_").style.left = 100 + "%";
                            document.getElementById("ers_margin_free_bar_").style.right = 0 + "%";
                        }
                        if (donnees.ers_margin < 0) {
                            document.getElementById("ers_margin_min_bar_").style.right = 0 + "%";
                            document.getElementById("ers_margin_min_bar_").style.left = (100 + donnees.ers_margin) + "%";
                            document.getElementById("ers_margin_min_bar_").style.backgroundColor = "#ffff00";
                        } else {
                            document.getElementById("ers_margin_min_bar_").style.left = 0 + "%";
                            document.getElementById("ers_margin_min_bar_").style.right = (100 - donnees.ers_margin) + "%";
                            document.getElementById("ers_margin_min_bar_").style.backgroundColor = "#ff0000";
                        }
                        if (donnees.mgu_margin < 0) {
                            document.getElementById("mgu_margin_max_bar_").style.right = 0 + "%";
                            document.getElementById("mgu_margin_max_bar_").style.left = (100 + donnees.mgu_margin) + "%";
                            document.getElementById("mgu_margin_max_bar_").style.backgroundColor = "#ffffff";
                        } else {
                            document.getElementById("mgu_margin_max_bar_").style.left = 0 + "%";
                            document.getElementById("mgu_margin_max_bar_").style.right = (100 - donnees.mgu_margin) + "%";
                            document.getElementById("mgu_margin_max_bar_").style.backgroundColor = "#00ff00";
                        }
                    } else {
                        document.getElementById("ers_margin_min_bar_").style.right = 100 + "%";
                        document.getElementById("mgu_margin_max_bar_").style.right = 100 + "%";
                        document.getElementById("ers_margin_free_bar_").style.left = 100 + "%";
                        document.getElementById("ers_margin_free_bar_").style.right = 0 + "%";
                    }
                    if (donnees.ers_delta > -100) {
                        document.getElementById("ers_margin").innerHTML = signe + (donnees.ers_delta).toFixed(1);
                    } else {
                        document.getElementById("ers_margin").innerHTML = "--";
                    }
                    if (donnees.boost_manu != boost_manu_old || donnees.ref_ok != ref_ok_old) {
                        if (donnees.ref_ok == 0) {
                            document.getElementById("ers_margin").style.backgroundColor = "#000000";
                        } else if (donnees.ref_ok == 2) {
                            document.getElementById("ers_margin").style.backgroundColor = "#ffffff";
                        } else if (donnees.boost_manu == 1) {  // boost activ� manuellement en dehors du d�ployment normal du mode choisi
                            document.getElementById("ers_margin").style.backgroundColor = "#ff8800";
                            document.getElementById("ers_margin").style.color = "#00d9ff";
                        } else {
                            document.getElementById("ers_margin").style.backgroundColor = "#0088ff";
                            document.getElementById("ers_margin").style.color = "#ffffff";
                        }
                    }
                    boost_manu_old = donnees.boost_manu;
                    ref_ok_old = donnees.ref_ok;
                }
                if (donnees.mgul != undefined) {
                    document.getElementById("mgul").innerHTML = donnees.mgul.toFixed(1);
                    // On change ensuite la couleur de fond si l'�nergie est utilis�e
                    // On ne change le fond que si la valeur a chang�e pour optimiser les performances graphiques
                    if (donnees.boost != boost_old) {
                        if (donnees.boost) {
                            document.getElementById("mgul").style.backgroundColor = "#00d9ff";
                            document.getElementById("mgul").style.color = "#ff8800";
                        } else {
                            document.getElementById("mgul").style.backgroundColor = "#00ff00";
                            document.getElementById("mgul").style.color = "#000000";
                        }
                    }
                    if (donnees.boost_off != boost_off_old) {
                        if (donnees.boost_off) {
                            document.getElementById("mgul").style.backgroundColor = "#666666";
                            document.getElementById("mgul").style.color = "#333333";
                        } else {
                            document.getElementById("mgul").style.backgroundColor = "#00ff00";
                            document.getElementById("mgul").style.color = "#000000";
                        }
                    }
                    boost_old = donnees.boost;
                    boost_off_old = donnees.boost_off;
                }
                if (donnees.mgu != undefined) {
                    if (donnees.mgu != 0)
                        document.getElementById("mgu").innerHTML = donnees.mgu.toFixed(1);
                    else
                        document.getElementById("mgu").innerHTML = "--";
                }
            }
        }
        if (carname in car_with_drs) {
            if (advanced["disp_" + "drs" + disp_sel] || advanced["disp_" + "gear" + disp_sel]) {
                if (carname == "formularenault35" && donnees.drs_c != undefined) {
                    if (donnees.drs_c != drs_c_old) {
                        if (donnees.styp == "Race") {
                            document.getElementById("drs").innerHTML = (8 - donnees.drs_c).toFixed(0);
                        } else {
                            document.getElementById("drs").innerHTML = (donnees.drs_c).toFixed(0);
                        }
                    }
                    drs_c_old = donnees.drs_c;
                }
                if (donnees.drs != undefined) {
                    if (donnees.drs == 0) {
                        //document.getElementById("drs").style.backgroundColor = "#000000";
                        change_bg("drs", "#000000", advanced["bg_" + "drs" + "_" + advanced["display_selected"]]);
                        document.getElementById("drs").style.color = "#333333";
                    }
                    if (donnees.drs == 1) {
                        //document.getElementById("drs").style.backgroundColor = "#000000";
                        change_bg("drs", "#000000", advanced["bg_" + "drs" + "_" + advanced["display_selected"]]);
                        document.getElementById("drs").style.color = "#ffffff";
                        //document.getElementById("gear").style.backgroundColor = "#000000";
                        change_bg("gear", "#000000", advanced["bg_" + "gear" + "_" + advanced["display_selected"]]);
                        document.getElementById("gear").style.color = "#ffffff";
                    }
                    if (donnees.drs == 2) {
                        //document.getElementById("drs").style.backgroundColor = "#ffffff";
                        change_bg("drs", "#ffffff", advanced["bg_" + "drs" + "_" + advanced["display_selected"]]);
                        document.getElementById("drs").style.color = "#00dd00";
                        //document.getElementById("gear").style.backgroundColor = "#ffffff";
                        change_bg("gear", "#ffffff", advanced["bg_" + "gear" + "_" + advanced["display_selected"]]);
                        document.getElementById("gear").style.color = "#00dd00";
                    }
                    if (donnees.drs == 3) {
                        //document.getElementById("drs").style.backgroundColor = "#00dd00";
                        change_bg("drs", "#00dd00", advanced["bg_" + "drs" + "_" + advanced["display_selected"]]);
                        document.getElementById("drs").style.color = "#ee00aa";
                        //document.getElementById("gear").style.backgroundColor = "#00dd00";
                        change_bg("gear", "#00dd00", advanced["bg_" + "gear" + "_" + advanced["display_selected"]]);
                        document.getElementById("gear").style.color = "#ee00aa";
                    }
                }
            }

        }

        if (advanced["disp_" + "regen_gain" + disp_sel] && donnees.regen_gain != undefined) {
            document.getElementById("regen_gain").innerHTML = donnees.regen_gain ;
        }
        if (advanced["disp_" + "fuel_mixture" + disp_sel] && donnees.fuel_mixture != undefined) {
            document.getElementById("fuel_mixture").innerHTML = donnees.fuel_mixture ;
        }
        if (advanced["disp_" + "eng_pw" + disp_sel] && donnees.eng_pw != undefined) {
            document.getElementById("eng_pw").innerHTML = donnees.eng_pw ;
        }
        if (advanced["disp_" + "peak_bb" + disp_sel] && donnees.peak_bb != undefined) {
            document.getElementById("peak_bb").innerHTML = donnees.peak_bb ;
        }
        if (advanced["disp_" + "diff_preload" + disp_sel] && donnees.diff_preload != undefined) {
            document.getElementById("diff_preload").innerHTML = donnees.diff_preload ;
        }
        if (advanced["disp_" + "diff_entry" + disp_sel] && donnees.diff_entry != undefined) {
            document.getElementById("diff_entry").innerHTML = donnees.diff_entry ;
        }

        if (advanced["disp_" + "wj" + disp_sel] && donnees.wj != undefined) {
            document.getElementById("wj").innerHTML = donnees.wj ;
        }
        if (advanced["disp_" + "abs" + disp_sel] && donnees.abs != undefined) {
            document.getElementById("abs").innerHTML = donnees.abs ;
        }
        if (advanced["disp_" + "arb_f" + disp_sel] && donnees.arb_f != undefined) {
            document.getElementById("arb_f").innerHTML = donnees.arb_f ;
        }
        if (advanced["disp_" + "arb_r" + disp_sel] && donnees.arb_r != undefined) {
            document.getElementById("arb_r").innerHTML = donnees.arb_r ;
        }


        // On affiche les valeurs chang�es (TC, ABS, ...) pendant incar_set_change_delay secondes
        tmp_time = Date.now() / 1000;
        for (tmp_name in tab_setting) {
            if (donnees[tmp_name] != undefined) {
                if (donnees[tmp_name] != setting_[tmp_name]) {
                    if (setting_[tmp_name] != undefined) {
                        setting_changed_time = tmp_time;
                    }
                    setting_changed_name = tab_setting[tmp_name];
                    if (tmp_name == "mgum") {
                        if (donnees[tmp_name] == 0) {
                            setting_changed_value = "Man.";
                        } else if (donnees[tmp_name] == 1) {
                            setting_changed_value = "Auto";
                        } else if (donnees[tmp_name] == 2) {
                            setting_changed_value = "Qual";
                        }
                    } else if (tmp_name == "tc_tog") {
                        if (donnees[tmp_name] == 0) {
                            setting_changed_value = "OFF";
                        } else {
                            setting_changed_value = "ON";
                        }
                    } else if (tmp_name == "svfuel") {
                        setting_changed_value = (fuelfactor * coef_fuel * donnees[tmp_name]).toFixed(1);
                        if (donnees.u == 1) {  // L ou Kg
                            if (coef_fuel ==1) {  // Kg
                                setting_changed_name = "Fuel Add (L)"
                            } else {
                                setting_changed_name = "Fuel Add (Kg)"
                            }
                        } else {  // Ga ou Lb
                            if (coef_fuel ==1) {
                                setting_changed_name = "Fuel Add (Ga)"
                            } else {
                                setting_changed_name = "Fuel Add (Lb)"
                            }
                        }
                    } else {
                        setting_changed_value = donnees[tmp_name];
                    }
                    setting_[tmp_name] = donnees[tmp_name];
                }
            }
        }

        if (tmp_time - setting_changed_time < incar_set_change_delay) {
            document.getElementById("setting_changed_name").style.display = "block" ;
            document.getElementById("setting_changed_value").style.display = "block" ;
            document.getElementById("setting_changed_name").innerHTML = setting_changed_name ;
            document.getElementById("setting_changed_value").innerHTML = setting_changed_value ;
        } else {
            document.getElementById("setting_changed_name").style.display = "none" ;
            document.getElementById("setting_changed_value").style.display = "none" ;
        }

        speed_old = donnees.s;

    }

}


function coord_compass(angle, rayon, type) {


    if (type == "wind") {
        coord = {
            1: {'x': -0.25, 'y': -1},
            2: {'x': -0.25, 'y': 0.5},
            3: {'x': -0.5, 'y': 0.5},
            4: {'x': 0, 'y': 1},
            5: {'x': 0.5, 'y': 0.5},
            6: {'x': 0.25, 'y': 0.5},
            7: {'x': 0.25, 'y': -1}
        };
    }
    if (type == "northL") {
        coord = {
            1: {'x': 0, 'y': -1},
            2: {'x': 0.625, 'y': 0.75},
            3: {'x': 0.25, 'y': 0.625},
            4: {'x': 0, 'y': -0.125}
        };
    }
    if (type == "northR") {
        coord = {
            1: {'x': 0, 'y': -1},
            2: {'x': -0.625, 'y': 0.75},
            3: {'x': -0.25, 'y': 0.625},
            4: {'x': 0, 'y': -0.125}
        };
    }

    // On fait la rotation
    coord2 = {};
    for (i in coord) {
        coord2[i] = {};
        coord2[i].x = (coord[i].x * Math.cos(angle) - coord[i].y * Math.sin(angle) + 1) * rayon;
        coord2[i].y = (coord[i].x * Math.sin(angle) + coord[i].y * Math.cos(angle) + 1) * rayon;
    }

    return coord2;
}


function draw(compass, coul, ombrage) {

    if (ombrage == 1) {
        // On met l'Ombrage
        context_compass.shadowColor = "black";
        context_compass.shadowOffsetX = compass_w / 6 / 10;
        context_compass.shadowOffsetY = compass_w / 6 / 10;
        context_compass.shadowBlur = compass_w / 2 / 10;
    }

    context_compass.beginPath(); //On d�marre un nouveau trac�.
    context_compass.fillStyle = coul;
    //context_compass.globalCompositeOperation = "destination-out";
    for (i in compass) {
        if (i == 0) {
            context_compass.moveTo(compass[0].x, compass[0].y);
        } else {
            context_compass.lineTo(compass[i].x, compass[i].y);
        }
    }
    context_compass.fill(); //On trace seulement les lignes.
    context_compass.closePath();

    if (ombrage == 1) {
        // On enl�ve l'ombrage
        context_compass.shadowOffsetX = 0;
        context_compass.shadowOffsetY = 0;
        context_compass.shadowBlur = 0;
        context_compass.inset = 1;
    }

}

function light(bg, col) {
    if (advanced["shiftlight_on"]) {  // Que si le shiftlight est actif
        document.getElementById("shift").style.backgroundColor = bg;
        document.getElementById("shift_bg").style.backgroundColor = bg;
        //document.getElementById("gear").style.backgroundColor = bg;
        change_bg("gear", bg, advanced["bg_" + "gear" + "_" + advanced["display_selected"]]);
        //document.getElementById("rpm").style.backgroundColor = bg;
        change_bg("rpm", bg, advanced["bg_" + "rpm" + "_" + advanced["display_selected"]]);
        //document.getElementById("speed").style.backgroundColor = bg;
        change_bg("speed", bg, advanced["bg_" + "speed" + "_" + advanced["display_selected"]]);
        document.getElementById("gear").style.color = col;
        document.getElementById("rpm").style.color = col;
        document.getElementById("speed").style.color = col;
        document.getElementById("shift").style.zIndex = 3;
    }
}

function light_off() {
    document.getElementById("shift").style.zIndex = -1;
    if (transparency_OBS) {
        document.getElementById("shift").style.backgroundColor = "rgba(0,0,0,0)";
        document.getElementById("shift_bg").style.backgroundColor = "rgba(0,0,0,0)";
    } else {
        document.getElementById("shift").style.backgroundColor = "#000000";
        document.getElementById("shift_bg").style.backgroundColor = "#000000";
    }
    //document.getElementById("gear").style.backgroundColor = "#cccccc";
    change_bg("gear", "#cccccc", advanced["bg_" + "gear" + "_" + advanced["display_selected"]]);
    //document.getElementById("rpm").style.backgroundColor = "#cccccc";
    change_bg("rpm", "#cccccc", advanced["bg_" + "rpm" + "_" + advanced["display_selected"]]);
    //document.getElementById("speed").style.backgroundColor = "#444444";
    change_bg("speed", "#444444", advanced["bg_" + "speed" + "_" + advanced["display_selected"]]);
    document.getElementById("speed").style.color = "#ffffff";
    document.getElementById("gear").style.color = "#000000";
    document.getElementById("rpm").style.color = "#000000";
}

function display_rpm() {
    if (display_rpmshift == 0) {
        document.getElementById("display_rpmshift").style.display = "block";
        display_rpmshift = 1;
        /*document.getElementById("display_rpmshift").innerHTML = "";
        for (i in gear_) {
            if (i != "N") {
                document.getElementById("display_rpmshift").innerHTML += "GEAR " + i + " : " + gear_[i].toFixed(0) + "<br>"
            }
        }*/
    } else {
        document.getElementById("display_rpmshift").style.display = "none";
        display_rpmshift = 0;
    }
}