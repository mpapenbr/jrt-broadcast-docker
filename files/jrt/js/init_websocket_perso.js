// connections websocket avec le serveur Python
function init_ws() {
    ws = new WebSocket("ws://" + localIP + ":" + PORT8001 + "/");
    // Socket for the local communications
    ws.onmessage = function(d) {
        var datas = d.data;
        if (datas != "wait") {
            wait = 0;
            update_datas(datas);
        } else
            wait = 1
    };
    ws.onclose = function() {
        setTimeout(function () {location.reload()},5000);
        ws.close();
    };
    window.onbeforeunload = function() {
        ws.onclose = function () {}; // disable onclose handler first
        ws.close()
    };
    ws.onopen = function () {
        ws.send("send_statics");    // we want to collect the statics datas (name, num, ir)

        // We define the refresh rate for the datas
        local_tick = 0;

        setTimeout(function () {
            setInterval(function () {
                if ((wait == 0) && (ws.bufferedAmount == 0)) {   // On v�rifie que tout a bien d�j� �t� envoy�
                    if (local_tick % fps == 0) {  // specify the refresh rate of the typ2 datas -> 1 time per second
                        ws.send("2")
                    } else {
                        ws.send("3")
                    }
                    local_tick++;
                }
            }, 1000 / fps_perso);
        }, 0);
    }
}
