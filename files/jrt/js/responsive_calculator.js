function RGBA(e, alpha) { //e = jQuery element, alpha = background-opacity
    if (alpha < 1/500) {  // Pour �viter de bugguer avec firefox
        alpha = 1/500;
    }
    //b = e.css('backgroundColor');
    b = $(e).css('background-color');
    if (b != 'transparent') {
        $(e).css('backgroundColor', 'rgba' + b.slice(b.indexOf('('), ( (b.match(/,/g).length == 2) ? -1 : b.lastIndexOf(',') - b.length)) + ', ' + alpha + ')');
    }
}

function responsive_dim() {

    if (disp_infosbar == 0) {
        sessioninfos_height_ = 0;
    } else {
        sessioninfos_height_ = sessioninfos_height;
    }

    estlaps_bg1_pct = 0;
    fuelneed_bg1_pct = 0;
    lapsremain_bg1_pct = 0;
    gap_pct_lastlap = 0;

    if (responsive) {
        document.getElementById("infosbar").style.fontSize = Math.floor(24 * window.innerWidth / reference_w * ligne_h / 40) + "px";
        document.getElementById("infosbar").style.lineHeight = Math.floor(window.innerWidth / reference_w * ligne_h) + "px";
        document.getElementById("infosbar").style.top = Math.floor(sessioninfos_height_ * window.innerWidth / reference_w * ligne_h / ligne_h) + Math.floor((disp_sofbar * sofbar_h / ligne_h) * Math.floor(window.innerWidth / reference_w * ligne_h)) + "px";

        document.getElementById("click_infos").style.height = Math.floor((disp_sofbar * sofbar_h / ligne_h + 2 * disp_infosbar) * Math.floor(window.innerWidth / reference_w * ligne_h)) + "px";
        document.getElementById("click_infos").style.top = Math.floor(sessioninfos_height_ * window.innerWidth / reference_w * ligne_h / ligne_h) + 0 + "px";

        document.getElementById("sofbar").style.fontSize = Math.floor(32 * sofbar_h / ligne_h * window.innerWidth / reference_w * ligne_h / 40) + "px";
        document.getElementById("sofbar").style.lineHeight = Math.floor((disp_sofbar * sofbar_h / ligne_h) * Math.floor(window.innerWidth / reference_w * ligne_h)) + "px";
        document.getElementById("sofbar").style.top = 0*Math.floor(sessioninfos_height_ * window.innerWidth / reference_w * ligne_h / ligne_h) + 0 + "px";
    } else {
        document.getElementById("infosbar").style.fontSize = Math.floor(24 * ligne_h / 40) + "px";
        document.getElementById("infosbar").style.lineHeight = ligne_h + "px";
        document.getElementById("infosbar").style.top = Math.floor(sessioninfos_height_ * ligne_h / ligne_h) + Math.floor((disp_sofbar * sofbar_h / ligne_h) * ligne_h) + "px";

        document.getElementById("click_infos").style.height = (disp_sofbar * sofbar_h / ligne_h + 2 * disp_infosbar)*ligne_h + "px";
        document.getElementById("click_infos").style.top = Math.floor(sessioninfos_height_ * ligne_h / ligne_h) + 0 + "px";

        document.getElementById("sofbar").style.fontSize = Math.floor(32 * sofbar_h / 40) + "px";
        document.getElementById("sofbar").style.lineHeight = Math.floor((disp_sofbar * sofbar_h / ligne_h)*ligne_h) + "px";
        document.getElementById("sofbar").style.top = 0*Math.floor(sessioninfos_height_ * ligne_h / ligne_h) + 0 + "px";
    }

    if (transparency_OBS) {
        //document.body.style.backgroundColor = "rgba(134,34,34,0.0)";
        RGBA(jQuery('body'), 0.0);
        RGBA(jQuery('#page'), 0.0);
    } else {
        //document.body.style.backgroundColor = "rgba(34,34,34,1.0)";
        RGBA(jQuery('body'), 1.0);
        RGBA(jQuery('#page'), 1.0);
    }

    $("#page").css("width", window.innerWidth + "px");
    $("#page").css("height", window.innerHeight + "px");

    // Mise en forme de la barre d'infos avec ses �l�ments
    if (disp_infosbar == 0) {
        document.getElementById("infosbar").style.display = "none"
    } else {
        document.getElementById("infosbar").style.display = "block"
    }
    if (responsive) {
        h = Math.floor(window.innerWidth / reference_w * ligne_h);
    } else {
        h = ligne_h
    }

    // Si on veut cacher les infos de fuel on d�cale le timeremain et les infos de session
    if (disp_fuelinfos) {
        decal_timeremain = 0;
        decal_lapsremain = 0;
        decal_infos = 0;
        document.getElementById("litre").style.display = "block";
        document.getElementById("kg").style.display = "block";
        document.getElementById("tank_h").style.display = "block";
        document.getElementById("tank").style.display = "block";
        document.getElementById("conso").style.display = "block";
        document.getElementById("estlaps_h").style.display = "block";
        document.getElementById("estlaps").style.display = "block";
        document.getElementById("estlaps_bg0").style.display = "block";
        document.getElementById("estlaps_bg1").style.display = "block";
        document.getElementById("fuelneed_h").style.display = "block";
        document.getElementById("fuelneed").style.display = "block"
        document.getElementById("fuelneed_bg0").style.display = "block";
        document.getElementById("fuelneed_bg1").style.display = "block"
    } else {
        decal_timeremain = -6*h;
        decal_lapsremain = -6*h;
        decal_infos = -11*h;
        document.getElementById("litre").style.display = "none";
        document.getElementById("kg").style.display = "none";
        document.getElementById("tank_h").style.display = "none";
        document.getElementById("tank").style.display = "none";
        document.getElementById("conso").style.display = "none";
        document.getElementById("estlaps_h").style.display = "none";
        document.getElementById("estlaps").style.display = "none";
        document.getElementById("estlaps_bg0").style.display = "none";
        document.getElementById("estlaps_bg1").style.display = "none";
        document.getElementById("fuelneed_h").style.display = "none";
        document.getElementById("fuelneed").style.display = "none"
        document.getElementById("fuelneed_bg0").style.display = "none";
        document.getElementById("fuelneed_bg1").style.display = "none";
    }

    document.getElementById("litre").style.top = 0*h + "px";
    document.getElementById("litre").style.left = 0*h + "px";
    document.getElementById("litre").style.width = h + "px";
    document.getElementById("kg").style.top = h + "px";
    document.getElementById("kg").style.left = 0*h + "px";
    document.getElementById("kg").style.width = h + "px";
    document.getElementById("tank_h").style.top = 0*h + "px";
    document.getElementById("tank_h").style.left = 0*h + "px";
    document.getElementById("tank_h").style.width = 3*h + "px";
    document.getElementById("tank_h").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("tank_h").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("tank").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("tank").style.left = 0*h + "px";
    document.getElementById("tank").style.width = 3*h + "px";
    document.getElementById("tank").style.lineHeight = 2*h - 2*Math.floor(0.5*h) + "px";
    document.getElementById("tank").style.fontSize = 40 * h / 40 + "px";
    document.getElementById("conso").style.top = Math.floor(0.5*h) + 2*h - 2*Math.floor(0.5*h) + "px";
    document.getElementById("conso").style.left = 0*h + "px";
    document.getElementById("conso").style.width = 3*h + "px";
    document.getElementById("conso").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("conso").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("estlaps_h").style.top = 0*h + "px";
    document.getElementById("estlaps_h").style.left = 3*h + "px";
    document.getElementById("estlaps_h").style.width = 3*h + "px";
    document.getElementById("estlaps_h").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("estlaps_h").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("estlaps").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("estlaps").style.left = 3*h + "px";
    document.getElementById("estlaps").style.width = 3*h + "px";
    document.getElementById("estlaps").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("estlaps").style.fontSize = 60 * h / 40 + "px";
    document.getElementById("estlaps_bg0").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("estlaps_bg0").style.left = 3*h + "px";
    document.getElementById("estlaps_bg0").style.width = 3*h + "px";
    document.getElementById("estlaps_bg0").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("estlaps_bg1").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("estlaps_bg1").style.left = 3*h + "px";
    document.getElementById("estlaps_bg1").style.width = 3*h*estlaps_bg1_pct + "px";
    document.getElementById("estlaps_bg1").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain_h").style.top = 0*h + "px";
    document.getElementById("lapsremain_h").style.left = decal_lapsremain + 13*h + "px";
    document.getElementById("lapsremain_h").style.width = 4*h + "px";
    document.getElementById("lapsremain_h").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain_h").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("lapsremain").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain").style.left = decal_lapsremain + 13*h + "px";
    document.getElementById("lapsremain").style.width = 4*h + "px";
    document.getElementById("lapsremain").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain").style.fontSize = 60 * h / 40 + "px";
    document.getElementById("lapsremain_bg0").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain_bg0").style.left = decal_lapsremain + 13*h + "px";
    document.getElementById("lapsremain_bg0").style.width = 4*h + "px";
    document.getElementById("lapsremain_bg0").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain_bg1").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain_bg1").style.left = decal_lapsremain + 13*h + "px";
    document.getElementById("lapsremain_bg1").style.width = 4*h + "px";
    document.getElementById("lapsremain_bg1").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain_bg2").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("lapsremain_bg2").style.left = decal_lapsremain + 13*h + "px";
    lapsremain_bg2_left = decal_lapsremain + 13*h;
    document.getElementById("lapsremain_bg2").style.width = 1 + "px";
    document.getElementById("lapsremain_bg2").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("fuelneed_h").style.top = 0*h + "px";
    document.getElementById("fuelneed_h").style.left = 17*h + "px";
    document.getElementById("fuelneed_h").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("fuelneed_h").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("fuelneed").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("fuelneed").style.left = 17*h + "px";
    document.getElementById("fuelneed_bg0").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("fuelneed_bg0").style.left = 17*h + "px";
    document.getElementById("fuelneed_bg1").style.top = Math.floor(0.5*h) + Math.floor(fuelneed_bg1_pct * (2*h - Math.floor(0.5*h))) + "px";
    document.getElementById("fuelneed_bg1").style.left = 17*h + "px";
    if (disp_infosbar == 2) {
        document.getElementById("fuelneed_h").style.right = 0 + "px";
        document.getElementById("fuelneed_h").style.width = "auto";
        document.getElementById("fuelneed").style.right = 0 + "px";
        document.getElementById("fuelneed").style.width = "auto";
        document.getElementById("fuelneed_bg0").style.right = 0 + "px";
        document.getElementById("fuelneed_bg0").style.width = "auto";
        document.getElementById("fuelneed_bg1").style.right = 0 + "px";
        document.getElementById("fuelneed_bg1").style.width = "auto";
    } else {
        document.getElementById("fuelneed_h").style.width = 5*h + "px";
        document.getElementById("fuelneed").style.width = 5 * h + "px";
        document.getElementById("fuelneed_bg0").style.width = 5 * h + "px";
        document.getElementById("fuelneed_bg1").style.width = 5 * h + "px";
    }
    document.getElementById("fuelneed").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("fuelneed").style.fontSize = 60 * h / 40 + "px";
    document.getElementById("fuelneed_bg0").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("fuelneed_bg0").style.fontSize = 60 * h / 40 + "px";
    document.getElementById("fuelneed_bg1").style.lineHeight = 2*h - Math.floor(0.5*h) - Math.floor(fuelneed_bg1_pct * (2*h - Math.floor(0.5*h))) + "px";
    document.getElementById("fuelneed_bg1").style.fontSize = 60 * h / 40 + "px";

    document.getElementById("timeremain_h").style.top = 0*h + "px";
    document.getElementById("timeremain_h").style.left = decal_timeremain + 6*h + "px";
    document.getElementById("timeremain_h").style.width = 7*h + "px";
    document.getElementById("timeremain_h").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("timeremain_h").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("timeremain").style.top = Math.floor(0.5*h) + "px";
    document.getElementById("timeremain").style.left = decal_timeremain + 6*h + "px";
    document.getElementById("timeremain").style.width = 7*h + "px";
    document.getElementById("timeremain").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("timeremain").style.fontSize = 60 * h / 40 + "px";

    document.getElementById("sessioninfos").style.top = -Math.floor(sessioninfos_height_ * h / ligne_h) + 0*2* (disp_infosbar - 1)*h + "px";
    document.getElementById("sessioninfos").style.left = 0*(decal_infos + 22*h)*(2-disp_infosbar) + "px";
    document.getElementById("sessioninfos").style.fontSize = sessioninfos_height_ / 20 * 14 * h / ligne_h + "px";
    document.getElementById("sessioninfos").style.lineHeight = Math.floor(sessioninfos_height_ * h / ligne_h) + 0*Math.floor(0.5*h) + "px";

    document.getElementById("sof_cont").style.top = 2* (disp_infosbar - 1)*h + 0*Math.floor(0.5 * h) + "px";
    document.getElementById("sof_cont").style.left = (decal_infos + 22*h) *(2-disp_infosbar) + "px";
    document.getElementById("sof_cont").style.lineHeight = 2*h - 0*Math.floor(0.5*h) + "px";
    document.getElementById("sof_cont").style.height = 2*h - 0*Math.floor(0.5*h) + "px";

    document.getElementById("tires_all_h").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("tires_all_h").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("tires_all_cont").style.lineHeight = 2*h - 2*Math.floor(0.5*h) + "px";
    document.getElementById("tires_all_cont").style.width = 2*h + "px";
    document.getElementById("tires_none_h").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("tires_none_h").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("tires_none_cont").style.lineHeight = 2*h - 2*Math.floor(0.5*h) + "px";
    document.getElementById("tires_none_cont").style.width = 2*h + "px";
    document.getElementById("tires").style.top = 2* (disp_infosbar - 1)*h + Math.floor(0.5*h) + "px";
    document.getElementById("tires").style.lineHeight = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("tires").style.height = 2*h - Math.floor(0.5*h) + "px";
    document.getElementById("tires").style.right = 2*h - Math.floor(0.5*h) + 8 + "px";

    document.getElementById("logo").style.top = 2* (disp_infosbar - 1)*h + Math.floor(0.5*h) + "px";
    document.getElementById("logo").style.height = 2*h - Math.floor(0.5*h) - 2 + "px";

    document.getElementById("app_name").style.fontSize = 14 * h / 40 + "px";
    document.getElementById("app_name").style.lineHeight = Math.floor(0.5*h) + "px";
    document.getElementById("app_name").style.top = (disp_infosbar - 1)*(2*h) + "px";

    document.getElementById("flag_img").style.top = 2* (disp_infosbar - 1)*h + Math.floor(0.5*h) + "px";
    document.getElementById("flag_img").style.left = (decal_infos + 22*h)*(2-disp_infosbar) + "px";
    document.getElementById("flag_img").style.right = 2*h - Math.floor(0.5*h) + 8 + "px";
    //document.getElementById("flag_img").style.width = window.innerWidth - (2*h - Math.floor(0.5*h) + 8) - (decal_infos + 23*h)*(2-disp_infosbar) + "px";
    document.getElementById("flag_img").style.height = 2*h - Math.floor(0.5*h) + "px";

    // mini sofbar
    if (disp_sofbar) {
        document.getElementById("sofbar").style.display = "block";
    } else {
        document.getElementById("sofbar").style.display = "none"
    }


    // Gestion du bouton fullscreen
    if ((fullscreen_button == 1) && (!document.fullscreenElement &&    // alternative standard method
        !document.mozFullScreenElement && !document.webkitFullscreenElement)) {  // current working methods
        // On n'est pas en fullscreen
        if (broadcast <= 1) {
            //if (fullscreen_button == 1) {
                $("#fullscreen").css("display", "block");
                if (fullscreen_button_timeout > 0) {
                    setTimeout(function () {
                        $("#fullscreen").css("display", "none");
                    }, 1000*fullscreen_button_timeout)
                }
            //}
        }
    } else {
        // On est d�j� en fullscreen donc on cache le bouton
        $("#fullscreen").css("display", "none");
    }
    if( /iPhone|iPad/i.test(navigator.userAgent)) {  //Si c'est un iPad ou iPhone
        $("#fullscreen").css("display", "none");
    }

}
