function change_config(config) {

    //console.log(config);

    /*
    config_ = config.split(";");
    config_page = config_[0];  // nom de la page � configurer
    config_ = config_[1].split("=");
    config_varname = config_[0];  // nom du param�tre � changer
    config_value = config_[1];  // nouvelle valeur
    */

    config_page = config.page;
    //console.log(config_page);

    // On fait correspondre le nom de la page avec le nom court
    if (window_name == "JRT Compteur") {
        window_shortname = "compteur";
    }
    if (window_name == "JRT Timing") {
        window_shortname = "timing";
    }
    if (window_name == "JRT Timing2") {
        window_shortname = "timing2";
    }
    if (window_name == "JRT Timing3") {
        window_shortname = "timing3";
    }
    if (window_name == "JRT Timing4") {
        window_shortname = "timing4";
    }
    if (window_name == "JRT Timing Broadcast") {
        window_shortname = "timing_broadcast";
    }
    if (window_name == "JRT Trackmap") {
        window_shortname = "trackmap";
    }
    if (window_name == "JRT Trackmap2") {
        window_shortname = "trackmap2";
    }
    if (window_name == "JRT Trackmap_3D") {
        window_shortname = "trackmap_3d";
    }
    if (window_name == "JRT Calculator") {
        window_shortname = "calculator";
    }
    if (window_name == "JRT Dashboard") {
        window_shortname = "dashboard";
    }
    if (window_name == "JRT Dashboard2") {
        window_shortname = "dashboard2";
    }
    if (window_name == "JRT Trackmap_3D") {
        window_shortname = "trackmap_3d";
    }
    if (window_name == "JRT ButtonBox") {
        window_shortname = "buttonbox";
    }
    if (window_name == "JRT Spotter") {
        window_shortname = "spotter";
    }

    if ((config_page == window_shortname || config_page == "all") && broadcast <= 1) {    // Si les changements doivent bien �tre effectu�s pour cette page
                                                                // et qu'on n'est pas en mode broadcast
        for(config_varname in config.param) {
            fullscreen_button_old = fullscreen_button;
            switch (config_varname) {
                case "tab_titres":
                    tab_titres = config.param["tab_titres"];
                    break;
                case "set_title":
                    set_title = config.param["set_title"];
                    break;
                case "w":
                    w = config.param["w"];
                    break;

                case "clubname_mode":
                    clubname_mode = config.param["clubname_mode"];
                    break;
                case "name_mode":
                    name_mode = config.param["name_mode"];
                    break;
                case "lic_mode":
                    lic_mode = config.param["lic_mode"];
                    break;
                case "ir_mode":
                    ir_mode = config.param["ir_mode"];
                    break;
                case "responsive":
                    responsive = config.param["responsive"];
                    break;
                case "reference_w":
                    reference_w = config.param["reference_w"];
                    break;
                case "ligne_h":
                    ligne_h = config.param["ligne_h"];
                    break;
                case "banner_height":
                    banner_height = config.param["banner_height"];
                    break;
                case "banner_logo":
                    banner_logo = config.param["banner_logo"];
                    break;
                case "banner_mode":
                    banner_mode = config.param["banner_mode"];
                    break;
                case "banner_color":
                    banner_color = config.param["banner_color"];
                    break;
                case "banner_background":
                    banner_background = config.param["banner_background"];
                    break;
                case "transparency_OBS":
                    transparency_OBS = config.param["transparency_OBS"];
                    break;
                case "transparence_lignes":
                    transparence_lignes = config.param["transparence_lignes"];
                    break;
                case "animation":
                    animation = config.param["animation"];
                    break;
                case "disp_sofbar":
                    disp_sofbar = config.param["disp_sofbar"];
                    break;
                case "sofbar_h":
                    sofbar_h = config.param["sofbar_h"];
                    break;
                case "selected_driver_mode":
                    selected_driver_mode = config.param["selected_driver_mode"];
                    break;
                case "deltagraph_for_all":
                    deltagraph_for_all = config.param["deltagraph_for_all"];
                    break;
                case "disp_titres":
                    disp_titres = config.param["disp_titres"];
                    break;
                case "f3_box":
                    f3_box = config.param["f3_box"];
                    break;
                case "tires_buttons":
                    tires_buttons = config.param["tires_buttons"];
                    break;
                case "autoscroll":
                    autoscroll = config.param["autoscroll"];
                    break;
                case "autoscroll_mode":
                    autoscroll_mode = config.param["autoscroll_mode"];
                    break;
                case "disp_infosbar":
                    disp_infosbar = config.param["disp_infosbar"];
                    break;
                case "disp_fuelinfos":
                    disp_fuelinfos = config.param["disp_fuelinfos"];
                    break;
                case "disp_gapcolors":
                    disp_gapcolors = config.param["disp_gapcolors"];
                    break;
                case "disp_scrollbar":
                    disp_scrollbar = config.param["disp_scrollbar"];
                    break;
                case "focus_replay_delay":
                    focus_replay_delay = config.param["focus_replay_delay"];
                    break;

                case "trackmap_disp_timelost":
                    trackmap_disp_timelost = config.param["trackmap_disp_timelost"];
                    break;
                case "trackmap_disp_north":
                    trackmap_disp_north = config.param["trackmap_disp_north"];
                    break;
                case "trackmap_disp_wind":
                    trackmap_disp_wind = config.param["trackmap_disp_wind"];
                    break;
                case "trackmap_disp_weather_infos":
                    trackmap_disp_weather_infos = config.param["trackmap_disp_weather_infos"];
                    break;
                case "disp_timing_under_trackmap":
                    disp_timing_under_trackmap = config.param["disp_timing_under_trackmap"];
                    break;
                case "timing_trackmap_leftmargin":
                    timing_trackmap_leftmargin = config.param["timing_trackmap_leftmargin"];
                    break;
                case "trackmap_disp_mode":
                    trackmap_disp_mode = config.param["trackmap_disp_mode"];
                    break;
                case "trackmap_thickness_coef":
                    trackmap_thickness_coef = config.param["trackmap_thickness_coef"];
                    break;
                case "transparence_fond_trackmap":
                    transparence_fond_trackmap = config.param["transparence_fond_trackmap"];
                    break;

                case "window_x":
                    window_x = config.param["window_x"];
                    break;
                case "window_y":
                    window_y = config.param["window_y"];
                    break;
                case "window_w":
                    window_w = config.param["window_w"];
                    break;
                case "window_h":
                    window_h = config.param["window_h"];
                    break;
                case "window_alpha":
                    window_alpha = config.param["window_alpha"];
                    break;
                case "window_topmost":
                    window_topmost = config.param["window_topmost"];
                    break;
                case "window_borders":
                    window_borders = config.param["window_borders"];
                    break;
                case "fullscreen_button":
                    fullscreen_button = config.param["fullscreen_button"];
                    break;
                case "fullscreen_button_timeout":
                    fullscreen_button_timeout = config.param["fullscreen_button_timeout"];
                    break;

                case "f3_mode_in_race_dashboard": f3_mode_in_race_dashboard = config.param["f3_mode_in_race_dashboard"];break;
                case "shiftlight_mode": shiftlight_mode = config.param["shiftlight_mode"];break;

                case "gear_":
                    for (i = 1; i <= 8; i++) {
                        gear_[i] = config.param["gear_"][i];
                        donnees.gear_[i] = gear_[i];
                    }
                    break;

                case "refuelspeed":
                    refuelspeed = config.param["refuelspeed"];
                    donnees.refuelspeed = refuelspeed;
                    break;
                case "conso_moy":
                    donnees.co5 = config.param["conso_moy"];
                    break;
                case "temperature_mode":
                    temperature_mode = parseInt(config.param["temperature_mode"]);
                    break;
                case "car_mode":
                    car_mode = config.param["car_mode"];
                    break;
                case "trackmap_disp_logo":
                    trackmap_disp_logo = config.param["trackmap_disp_logo"];
                    break;
                case "trackmap_color":
                    trackmap_color = config.param["trackmap_color"];
                    break;
                case "trackmap_bg_img":
                    trackmap_bg_img = config.param["trackmap_bg_img"];
                    break;
                case "trackmap_bg_color":
                    trackmap_bg_color = config.param["trackmap_bg_color"];
                    break;
                case "disp_events_ticker":
                    disp_events_ticker = config.param["disp_events_ticker"];
                    break;
                case "disable_all_events":
                    disable_all_events = config.param["disable_all_events"];
                    break;
                case "events_ticker_height":
                    events_ticker_height = config.param["events_ticker_height"];
                    break;
                case "events_ticker_font_coef":
                    events_ticker_font_coef = config.param["events_ticker_font_coef"];
                    break;
                case "events_ticker_disp_pits":
                    events_ticker_disp_pits = config.param["events_ticker_disp_pits"];
                    break;
                case "events_ticker_disp_newbest":
                    events_ticker_disp_newbest = config.param["events_ticker_disp_newbest"];
                    break;
                case "events_ticker_disp_newleader":
                    events_ticker_disp_newleader = config.param["events_ticker_disp_newleader"];
                    break;
                case "events_ticker_disp_driverswap":
                    events_ticker_disp_driverswap = config.param["events_ticker_disp_driverswap"];
                    break;
                case "events_ticker_disp_overtake":
                    events_ticker_disp_overtake = config.param["events_ticker_disp_overtake"];
                    break;
                case "wind_alert":
                    wind_alert = config.param["wind_alert"];
                    break;
                case "disp_menu":
                    disp_menu = config.param["disp_menu"];
                    break;
                case "infosbar_coef":
                    infosbar_coef = config.param["infosbar_coef"];
                    break;
                case "sessioninfos_height":
                    sessioninfos_height = config.param["sessioninfos_height"];
                    break;
                case "gap_mode":
                    gap_mode = config.param["gap_mode"];
                    break;
                case "cgap_mode":
                    cgap_mode = config.param["cgap_mode"];
                    break;
                case "rel_mode":
                    rel_mode = config.param["rel_mode"];
                    break;
                case "pack_disp":
                    pack_disp = config.param["pack_disp"];
                    break;
                case "pack_gap":
                    pack_gap = config.param["pack_gap"];
                    break;
                case "pack_transparency":
                    pack_transparency = config.param["pack_transparency"];
                    break;
                case "pack_color":
                    pack_color = config.param["pack_color"];
                    break;
                case "disp_wheel":
                    disp_wheel = config.param["disp_wheel"];
                    break;
                case "incar_set_change_delay":
                    incar_set_change_delay = config.param["incar_set_change_delay"];
                    break;
                case "trackmap_camera_fov":
                    trackmap_camera_fov = config.param["trackmap_camera_fov"];
                    break;
                case "trackmap_elevation_factor":
                    trackmap_elevation_factor = config.param["trackmap_elevation_factor"];
                    break;
                case "trackmap_antialias":
                    trackmap_antialias = config.param["trackmap_antialias"];
                    break;
                case "trackmap_lateral_color":
                    trackmap_lateral_color = config.param["trackmap_lateral_color"];
                    break;
                case "trackmap_banking_factor":
                    trackmap_banking_factor = config.param["trackmap_banking_factor"];
                    break;
                case "trackmap_disp_turns":
                    trackmap_disp_turns = config.param["trackmap_disp_turns"];
                    break;
                case "avg1_nblaps":
                    avg1_nblaps = config.param["avg1_nblaps"];
                    break;
                case "avg2_nblaps":
                    avg2_nblaps = config.param["avg2_nblaps"];
                    break;
                case "avg3_nblaps":
                    avg3_nblaps = config.param["avg3_nblaps"];
                    break;
                case "avg1_best":
                    avg1_best = config.param["avg1_best"];
                    break;
                case "avg2_best":
                    avg2_best = config.param["avg2_best"];
                    break;
                case "avg3_best":
                    avg3_best = config.param["avg3_best"];
                    break;
                case "spotter_landmark_disp":
                    spotter_landmark_disp = config.param["spotter_landmark_disp"];
                    break;
                case "jrt_logo_disp":
                    jrt_logo_disp = config.param["jrt_logo_disp"];
                    break;
                case "spotter_rule_disp":
                    spotter_rule_disp = config.param["spotter_rule_disp"];
                    break;
                case "drag_enable":
                    drag_enable = config.param["drag_enable"];
                    break;
                case "clock_disp":
                    clock_disp = config.param["clock_disp"];
                    break;


                case "advanced":
                    //advanced = config.param["advanced"];
                    $.extend(true, advanced, config.param["advanced"]);
                    dashboard_ref_w = config.param["dashboard_ref_w"];
                    dashboard_ref_h = config.param["dashboard_ref_h"];
                    break;
            }
        }

        for(config_varname in config.param) {
            switch (config_varname) {
                case "fps":
                    fps = config.param["fps"];
                    if (broadcast == 0) {
                        clearInterval(ws_boucle);
                        ws.send("window;"+window_name+";"+window_x+";"+window_y+";"+window_w+";"+window_h+";"+window_alpha+";"+window_topmost+";"+window_borders);
                        if (config_page == "compteur") {
                            t0_ms = parseInt(Date.now());
                            local_tick = 0;
                            ws_boucle = setInterval(ws_boucle_compteur, 1000 / fps);
                        } else if (config_page == "spotter") {
                            t0_ms = parseInt(Date.now());
                            local_tick = 0;
                            ws_boucle = setInterval(ws_boucle_spotter, 1000 / fps);
                        } else {
                            t0_ms = parseInt(Date.now());
                            local_tick = 0;
                            ws_boucle = setInterval(ws_boucle_timing, 1000 / fps);
                        }
                    }
                    break;

                case "fps_trackmap":
                    fps_trackmap = config.param["fps_trackmap"];
                    if (broadcast == 0) {
                        clearInterval(ws_boucle);
                        ws.send("window;"+window_name+";" + window_x + ";" + window_y + ";" + window_w + ";" + window_h + ";" + window_alpha + ";" + window_topmost + ";" + window_borders);
                        t0_ms = parseInt(Date.now());
                        local_tick = 0;
                        ws_boucle = setInterval(ws_boucle_trackmap, 1000 / fps_trackmap);
                    }
                    break;

                case "fps_dashboard":
                    fps_dashboard = config.param["fps_dashboard"];
                    if (broadcast == 0) {
                        clearInterval(ws_boucle);
                        //document.getElementById("estlaps_h").innerHTML = window_x;
                        ws.send("window;"+window_name+";" + window_x + ";" + window_y + ";" + window_w + ";" + window_h + ";" + window_alpha + ";" + window_topmost + ";" + window_borders);
                        t0_ms = parseInt(Date.now());
                        local_tick = 0;
                        ws_boucle = setInterval(ws_boucle_dashboard, 1000 / fps_dashboard);
                    }
                    break;

                case "fps_calculator":
                    fps_calculator = config.param["fps_calculator"];
                    if (broadcast == 0) {
                        clearInterval(ws_boucle);
                        ws.send("window;"+window_name+";" + window_x + ";" + window_y + ";" + window_w + ";" + window_h + ";" + window_alpha + ";" + window_topmost + ";" + window_borders);
                        ws_boucle = setInterval(function () {
                            if ((wait == 0) && (ws.bufferedAmount == 0)) {   // On v�rifie que tout a bien d�j� �t� envoy�
                                ws.send("22")
                            }
                        }, 1000 / fps_calculator);
                    }
                    break;

            }

            if (fullscreen_button_old == 0 && fullscreen_button == 1) {
                $("#fullscreen").css("display", "block");
                if (fullscreen_button_timeout > 0) {
                    setTimeout(function () {
                        $("#fullscreen").css("display", "none");
                    }, 1000 * fullscreen_button_timeout)
                }
            }

        }

        // On applique les modifications
        if (window_name == "JRT Timing" || window_name == "JRT Timing2" || window_name == "JRT Timing3" || window_name == "JRT Timing4" || window_name == "JRT Timing Broadcast") {
            //ws.send("window;"+window_name+";" + window_x + ";" + window_y + ";" + window_w + ";" + window_h + ";" + window_alpha + ";" + window_topmost + ";1");
            for (i = 0; i < 64; i++) {
                $("#canvas" + i).remove();
                $("#canvasB" + i).remove();
            }
            $("#trackmap_canvas").remove();
            $("#trackmap_fond_canvas").remove();
            $("#trackmap_fond_turns_canvas").remove();
            init_var();
            init_aff(disp_param);
            update_aff(disp_param);
            update_datas(-1);
            responsive_dim(disp_param);
            //console.log("***", loaded)
            if (loaded == 0) {
                loaded = 1;
            } else {
                loaded = -1;
            }
        }
        if (window_name == "JRT Trackmap" || window_name == "JRT Trackmap2" || window_name == "JRT Calculator") {
            //if(parseInt(Date.now()/1000) - 60 < donnees.tct) {  // Si la trackmap a �t� cr��e il y a moins de 60 s
                //console.log("redrawing trackmap ...", parseInt(Date.now()/1000) - 3, donnees.tct);
                $("#trackmap_canvas").remove();
                $("#trackmap_fond_canvas").remove();
                $("#trackmap_fond_turns_canvas").remove();
                init_var();
                responsive_dim();
            //}
        }
        if (window_name == "JRT Trackmap_3D") {
            //if(parseInt(Date.now()/1000) - 60 < donnees.tct) {  // Si la trackmap a �t� cr��e il y a moins de 60 s
                init_var();
    			draw_track("#ffffff", 1, 1, 1);
            //}
        }
        if (window_name == "JRT Dashboard" || window_name == "JRT Dashboard2" || window_name == "JRT Spotter") {
            //init_var();
            responsive_dim();
            sessiontime = 0;
        }

    }

    // Raffra�chissement des turns et du logo
    if ((config_page == "trackmap" || config_page == "trackmap2" || config_page == "trackmap_3d") && broadcast <= 1) {
        if(window_name == "JRT Timing" || window_name == "JRT Timing2" || window_name == "JRT Timing3" ||
            window_name == "JRT Timing4" || window_name == "JRT Timing Broadcast" || window_name == "JRT Trackmap" || window_name == "JRT Trackmap2" || window_name == "JRT Trackmap_3D") {

            for(config_varname in config.param) {
                //console.log(config_varname);

                switch(config_varname) {
                    case "turn_num": donnees.turn_num = config.param["turn_num"];break;
                    case "turn_ldp": donnees.turn_ldp = config.param["turn_ldp"];break;
                    case "turn_side": donnees.turn_side = config.param["turn_side"];break;
                    case "turn_info": donnees.turn_info = config.param["turn_info"];break;
                    case "orientation": donnees.orientation = config.param["orientation"];break;
                }

            }

            if(window_name == "JRT Trackmap" || window_name == "JRT Trackmap2" || window_name == "JRT Trackmap_3D") {
                if(window_name == "JRT Trackmap_3D") {
        			draw_track("#ffffff", 1, 1, 1);
                } else {
                    responsive_dim();
                }
            } else {
                responsive_dim(disp_param);
            }
        }
    }

}

function ws_boucle_compteur() {
    dt_ms = (parseInt(Date.now()) - t0_ms);
    //if (dt_ms > 1000 * 60) {  // on arr�te la connection si �a a fig� plus de 60 secondes
    if (false) {
        clearInterval(ws_boucle);
        console.log("WEBSOCKET CONNECTION CLOSED");
        ws.close;
    /*} else if (dt_ms > 1000 / fps * 1.5 && local_tick > 1) {
        //console.log("dt_ms = ", dt_ms, "/", 1000 / fps);
        //console.log(local_tick, "Refresh rate maybe too high ! If this message is not displayed repeatedly and if you don't have any issue, ignore it");
        clearInterval(ws_boucle);
        setTimeout(function () { t0_ms = parseInt(Date.now()); local_tick = 0; ws_boucle = setInterval(ws_boucle_compteur, 1000 / fps); } , dt_ms);*/
    } else {
        if ((wait == 0) && (ws.bufferedAmount == 0)) {   // On v�rifie que tout a bien d�j� �t� envoy�
            if (local_tick % fps == 0) {  // specify the refresh rate of the typ2 datas -> 1 time per second
                ws.send("32")
            } else {
                ws.send("33")
            }
            local_tick++;
            t0_ms = parseInt(Date.now());
        }
    }
}

function ws_boucle_dashboard() {
    dt_ms = (parseInt(Date.now()) - t0_ms);
    //console.log("dt_ms = ", dt_ms, "/", 1000 / fps_dashboard);
    //if (dt_ms > 1000 * 60) {  // on arr�te la connection si �a a fig� plus de 60 secondes
    if (false) {
        clearInterval(ws_boucle);
        console.log("WEBSOCKET CONNECTION CLOSED");
        ws.close;
    /*} else if (dt_ms > 1000 / fps_dashboard * 1.5 && local_tick > 1) {
        console.log(dt_ms + " ms instead of " + (1000 / fps_dashboard).toFixed(1) + " ms. Refresh rate maybe too high ! If this message is not displayed repeatedly and if you don't have any issue, ignore it");
        clearInterval(ws_boucle);
        setTimeout(function () { t0_ms = parseInt(Date.now()); local_tick = 0; ws_boucle = setInterval(ws_boucle_dashboard, 1000 / fps_dashboard); } , dt_ms);*/
    } else {
        if ((wait == 0) && (ws.bufferedAmount == 0)) {   // On v�rifie que tout a bien d�j� �t� envoy�
            if (local_tick % fps_dashboard == 0) {  // specify the refresh rate of the typ2 datas -> 1 time per second
                ws.send("32")
            } else {
                ws.send("33")
            }
            local_tick++;
            t0_ms = parseInt(Date.now());
        }
    }
}

function ws_boucle_trackmap() {
    dt_ms = (parseInt(Date.now()) - t0_ms);
    //if (dt_ms > 1000 * 60) {  // on arr�te la connection si �a a fig� plus de 60 secondes
    if (false) {
        clearInterval(ws_boucle);
        console.log("WEBSOCKET CONNECTION CLOSED");
        ws.close;
    /*}else if (dt_ms > 1000 / fps_trackmap * 1.5 && local_tick > 1) {
        //console.log("dt_ms = ", dt_ms, "/", 1000 / fps);
        //console.log(local_tick, "Refresh rate maybe too high ! If this message is not displayed repeatedly and if you don't have any issue, ignore it");
        clearInterval(ws_boucle);
        setTimeout(function () { t0_ms = parseInt(Date.now()); local_tick = 0; ws_boucle = setInterval(ws_boucle_trackmap, 1000 / fps_trackmap); } , dt_ms);*/
    } else {
        if ((wait == 0) && (ws.bufferedAmount == 0)) {   // On v�rifie que tout a bien d�j� �t� envoy�
            ws.send("12");
            local_tick++;
            t0_ms = parseInt(Date.now());
        }
    }
}

function ws_boucle_timing() {
    dt_ms = (parseInt(Date.now()) - t0_ms);
    //if (dt_ms > 1000 * 60) {  // on arr�te la connection si �a a fig� plus de 60 secondes
    if (false) {
        clearInterval(ws_boucle);
        console.log("WEBSOCKET CONNECTION CLOSED");
        ws.close;
    /*} else if (dt_ms > 1000 / fps * 3 && local_tick > 1) {
        console.log(dt_ms + " ms instead of " + (1000 / fps).toFixed(1) + " ms. Refresh rate maybe too high ! If this message is not displayed repeatedly and if you don't have any issue, ignore it");
        clearInterval(ws_boucle);
        setTimeout(function () { t0_ms = parseInt(Date.now()); local_tick = 0; ws_boucle = setInterval(ws_boucle_timing, 1000 / fps); } , dt_ms);*/
    } else {
        //console.log("OK")
        if (photo_processing == 0 && photo_processing_old == 0) {  // Important pour le mode photo
            if ((wait == 0) && (ws.bufferedAmount == 0)) {   // On v�rifie que tout a bien d�j� �t� envoy�
                if (fps >= 2) {
                    if (local_tick % fps == 0) {  // specify the refresh rate of the typ2 datas -> 1 time per second
                        //if (disp_events_ticker) {  // ATTENTION : si on enl�ve cette partie, les laptimes ne seront pas affich�s
                        if (disable_all_events == 0) {
                            ws.send("2;" + nb_events);
                        } else {
                            ws.send("2;-1")
                        }
                    } else {
                        ws.send("3")
                    }
                } else {
                    if (disable_all_events == 0) {
                        ws.send("2;" + nb_events);
                    } else {
                        ws.send("2;-1")
                    }
                }
                local_tick++;
                t0_ms = parseInt(Date.now());
            }
        } else {
            t0_ms = parseInt(Date.now());  // pour �viter d'arr�ter la connection si la photo dure plus de 60 secondes
        }
    }
}

function ws_boucle_spotter() {
    dt_ms = (parseInt(Date.now()) - t0_ms);
    //console.log("dt_ms = ", dt_ms, "/", 1000 / fps);
    //if (dt_ms > 1000 * 60) {  // on arr�te la connection si �a a fig� plus de 60 secondes
    if (false) {
        clearInterval(ws_boucle);
        console.log("WEBSOCKET CONNECTION CLOSED");
        ws.close;
    /*} else if (dt_ms > 1000 / fps * 1.5 && local_tick > 1) {
        //console.log(dt_ms + " ms instead of " + (1000 / fps).toFixed(1) + " ms. Refresh rate maybe too high ! If this message is not displayed repeatedly and if you don't have any issue, ignore it");
        clearInterval(ws_boucle);
        setTimeout(function () { t0_ms = parseInt(Date.now()); local_tick = 0; ws_boucle = setInterval(ws_boucle_spotter, 1000 / fps); } , dt_ms);*/
    } else {
        if (ws.bufferedAmount == 0) {   // On v�rifie que tout a bien d�j� �t� envoy�
            ws.send("spotter");
            local_tick++;
            t0_ms = parseInt(Date.now());
        }
    }
}
