function update_infosbar() {

    if (donnees.u != undefined) {
        speedfactor = donnees.u == 1 ? 1 : 1 / 1.609344;
        if (donnees.carname == "lotus79" || donnees.carname == "lotus49") {
            fuelfactor = donnees.u == 1 ? 1 : 1 / 4.54609;
        } else {
            fuelfactor = donnees.u == 1 ? 1 : 1 / 3.78541178;
        }
    }

    document.getElementById("tank").innerHTML = (fuelfactor * coef_fuel * donnees.f).toFixed(2);
    conso = 0;
    calcfuel_mode = donnees.fuel_use_avg;

    if (calcfuel_mode == 0) {
        conso = donnees.co;
        fuelneed = donnees.fn;
        text_conso = "(last lap)"
    } else {
        conso = donnees.co5;
        fuelneed = donnees.fn5;
        text_conso = "(5 laps)"
    }

    if (donnees.refuelspeed == 0) {
        donnees.refuelspeed = 1
    }
    if (fuelneed >= 5*donnees.refuelspeed) {
        fuelneed_bg1_pct = 1;
    } else if (fuelneed <= 0) {
        fuelneed_bg1_pct = 0;
    } else {
        fuelneed_bg1_pct = fuelneed / (5 * donnees.refuelspeed);
    }

    //fuelneed_bg1_pct = 0.75;

    if (responsive) {
        h = Math.floor(window.innerWidth / reference_w * ligne_h);
    } else {
        h = ligne_h
    }

    document.getElementById("fuelneed_bg1").style.top = Math.floor(0.5*h) + Math.floor(fuelneed_bg1_pct * (2*h - Math.floor(0.5*h))) + "px";
    document.getElementById("fuelneed_bg1").style.lineHeight = 2*h - Math.floor(0.5*h) - Math.floor(fuelneed_bg1_pct * (2*h - Math.floor(0.5*h))) + "px";

    if (donnees.estim_status == 0 || donnees.fuel_accurate != 1) {
        document.getElementById("fuelneed").style.color = "#666666"
    } else {
        if (fuelneed_bg1_pct == 0) {
            document.getElementById("fuelneed").style.color = "#ffffff"
        } else {
            document.getElementById("fuelneed").style.color = "#000000"
        }
    }

    if (donnees.fuel_accurate != 1) {
        document.getElementById("tank").style.color = "#bbbbbb";
        document.getElementById("conso").style.color = "#bbbbbb";
        document.getElementById("estlaps").style.color = "#555555";
    } else {
        document.getElementById("tank").style.color = "#ffffff";
        document.getElementById("conso").style.color = "#ffffff";
        document.getElementById("estlaps").style.color = "#000000";
    }

    if (conso > 0) {
        // On rajoute les tours de marge
        //fuelneed += fuel_spare_nblaps * conso;

        if (donnees.pro_v == 1 || donnees.try_v == 1) {
            document.getElementById("conso").innerHTML = (fuelfactor * coef_fuel * conso).toFixed(3) + " " + text_conso;
        } else {
            document.getElementById("conso").innerHTML = "buy pro";
        }
        //document.getElementById("estlaps").innerHTML = (donnees.f / conso).toFixed(1);
        if (donnees.estlaps != undefined && donnees.estlaps_bg1_pct != undefined) {
            if (donnees.pro_v == 1 || donnees.try_v == 1) {
                document.getElementById("estlaps").innerHTML = donnees.estlaps.toFixed(1);
            } else {
                document.getElementById("estlaps").innerHTML = "buy pro";
            }
            estlaps_bg1_pct = donnees.estlaps_bg1_pct;
        }
        document.getElementById("estlaps_bg1").style.width = 3*h*estlaps_bg1_pct + "px";
        //if (donnees.f / conso < 2) {
        if (donnees.f_alert == 1) {
            //document.getElementById("estlaps").style.backgroundColor = "#ee0000";
            document.getElementById("estlaps_bg0").style.backgroundColor = "#ee0000";
            document.getElementById("tank").style.backgroundColor = "#cc0000";
            document.getElementById("conso").style.backgroundColor = "#880000";
        } else {
            //document.getElementById("estlaps").style.backgroundColor = "#00aa00";
            document.getElementById("estlaps_bg0").style.backgroundColor = "#00aa00";
            document.getElementById("tank").style.backgroundColor = "#008800";
            document.getElementById("conso").style.backgroundColor = "#005500";
        }
    } else {
        document.getElementById("conso").innerHTML = "-- " + text_conso;
        document.getElementById("estlaps").innerHTML = "--";
    }

    if (donnees.tr != undefined && donnees.state != undefined) {
        if (donnees.state >= 4 && donnees.laps_l == 1 && donnees.tr != -1 && donnees.tr != -2 && donnees.tr != -3 && donnees.tr != "unlimited" && donnees.styp == "Race" && donnees.laps != "unlimited") {
            timeremain = "<div style='font-size: 0.75em; vertical-align: top; top: 25%;'>" + "Lap " + (donnees.lead_lc + 1) + "/" + donnees.laps + "</div>";
        } else {
            timeremain = reformat_timeremain(donnees.tr);
        }
        if (timeremain != timeremain_old) {  // on actualise que si la valeur a chang�
            document.getElementById("timeremain").innerHTML = timeremain;
            timeremain_old = timeremain;
        }
    }

    //if (selected_idxjs in donnees.d && donnees.c in donnees.d)
    if (donnees.c in donnees.d) {
        //if (donnees.d[selected_idxjs].lr < 32767)
        if (donnees.pro_v == 1 || donnees.try_v == 1) {
            document.getElementById("lapsremain").innerHTML = reformat_lapsremain(donnees.d[donnees.c].lr);
            if (donnees.lapsremain_bg1_pct != undefined) {
                lapsremain_bg1_pct = donnees.lapsremain_bg1_pct;
            }
            document.getElementById("lapsremain_bg1").style.width = 4 * h * lapsremain_bg1_pct + "px";

            if (donnees.gap_pct_lastlap != undefined && donnees.lead_lap != undefined) {
                gap_pct_lastlap = donnees.gap_pct_lastlap;
                //gap_pct_lastlap = 0.9999
                tmp_x = Math.floor(Math.min(4 * h - 2, 4 * h * gap_pct_lastlap));
                document.getElementById("lapsremain_bg2").style.left = lapsremain_bg2_left + tmp_x + "px";
                if (gap_pct_lastlap == 0) {
                    //document.getElementById("lapsremain_bg2").style.width = 0 + "px";
                } else {
                    document.getElementById("lapsremain_bg2").style.width = 2 + "px";
                }
                // Si on doit finir dans le m�me tour que le leader alors on n'affiche la gold line d'une autre couleur
                if (donnees.lead_lap == 1) {
                    document.getElementById("lapsremain_bg2").style.backgroundColor = "#0088ff";
                } else {
                    document.getElementById("lapsremain_bg2").style.backgroundColor = "#ffd700";
                }
            }

        } else {
            document.getElementById("lapsremain").innerHTML = "buy pro";
        }
    }
    // On affiche l'estimation du fuel requis uniquement si la conso a �t� calcul�e
    if (conso > 0.1)
        if (donnees.pro_v == 1 || donnees.try_v == 1) {
            if (fuelfactor * coef_fuel * fuelneed > 9999) {
                document.getElementById("fuelneed").innerHTML = "9999";
            } else if (fuelfactor * coef_fuel * fuelneed > 999) {
                document.getElementById("fuelneed").innerHTML = (fuelfactor * coef_fuel * fuelneed).toFixed(0);
            } else {
                document.getElementById("fuelneed").innerHTML = (fuelfactor * coef_fuel * fuelneed).toFixed(1);
            }
        } else {
            document.getElementById("fuelneed").innerHTML = "buy pro";
        }
    else
        document.getElementById("fuelneed").innerHTML = "--";

    // Affichage du type de session
    //document.getElementById("sessioninfos").innerHTML = type_session + " @ " + donnees.trackname;
    if (type_session == "Race" && name_session != "RACE") {
        document.getElementById("sessioninfos").innerHTML = name_session + ", ";
    } else {
        document.getElementById("sessioninfos").innerHTML = type_session + ", ";
    }


    // Affichage de la m�t�o

    //document.getElementById("sessioninfos").innerHTML += "<br>Weather: " + "<span style='font-style: italic; font-weight: bold'>" + donnees.weathertype + "</span>";
    document.getElementById("sessioninfos").innerHTML += reformat_skies(donnees.skies);

    if (donnees.u == 1) {
        str_speed = (donnees.windspeed * 3.6).toFixed(1) + " km/h";
    } else {
        str_speed = (donnees.windspeed * 3.6 / 1.609344).toFixed(1) + " MPH";
    }
    if ((donnees.airtemp != undefined) && (donnees.tracktemp != undefined) && (donnees.winddir != undefined) && (donnees.humidity != undefined) && (donnees.fog != undefined)) {
        if ((temperature_mode != 2) && ((donnees.u == 1 && temperature_mode == 0) || (temperature_mode == 1))) {  // systeme metric ou forc� en Celsius dans les options
            document.getElementById("sessioninfos").innerHTML += " " + "<span style='font-style: italic; font-weight: bold'>" + donnees.airtemp.toFixed(1) + "&degC</span>";
            document.getElementById("sessioninfos").innerHTML += ", track " + "<span style='font-style: italic; font-weight: bold'>" + donnees.tracktemp.toFixed(1) + "&degC</span>";
            document.getElementById("sessioninfos").innerHTML += ", Winds " + "<span style='font-style: italic; font-weight: bold'>" + reformat_winddir(donnees.winddir / Math.PI * 180) + " " + ((donnees.winddir / Math.PI * 180) % 360).toFixed(0) + "&deg</span>";
            document.getElementById("sessioninfos").innerHTML += " " + "<span style='font-style: italic; font-weight: bold'>" + str_speed + "</span>";
        } else {
            document.getElementById("sessioninfos").innerHTML += " " + "<span style='font-style: italic; font-weight: bold'>" + (donnees.airtemp * 1.8 + 32).toFixed(1) + "&degF</span>";
            document.getElementById("sessioninfos").innerHTML += ", track " + "<span style='font-style: italic; font-weight: bold'>" + (donnees.tracktemp * 1.8 + 32).toFixed(1) + "&degF</span>";
            document.getElementById("sessioninfos").innerHTML += ", Winds " + "<span style='font-style: italic; font-weight: bold'>" + reformat_winddir(donnees.winddir / Math.PI * 180) + " " + ((donnees.winddir / Math.PI * 180) % 360).toFixed(0) + "&deg</span>";
            document.getElementById("sessioninfos").innerHTML += " " + "<span style='font-style: italic; font-weight: bold'>" + str_speed + "</span>";
        }
        document.getElementById("sessioninfos").innerHTML += ", RH " + "<span style='font-style: italic; font-weight: bold'>" + (donnees.humidity * 100).toFixed(0) + "%</span>";
        //document.getElementById("sessioninfos").innerHTML += ", Pressure " + "<span style='font-style: italic; font-weight: bold'>" + donnees.airpress.toFixed(0) + " Hg</span>";
        //document.getElementById("sessioninfos").innerHTML += ", Density " + "<span style='font-style: italic; font-weight: bold'>" + donnees.airdens.toFixed(3) + " kg/m<sup>3</sup></span>";
        document.getElementById("sessioninfos").innerHTML += ", Fog " + "<span style='font-style: italic; font-weight: bold'>" + (donnees.fog * 100).toFixed(0) + "%</span>";
    }
    if (donnees.tod != undefined) document.getElementById("sessioninfos").innerHTML += ", " + donnees.tod;

    // ***
    //document.body.innerHTML = ", " + "<div style='color:#ffffff;font-style: italic; font-weight: bold'>" + donnees.weekendinfo + "</div>";


    // Affichage des SOF pour chaque classe et du SOF global
    if (donnees.sof != undefined && sof_displayed == 0) {
    //if (donnees.sof != undefined) {

        // ***.
        //console.log("SOF recalculated");

        sof_displayed = 1;
        if (responsive) {
            h = Math.floor(window.innerWidth / reference_w * ligne_h);
            h2 = (disp_sofbar * sofbar_h / ligne_h) * Math.floor(window.innerWidth / reference_w * ligne_h);
            fz = Math.floor(32 * sofbar_h / ligne_h * window.innerWidth / reference_w * ligne_h / 40)
        } else {
            h = ligne_h;
            h2 = (disp_sofbar * sofbar_h / ligne_h) * ligne_h;
            fz = Math.floor(32 * sofbar_h / 40)
        }
        h_ = h * infosbar_coef;
        document.getElementById("sof_cont").innerHTML = "<div onmousedown='class_selected="+0+";update_datas(-1);' style='z-index:6;color:#ffffff;padding-left:8px;padding-right:8px;" +
            "display:inline-block;position:relative;left:0;line-height:" + (2 * h_ - Math.floor(0.5 * h_)) + "px;background-color:#000000'>" +
            "<div style='text-align:center;font-size:" + 14 * h_ / 40 + "px;line-height:" + (Math.floor(0.5 * h_)) + "px'>ALL ("+ donnees.nb+")</div>" +
            "<div style='text-align:center;line-height:" + (2 * h_ - 1 * Math.floor(0.5 * h_)) + "px'>" + donnees.sof[0] +
            "</div></div>";
        document.getElementById("sofbar").innerHTML = "<div onclick='class_selected="+0+";update_datas(-1);' style='z-index:6;font-size:" + fz + "px;padding-left:8px;" +
            "padding-right:8px;display:inline-block;position:relative;left:0;top:0;line-height:" + h2 + "px;" +
            "background-color:#000000'>ALL ("+ donnees.nbcars_class[0]+"): " + donnees.sof[0] + "</div>";
        nb_classes = 0;
        for (c in donnees.classes) nb_classes += 1
        if (nb_classes > 1) {
            for (c in donnees.classes) {
                str = donnees.carclasscolor[c];
                if (str == "0x0") str = "0xaaaaaa";
                str = str.slice(2);
                for (n = str.length; n < 6; n++) {
                    str = "0" + str
                }

                a1 = "";
                a2 = "";
                if (f3_box == 0) {
                    a1 += "<div onmousedown='class_selected=" + c + ";update_datas(-1);'";
                    a2 += "<div onclick='class_selected=" + c + ";update_datas(-1);'";
                } else {
                    a1 += "<div ";
                    a2 += "<div ";
                }
                a1 += " style='z-index:6;padding-left:8px;padding-right:8px;" +
                    "display:inline-block;position:relative;left:0;top:0;line-height:" + (2 * h_ - Math.floor(0.5 * h_)) + "px;background-color:#" + str + "'>" +
                    "<div style='text-align:center;font-size:" + 14 * h_ / 40 + "px;line-height:" + Math.floor(0.5 * h_) + "px'>" + donnees.classname[c] +
                    " ("+ donnees.nbcars_class[c]+")</div>" +
                    "<div style='text-align:center;line-height:" + (2 * h_ - 1 * Math.floor(0.5 * h_)) + "px'>" + donnees.sof[c] + "</div></div>";
                a2 += " style='z-index:6;color:#000000; font-size:" + fz + "px;padding-left:8px;" +
                    "padding-right:8px;display:inline-block;position:relative;left:0;top:0;line-height:" + h2 + "px;" +
                    "background-color:#" + str + "'>" + donnees.classname[c] + " ("+ donnees.nbcars_class[c]+") : " + donnees.sof[c] + "</div>";
                document.getElementById("sof_cont").innerHTML += a1;
                document.getElementById("sofbar").innerHTML += a2;
            }
        }

        /*document.getElementById("sof_cont").innerHTML += "<div style='padding-left:4px;padding-right:0px;" +
            "display:inline-block;position:relative;left:0;top:0;line-height:" + (2 * h - Math.floor(0.5 * h)) + "px;background-color:#" + "#000000" + "'>" +
            "<div style='color: rgba(255,255,255,0.5); text-align:center;font-size:" + 14 * h / 40 + "px;line-height:" + Math.floor(0.5 * h) + "px'>" + "FLAG" + "</div>" +
            "<div style='text-align:center;line-height:" + (2 * h - 2 * Math.floor(0.5 * h)) + "px'>" + "&nbsp" + "</div></div>";*/
    }


    // En fonction du syst�me d'unit� d'iRacing on passe de litre/kg � Gallon/livre
    if (donnees.u == 1) {
        document.getElementById("litre").innerHTML = "L";
        document.getElementById("kg").innerHTML = "Kg"
    } else {
        document.getElementById("litre").innerHTML = "Ga";
        document.getElementById("kg").innerHTML = "Lb"
    }

    // Couleurs des �l�ments dynamiques de la barre d'infos (L, Kg, Tank et Flag-typesession-sof-circuit
    if (coef_fuel ==1) {
        document.getElementById("litre").style.backgroundColor = "#dddddd";
        document.getElementById("litre").style.color = "#000000";
        document.getElementById("kg").style.backgroundColor = "#999999";
        document.getElementById("kg").style.color = "#555555";
    } else {
        document.getElementById("litre").style.backgroundColor = "#999999";
        document.getElementById("litre").style.color = "#555555";
        document.getElementById("kg").style.backgroundColor = "#dddddd";
        document.getElementById("kg").style.color = "#000000";
    }

    if (donnees.flag != undefined) {
        if (donnees.flag.slice(-4, -3) == "1") bg = "#ffff00";       // yellow
        else if (donnees.flag.slice(-9, -8) == "1") bg = "#ffff00";  // yellow waving
        else if (donnees.flag.slice(-15, -14) == "1") bg = "#ffff00";  // caution
        else if (donnees.flag.slice(-16, -15) == "1") bg = "#ffff00";  // caution waving
        else if (donnees.flag.slice(-6, -5) == "1") bg = "#0000ff";  // blue
        else if (donnees.flag.slice(-2, -1) == "1") bg = "#ffffff";  // white
        else if (donnees.flag.slice(-3, -2) == "1") bg = "#00ff00";  // green
        else if (donnees.flag.slice(-11, -10) == "1") bg = "#00ff00";  // green held
        else bg = "#999999";                                        // autres
    } else {
        bg = "#333333";
    }

    /*
    else if (donnees.flag == "0x00000002") bg = "#999999";  // checkered
    else if (donnees.flag == "0x00000008") bg = "#ff0000";  // red
    else if (donnees.flag == "0x00000100") bg = "#ffffff";  // one lap to green
    else if (donnees.flag == "0x00010000") bg = "#000000";  // black
    */

    //document.getElementById("sessioninfos").style.backgroundColor = bg;
    document.getElementById("sof_cont").style.backgroundColor = bg;

    // Si on peut pitter sans risquer de devoir faire un pit de plus on met le fond en mauve
    // Estimation d'abord du nombre de pits restants
    if (fuelneed > 0 && donnees.tcap > 0) {
        //p = Math.floor((fuelneed - 2*conso) / donnees.tcap) + 1;
        p = ((fuelneed - 2*conso) / donnees.tcap) + 1;
        p = parseFloat(p).toFixed(2);
        if (p < 0)
            p = 0;
    } else {
        p = 0;
    }
    if (conso > 0)
        document.getElementById("fuelneed_h").innerHTML = "Fuel to add (" + p + " pits)";
    else
        document.getElementById("fuelneed_h").innerHTML = "Fuel to add ( --  pits)";

    if (donnees.estim_status == 0) {
        document.getElementById("fuelneed_bg0").style.backgroundColor = "#999999";
        //document.getElementById("fuelneed_bg1").style.backgroundColor = "#999999"
        document.getElementById("fuelneed_bg1").style.backgroundColor = "#0099ff";
    } else {
        document.getElementById("fuelneed_bg1").style.backgroundColor = "#0099ff";
        if (Math.floor(p) > 0 && fuelneed < donnees.tcap * Math.floor(p) - donnees.f) {  //
            document.getElementById("fuelneed_bg0").style.backgroundColor = "#ff99ff"
        } else {
            document.getElementById("fuelneed_bg0").style.backgroundColor = "#ff9900"
        }
    }

    if (donnees.estim_status == 2 && donnees.tr >= 0 && donnees.laps_l != 1) {
        document.getElementById("timeremain").style.color = "#333333";
    } else {
        document.getElementById("timeremain").style.color = "#ffffff";
    }

    // Mise � jour de la banni�re
    if (type_session == "Race" && name_session != "RACE") {
        ban = name_session + ", ";
    } else {
        ban = type_session + ", ";
    }

    if (donnees.laps != "unlimited") {
        ban += donnees.laps + " laps";
    } else {
        ban += reformat_timeremain(donnees.time);
    }
    ban += " - " + donnees.date + "<br>";
    if (donnees.trackconfig != null) {
        ban += donnees.trackname + " - " + donnees.trackconfig + "<br>" + donnees.carscreenname;
    } else {
        ban += donnees.trackname + "<br>" + donnees.carscreenname;
    }

    decalage = (parseInt(Date.now()/1000) - donnees.tstamp);
    if (decalage > 120) {  // au-del� de 2 minutes de d�calage on affiche OFFLINE
        $("#banner_live").html("OFFLINE");
        $("#banner_live").css("background-color", "#dd0000");
    } else {
        $("#banner_live").html("LIVE");
        $("#banner_live").css("background-color", "#00dd00");
    }

    $("#banner_text").html(ban);

}
