
function opt_north_edit(elt) {
    if (elt.checked) {
        north_edit = 1;
        turn_edit = 0;
        document.getElementById("north").style.display = "block"
        document.getElementById("lapdistpct").style.display = "none"
        document.getElementById("opt_turn_edit").checked = false
    } else {
        north_edit = 0;
        document.getElementById("north").style.display = "none"
        draw_track("#2c2c2c", 1, 1, 1);
    }
    change_turn_edit_ldp()
}


function opt_turn_edit(elt) {
    if (elt.checked) {
        turn_edit = 1;
        north_edit = 0;
        document.getElementById("lapdistpct").style.display = "block"
        document.getElementById("north").style.display = "none"
        document.getElementById("opt_north_edit").checked = false
    } else {
        turn_edit = 0;
        document.getElementById("lapdistpct").style.display = "none"
        draw_track("#2c2c2c", 1, 1, 1);
    }
    change_turn_edit_ldp()
}



// Permet de faire bouger le nord ou la position sur la piste avec la roulette de la souris
function change_turn_edit_ldp(event) {
    //mouseX = event.clientX;
    //mouseY = event.clientY;
    //console.log(event.wheelDelta);

    //d = event.wheelDelta;
    if (event != undefined) {
        event = event.originalEvent;
        d = event.wheelDelta > 0 || event.detail < 0 ? 1 : -1;
    }

    if (turn_edit == 1) {
        //ldp = (mouseX - 0.05 * window.innerWidth) / window.innerWidth / 0.9;
        if (event != undefined) {
            //turn_edit_ldp += d / 120 / 1000;
            turn_edit_ldp += d / 1000;
        }
        if (turn_edit_ldp > 1) turn_edit_ldp = 0;
        if (turn_edit_ldp < 0) turn_edit_ldp = 0.999;
        document.getElementById("lapdistpct").innerHTML = "<span style='padding-left:0.25em;color:#ff0000;" +
            "font-weight:bold;font-size:2em;'>" +
            "lapdistpct = " + turn_edit_ldp.toFixed(3) + "</span>";
        trackmap_context.clearRect(0, 0, container_w - ttl, container_h);
        draw_turn_edit("#ff6666", "13", turn_edit_ldp, -1,"Lucky Turn (left: -1)");
        draw_turn_edit("#00ccff", "13", turn_edit_ldp, 1,"Lucky Turn (right : 1)")
    }

    if (north_edit == 1) {
        //ldp = (mouseX - 0.05 * window.innerWidth) / window.innerWidth / 0.9;
        if (event != undefined) {
            //north_edit_rad += d / 120 * Math.PI / 240;
            north_edit_rad += d * Math.PI / 300;
        }
        if (north_edit_rad > Math.PI) north_edit_rad = - Math.PI;
        if (north_edit_rad < - Math.PI) north_edit_rad = Math.PI;
        //console.log(north_edit_rad);
        document.getElementById("north").innerHTML = "<span style='padding-left:0.25em;color:#ff0000;" +
            "font-weight:bold;font-size:2em;'>" +
            "north (in rad) = " + north_edit_rad.toFixed(2) + "</span>";
        trackmap_context.clearRect(0, 0, container_w - ttl, container_h);
        draw_north_edit(north_edit_rad)
    }
}


// Calcule des coefficient de la fonction permettant de lisser les mouvements des voitures
function calc_coef(x0, x1, y0, y1, coef_old) {
    var coef = {}
    a = coef_old["a"];
    b = coef_old["b"];
    c = coef_old["c"];
    /*var alpha0 = 3 * a * x0*x0 + 2 * b * x0 + c;
    var alpha1 = 0;
    if (x1 - x0 != 0)
        alpha1 = (y1 - y0)/(x1 - x0);
    coef["a"] = 2*(alpha1-alpha0)/(alpha1-alpha0-2*x0*x1-(x0+x1)*(x0+x1));
    coef["b"] = (alpha1-alpha0-3*(x1-x0)*(x1+x0)*coef["a"])/(2*(x1-x0));
    coef["c"] = alpha0 - 3*coef["a"]*x0*x0-3*coef["b"]*x0;
    coef["d"] = y0-coef["a"]*x0*x0*x0-coef["b"]*x0*x0-coef["c"]*x0;*/

    // Interpolation lin�aire
    if (x0 - x1 != 0) {
        coef["a"] = (y0 - y1) / (x0 - x1);
    } else {
        coef["a"] = 0;
    }
    coef["b"] = y0 - coef["a"] * x0;
    return coef
}
//JSON.parse(JSON.stringify(coef_[i]));  // copie de l'objet


// Affiche tous les pilotes sur la trackmap
function trackmap() {

    var w = parseInt($("#trackmap_canvas").css("width"));
    var h = parseInt($("#trackmap_canvas").css("height"));

    init_colorize();

    if (donnees["expired"] == 1) {
        $("#expired").css("display", "block");
    }

    me = donnees.me;

    //document.getElementById("pexit").innerHTML = "Projected track position after pit : " + (donnees.pexit).toFixed(4);

    // On efface les pilotes avant de les redessiner
    if (turn_edit == 0 && north_edit == 0) {
        trackmap_context.clearRect(0, 0, w, h);
    }

    if (selected_idxjs != undefined && selected_idxjs in donnees.d && turn_edit == 0 && north_edit == 0) {

        dp = donnees.d[selected_idxjs].dp;

        i = selected_idxjs;
        ldp = dp - Math.floor(dp);

        trackmap_context.globalAlpha=1;
        if (donnees.d[selected_idxjs].fr == 0) {
            //driver_on_trackmap("rgba(255,255,255,0.75)", ldp, selected_idxjs, 2.5, 1, 1);
            driver_on_trackmap("rgba(255,255,255,0.75)", ldp, selected_idxjs, 2.5, 0, 2.5);
        }

        //***
        //donnees.co = 4;
        //donnees.plost = 62.3;
        //donnees.pexit = 34.42;


        if (donnees.pexit != undefined && selected_idxjs == me && donnees.co > 0 && donnees.d[selected_idxjs].fr == 0) {
            dp_exit = donnees.pexit;
            ldp_exit = dp_exit - Math.floor(dp_exit);
            if (selected_idxjs == me && donnees.co > 0 && donnees.d[selected_idxjs].fr == 0) {  // On affiche l'estimation apr�s la sortie des pits que pour le pilote local et s'il on a les infos de fuel
                if (donnees.plost != undefined) {
                    if (trackmap_disp_timelost) {
                        document.getElementById("plost").innerHTML = "<span style='-webkit-filter: drop-shadow(1px 1px 1px black);filter: drop-shadow(1px 1px 1px black);" +
                                //"background-color:rgba(90,90,90,0.8);" +
                            "padding:0.5em;" +
                            "font-weight:bold;color:#ff8800; margin-left:0.0em'>Time Lost Next PIT : " + (donnees.plost).toFixed(1) + "s</span>";
                        //console.log(donnees.pexit)
                    }
                    if (donnees.plost > 15) { // on n'affiche le point orange que si le plost est sup�rieur � 15 s
                        driver_on_trackmap("rgba(255,128,0,0.75)", ldp_exit, selected_idxjs, 2.5, 1, 1);
                    } else {
                        dp_exit = -1;
                    }
                }
            }
        } else {
            dp_exit = -1;
        }

        // Si je suis dans les pits, on change ma couleur en rajoutant une couche grise dessus
        trackmap_context.globalAlpha=0.666;
        if (donnees.d[selected_idxjs].p) {
            driver_on_trackmap("#666666", ldp, selected_idxjs, 2.95, 1, 1);
        }
    } else {
        dp_exit = -1;  // en dernier recours on donne une valeur � dp_exit si jamais on n'acc�de pas aux donn�es
    }


    // Affichage des autres pilotes
    for (i in donnees.d) {
        // On affiche les autres pilotes encore en piste
        if ((donnees.d[i].fr == 0) && (donnees.d[i].pr == 1 || donnees.d[i].s > 1 || donnees.d[i].cts != -1) && turn_edit == 0 && north_edit == 0) {

            dp = donnees.d[i].dp;

            ldp = dp - Math.floor(dp);
            //coul = "rgba(255,56,211,0.5)"; // couleur de base s'il n'y a qu'une seule class

            if (donnees.teamracing) {
                nom = donnees.d[i].tn;
                id = donnees.d[i].tid;
            } else {
                nom = donnees.d[i].name;
                id = donnees.d[i].uid;
            }
            // On colorize le pilote avec les donn�es du fichier _colorize.js (m�me couleur que dans le timing)
            if (id in colorize_) {
                trackmap_context.globalAlpha = 0.80;
                driver_on_trackmap(colorize_[id], ldp, i, 2.5, 0, 2.5);
            }

            // Si on n'a pas pu calculer le dp_exit auparavant on prend la position du pilote s�lectionn�
            if ((me != selected_idxjs || dp_exit < 0) && selected_idxjs in donnees.d) {
                dp_exit = donnees.d[selected_idxjs].dp
            }

            if (donnees.d[i].cts != -1 || donnees.d[i].pr != 1) {  // si un pilote est aux pits et a quitt� le jeu on ne l'affiche pas

                if (i != selected_idxjs) {
                    trackmap_context.globalAlpha = 0.75;
                    // Si le pilote a un tour de retard on rajoute du bleu, et s'il a un tour d'avance on met du rouge
                    if (donnees.d[i].dp - dp_exit > 0.5)
                        driver_on_trackmap("#ff4444", ldp, i, 1.5, 0, 1.5);
                    if (donnees.d[i].dp - dp_exit < -0.5)
                        driver_on_trackmap("#0099ff", ldp, i, 1.5, 0, 1.5);
                }

                // Couleur de la class du pilote
                str = donnees.d[i].cc;
                if (str == "0xffffff" || str == "0x0") str = "0xbbeeff"; // couleur de base si une seule class
                if (str != undefined) {
                    if (str != "0xffffff" && str != "0x0") {
                        str = str.slice(2);
                        for (n = str.length; n < 6; n++) {
                            str = "0" + str
                        }
                    }
                }
                trackmap_context.globalAlpha = 0.65;
                coul = "#" + str;

                driver_on_trackmap(coul, ldp, i, 1, 1, 1);

                if (trackmap_disp_mode == 0) {
                    if (selected_idxjs in donnees.d) {
                        trackmap_context.globalAlpha = 1;
                        // Si le pilote a pitt� plus tard on le marque d'un petit point noir (seulement si plus d'un tour d'�cart)
                        // A condition qu'on soit dans la m�me class
                        if (donnees.d[selected_idxjs].cc == donnees.d[i].cc && donnees.d[i].sti < donnees.d[selected_idxjs].sti - 1)
                            driver_on_trackmap("#000000", ldp, i, 0.33, 1, 1);
                    }
                }

                if (donnees.d[i].s < 20 && !(donnees.d[i].pr)) {  // Les pilotes quasiments arr�t�s sur les piste on les met en jaune
                    coul = "rgba(255,255,0,0.75)";
                    driver_on_trackmap(coul, ldp, i, 2.5, 0, 2.5);
                }

                if (donnees.d[i].cts == -1 && i != selected_idxjs) {  // On indique les pilotes d�connect�s en les grisant
                    coul = "rgba(32,32,32,0.5)";
                    driver_on_trackmap(coul, ldp, i, 1.65, 1, 1);
                }

            }
        }
    }

    draw_winddir(donnees.north, donnees.winddir)
}


function driver_on_trackmap(coul, ldp, caridx, taille, plein, epaisseur_trait) {
    var w = 0;
    k = Math.floor(donnees.coef_k * ldp);
    d = donnees.coef_k * ldp;
    if (k >= donnees.k_max) k = 0;
    k2 = k + 1;
    if (k2 >= donnees.k_max) k2 = 0;

    if (k in track.x && k in track.y && donnees.k_max > 0) {

        rayon = taille * track_epaisseur / 2;
        if (trackmap_disp_mode != 0) {  // on double le rayon si on affiche le n� ou les 3 premi�res lettres
            rayon = 2 * rayon;
        }

        x1 = (-track.x[k] + track_max_x) * track_mult;
        y1 = (-track.y[k] + track_max_y) * track_mult;
        x2 = (-track.x[k2] + track_max_x) * track_mult;
        y2 = (-track.y[k2] + track_max_y) * track_mult;

        // On fait une interpolation
        x = (container_w - ttl - track_w - 17)/2 + x1 + (x2 - x1) * (d - k);
        y = (container_h - track_h)/2 + y1 + (y2 - y1) * (d - k);

        l = Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));

        // Indication du premier de la classe
        if (donnees.styp == "Race") {
            classpos = donnees.d[caridx].cpos;
        } else {
            classpos = donnees.d[caridx].cposbest;
        }

        if (plein != 0 && taille == 1 && classpos == 1) {
            if (trackmap_disp_mode != 0) {
                rayon2 = rayon / 2;
                tmp_coef1 = 4/3;
                tmp_coef2 = 1.92;
                tmp_coef3 = 1.64;
            } else {
                rayon2 = rayon;
                tmp_coef = 1;
                tmp_coef1 = 1;
                tmp_coef2 = 1;
                tmp_coef3 = 1;
            }
            xp1 = x + 1.5*(y2 - y1) / l * track_epaisseur * 1.5 * tmp_coef1;
            yp1 = y - 1.5*(x2 - x1) / l * track_epaisseur * 1.5 * tmp_coef1;
            xt1 = x + 0.35*(y2 - y1) / l * track_epaisseur * 1.5 * tmp_coef2;
            yt1 = y - 0.35*(x2 - x1) / l * track_epaisseur * 1.5 * tmp_coef2;
            xt2 = x + 0.75*(y2 - y1) / l * track_epaisseur * 1.5 * tmp_coef3;
            yt2 = y - 0.75*(x2 - x1) / l * track_epaisseur * 1.5 * tmp_coef3;
            trackmap_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_context.strokeStyle = "#000000";
            trackmap_context.lineWidth = 3*rayon2/7*epaisseur_trait;
            trackmap_context.moveTo(xt1, yt1);//On se d�place au coin inf�rieur gauche
            trackmap_context.lineTo(xt2, yt2);
            trackmap_context.stroke(); //On trace seulement les lignes.
            trackmap_context.closePath();
            trackmap_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_context.arc(xp1, yp1, 2*rayon2, 0, Math.PI * 2); //On trace la courbe d�limitant notre forme
            trackmap_context.fillStyle = coul;
            trackmap_context.fill(); //On utilise la m�thode fill(); si l'on veut une forme pleine
            trackmap_context.closePath();
            trackmap_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_context.arc(xp1, yp1, 2.1*rayon2, 0, Math.PI * 2); //On trace la courbe d�limitant notre forme
            trackmap_context.strokeStyle = "#000000";
            trackmap_context.lineWidth = 3*rayon2/7*epaisseur_trait;
            trackmap_context.stroke(); //On utilise la m�thode fill(); si l'on veut une forme pleine
            trackmap_context.closePath();
            trackmap_context.fillStyle = "#000000";
            trackmap_context.font = "bold " + 2.5 * rayon2 + "px Arial";
            trackmap_context.textAlign = "center";
            trackmap_context.fillText("P1", xp1, yp1 + 0.75*rayon2);
        }


        // Si le driver est dans les stands, on le d�cale sur le c�t� de la piste
        if (donnees.d[caridx].pr && l != 0) {
            decale_x = -(y2 - y1) / l * track_epaisseur * 1.5;
            decale_y = (x2 - x1) / l * track_epaisseur * 1.5;
        } else {
            decale_x = 0;
            decale_y = 0
        }
        x += decale_x;
        y += decale_y;

        trackmap_context.beginPath(); //On d�marre un nouveau trac�.
        trackmap_context.arc(x, y, rayon, 0, Math.PI * 2); //On trace la courbe d�limitant notre forme
        trackmap_context.fillStyle = coul;
        trackmap_context.strokeStyle = coul;
        trackmap_context.lineWidth = rayon/7*epaisseur_trait;
        if (plein)
            trackmap_context.fill(); //On utilise la m�thode fill(); si l'on veut une forme pleine
        else
            trackmap_context.stroke(); //On utilise la m�thode fill(); si l'on veut une forme pleine
        trackmap_context.closePath();

        // Si le driver est dans les stands, on le grise
        if (donnees.d[caridx].pr && l != 0 && caridx != selected_idxjs && plein != 0 && taille == 1) {
            trackmap_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_context.arc(x, y, rayon*1.65, 0, Math.PI * 2); //On trace la courbe d�limitant notre forme
            trackmap_context.fillStyle = "#666666";
            trackmap_context.fill(); //On utilise la m�thode fill(); si l'on veut une forme pleine
            trackmap_context.closePath();
        }

        // On �crit le num�ros du pilote ou son nom en fonction du mode choisi
        if (plein != 0 && taille == 1 && trackmap_disp_mode != 0) {
            trackmap_context.globalAlpha = 1;
            trackmap_context.fillStyle = "#FFFFFF";
            trackmap_context.textAlign = "center";
            // Ombrage
            trackmap_context.shadowColor = "black";
            trackmap_context.shadowOffsetX = 0;
            trackmap_context.shadowOffsetY = 0;
            trackmap_context.shadowBlur = rayon/3;
            //
            if (trackmap_disp_mode == 1) {
                trackmap_context.font = "bold " + 1.5 * rayon + "px Arial";
                if (donnees.d[caridx].num != undefined) {
                    trackmap_context.fillText(donnees.d[caridx].num, x, y + 0.5 * rayon);
                }
            }
            if (trackmap_disp_mode == 2) {
                name = donnees.d[caridx].name;
                nom_ = name.split(" ");
                nom = nom_[nom_.length - 1].toUpperCase();
                name = "";
                for (i = 0; i < 3; i++) {
                    if (nom[i] != undefined) {
                        name += nom[i];
                    }
                }
                trackmap_context.font = "bold " + 1 * rayon + "px Arial";
                trackmap_context.fillText(name, x, y + 0.4 * rayon);
            }
            if (trackmap_disp_mode == 3) {
                trackmap_context.font = "bold " + 1.5 * rayon + "px Arial";
                if (classpos != undefined) {
                    trackmap_context.fillText(classpos, x, y + 0.5 * rayon);
                }
            }
            trackmap_context.globalAlpha = 0.65;
            // On enl�ve l'ombrage
            trackmap_context.shadowOffsetX = 0;
            trackmap_context.shadowOffsetY = 0;
            trackmap_context.shadowBlur = 0;
        }

    }
}


function draw_track(coul, opac, epaisseur, efface) {
    coul = trackmap_color;
    var w = 0;
    var h = 0;
    cv_w = (container_w - ttl - 17) * 0.82;
    cv_h = container_h * 0.82;
    if (efface)
        trackmap_fond_context.clearRect(0, 0, container_w - ttl, container_h);
        trackmap_fond_turns_context.clearRect(0, 0, container_w - ttl, container_h);
    trackmap_fond_context.globalAlpha = opac;
    trackmap_fond_turns_context.globalAlpha = opac;

    if (donnees.k_max > 0) {

        if (donnees.orientation == "auto" || donnees.orientation == undefined) {
            angle = optimize_track_angle();
        } else {
            angle = donnees.orientation * Math.PI / 180;
        }

        // ***
        //angle = 0

        // Caract�ristiques du circuit � dessiner
        for (k = 0; k < donnees.k_max; k++) {
            track.x[k] = donnees.x[k] * Math.cos(angle) - donnees.y[k] * Math.sin(angle);
            track.y[k] = donnees.x[k] * Math.sin(angle) + donnees.y[k] * Math.cos(angle);
            if (k == 0) {
                track_max_x = track.x[0];
                track_min_x = track.x[0];
                track_max_y = track.y[0];
                track_min_y = track.y[0]
            } else {
                if (track.x[k] < track_min_x)
                    track_min_x = track.x[k];
                if (track.y[k] < track_min_y)
                    track_min_y = track.y[k];
                if (track.x[k] > track_max_x)
                    track_max_x = track.x[k];
                if (track.y[k] > track_max_y)
                    track_max_y = track.y[k]
            }
        }

        w = track_max_x - track_min_x;
        h = track_max_y - track_min_y;
        if (w == 0 || h == 0) {
            w = 1;
            h = 1
        }
        mult = cv_w / w;
        mult_h = cv_h / h;
        if (mult_h < mult) mult = mult_h;

        track_w = (track_max_x - track_min_x) * mult;
        track_h = (track_max_y - track_min_y) * mult;

        //track_maxlength = track_w;
        //if (track_h > track_w)
        //    track_maxlength = track_h;
        track_maxlength = Math.sqrt(cv_w*cv_w + cv_h*cv_h);  // L'�paisseur de la trackmap d�pend de la longueur de la diagonale

        track_epaisseur = trackmap_thickness_coef * epaisseur * track_maxlength / 60;
        track_mult = mult;

        rayon = track_maxlength / 75;
        // Ombrage du circuit
        trackmap_fond_context.shadowColor = "black";
        trackmap_fond_context.shadowOffsetX = rayon/6;
        trackmap_fond_context.shadowOffsetY = rayon/6;
        trackmap_fond_context.shadowBlur = rayon/2;

        // Ombrage des noms des virages du circuit
        trackmap_fond_turns_context.shadowColor = "black";
        trackmap_fond_turns_context.shadowOffsetX = rayon/6;
        trackmap_fond_turns_context.shadowOffsetY = rayon/6;
        trackmap_fond_turns_context.shadowBlur = rayon/2;

        trackmap_fond_context.beginPath(); //On d�marre un nouveau trac�.
        trackmap_fond_context.lineWidth = track_epaisseur;
        trackmap_fond_context.lineJoin = "round";
        trackmap_fond_context.strokeStyle = coul;
        for (k = 0; k < donnees.k_max; k++) {
            k2 = k + 1;
            if (k2 >= donnees.k_max) k2 = 0;

            if (k in track.x && k in track.y && donnees.k_max > 0) {
                x = (container_w - ttl - track_w - 17) / 2 + (-track.x[k] + track_max_x) * track_mult;
                y = (container_h - track_h) / 2 + (-track.y[k] + track_max_y) * track_mult;

                if (k == 0) {
                    trackmap_fond_context.moveTo(x, y);//On se d�place au coin inf�rieur gauche
                    x0 = x;
                    y0 = y;
                } else if (k == 1) {
                    x1 = x;
                    y1 = y
                }
                if (k == 2) {
                    x2 = x;
                    y2 = y
                }
                trackmap_fond_context.lineTo(x, y);
            }
        }
        trackmap_fond_context.lineTo(x0, y0);
        trackmap_fond_context.lineTo(x1, y1);
        trackmap_fond_context.stroke(); //On trace seulement les lignes.
        trackmap_fond_context.closePath();


        l = Math.sqrt((x2-x0)*(x2-x0) + (y2-y0)*(y2-y0));
        if (l != 0) {
            x1 = x0 + (x2 - x0) / l * track_epaisseur / 2.5;
            y1 = y0 + (y2 - y0) / l * track_epaisseur / 2.5;
            trackmap_fond_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_fond_context.strokeStyle = "#ff0000"; // couleur de la ligne d'arriv�e
            trackmap_fond_context.lineWidth = epaisseur * track_maxlength / 60 * 2.5;
            trackmap_fond_context.moveTo(x0, y0);
            trackmap_fond_context.lineTo(x1, y1);
            trackmap_fond_context.stroke(); //On trace seulement les lignes.
            trackmap_fond_context.closePath();
            /*trackmap_fond_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_fond_context.strokeStyle = "#ffffff"; // couleur de la ligne d'arriv�e
            trackmap_fond_context.lineWidth = epaisseur * track_maxlength / 60;
            trackmap_fond_context.moveTo(x0, y0);
            trackmap_fond_context.lineTo(x1, y1);

            trackmap_fond_context.stroke(); //On trace seulement les lignes.
            trackmap_fond_context.closePath();*/
        }

        // On place les noms des virages
        for (y in donnees.turn_num) {
            if (y in donnees.turn_info) {
                info = donnees.turn_info[y]
            } else {
                info = ""
            }
            //draw_turn(coul, turn, donnees.turn_ldp[turn], donnees.turn_side[turn], info);
            draw_turn("#ffffff", donnees.turn_num[y], donnees.turn_ldp[y], donnees.turn_side[y], info);
        }

        draw_fleche();
        draw_north(donnees.north);

        // On enl�ve l'ombrage
        trackmap_fond_context.shadowOffsetX = 0;
        trackmap_fond_context.shadowOffsetY = 0;
        trackmap_fond_context.shadowBlur = 0;

        trackmap_fond_turns_context.shadowOffsetX = 0;
        trackmap_fond_turns_context.shadowOffsetY = 0;
        trackmap_fond_turns_context.shadowBlur = 0;

        trackmap_loaded = 1;




        // *** Trac� des secteurs MM ***
        /*for (k = 0; k < donnees.k_max; k++) {
            trackmap_fond_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_fond_context.lineWidth = track_epaisseur;
            trackmap_fond_context.lineJoin = "round";
            //trackmap_fond_context.strokeStyle = coul;
            if (k % 2 == 0)
                trackmap_fond_context.strokeStyle = "#000000";
            else
                trackmap_fond_context.strokeStyle = "#00ff00";
            k2 = k + 1;
            if (k2 >= donnees.k_max) k2 = 0;


            if (k in track.x && k in track.y && donnees.k_max > 0 && k2 in track.x && k2 in track.y && donnees.k_max > 0) {
                x = (container_w - ttl - track_w - 17) / 2 + (-track.x[k] + track_max_x) * track_mult;
                y = (container_h - track_h) / 2 + (-track.y[k] + track_max_y) * track_mult;
                x2 = (container_w - ttl - track_w - 17) / 2 + (-track.x[k2] + track_max_x) * track_mult;
                y2 = (container_h - track_h) / 2 + (-track.y[k2] + track_max_y) * track_mult;
                trackmap_fond_context.moveTo(x, y);
                trackmap_fond_context.lineTo(x2, y2);
            }
            trackmap_fond_context.stroke(); //On trace seulement les lignes.
            trackmap_fond_context.closePath();
        }*/
        //





    }
}


// Affiche la direction du nord
function draw_north(north) {
    if (trackmap_disp_north) {
        if (north != 99) {  // si c'est 99 �a veut dire qu'il n'est pas d�fini
            north = north + angle - Math.PI/2;
            x0 = container_w - ttl - 17 - track_maxlength / 20;
            y0 = track_maxlength / 20;

            dx = 0 * Math.cos(north) - (-1) * Math.sin(north);
            dy = 0 * Math.sin(north) + (-1) * Math.cos(north);

            rayon = track_maxlength / 25 * 1.75;

            trackmap_fond_context.globalAlpha = 1;

            x1 = x0 - dx * rayon * 0.3;
            y1 = y0 - dy * rayon * 0.3;
            x2 = x0 + dx * rayon * 0.5;
            y2 = y0 + dy * rayon * 0.5;
            x3 = x0 - dx * rayon * 0.5 - dy * rayon * 0.35;
            y3 = y0 - dy * rayon * 0.5 + dx * rayon * 0.35;
            x4 = x0 - dx * rayon * 0.5 + dy * rayon * 0.35;
            y4 = y0 - dy * rayon * 0.5 - dx * rayon * 0.35;
            trackmap_fond_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_fond_context.fillStyle = "#ff0000";
            trackmap_fond_context.moveTo(x1, y1);//On se d�place au coin inf�rieur gauche
            trackmap_fond_context.lineTo(x4, y4);
            trackmap_fond_context.lineTo(x2, y2);
            trackmap_fond_context.lineTo(x3, y3);
            trackmap_fond_context.fill(); //On trace seulement les lignes.
            trackmap_fond_context.closePath();

            // On enl�ve l'ombrage
            trackmap_fond_context.shadowOffsetX = 0;
            trackmap_fond_context.shadowOffsetY = 0;
            trackmap_fond_context.shadowBlur = 0;
            trackmap_fond_context.inset = 1;
            x1 = x0 - dx * rayon * 0.2;
            y1 = y0 - dy * rayon * 0.2;
            x2 = x0 + dx * rayon * 0.35;
            y2 = y0 + dy * rayon * 0.35;
            x4 = x0 - dx * rayon * 0.35 + dy * rayon * 0.25;
            y4 = y0 - dy * rayon * 0.4 - dx * rayon * 0.25;
            trackmap_fond_context.beginPath(); //On d�marre un nouveau trac�.
            trackmap_fond_context.fillStyle = "#000000";
            trackmap_fond_context.globalCompositeOperation = "destination-out";
            trackmap_fond_context.moveTo(x1, y1);//On se d�place au coin inf�rieur gauche
            trackmap_fond_context.lineTo(x4, y4);
            trackmap_fond_context.lineTo(x2, y2);
            trackmap_fond_context.fill(); //On trace seulement les lignes.
            trackmap_fond_context.closePath();

            trackmap_fond_context.globalCompositeOperation = "source-over";

            /*x1 = x0 - dx * rayon * 0.3;
             y1 = y0 - dy * rayon * 0.3;
             x2 = x0 + dx * rayon * 0.5;
             y2 = y0 + dy * rayon * 0.5;
             x3 = x0 - dx * rayon * 0.5 + dy * rayon * 0.35;
             y3 = y0 - dy * rayon * 0.5 - dx * rayon * 0.35;
             trackmap_fond_context.beginPath(); //On d�marre un nouveau trac�.
             trackmap_fond_context.fillStyle = "#ff0000";
             trackmap_fond_context.moveTo(x1, y1);//On se d�place au coin inf�rieur gauche
             trackmap_fond_context.lineTo(x2, y2);
             trackmap_fond_context.lineTo(x3, y3);
             trackmap_fond_context.fill(); //On trace seulement les lignes.
             trackmap_fond_context.closePath();*/

            // On remet l'Ombrage
            trackmap_fond_context.shadowColor = "black";
            trackmap_fond_context.shadowOffsetX = rayon / 6;
            trackmap_fond_context.shadowOffsetY = rayon / 6;
            trackmap_fond_context.shadowBlur = rayon / 2;
        }
    }
}


// Affiche la direction du nord dans le mode edition
function draw_north_edit(north) {
    north = north + angle;
    x0 = container_w - ttl - 17 - track_maxlength / 20;
    y0 = track_maxlength / 20;

    dx = 0 * Math.cos(north) - (-1) * Math.sin(north);
    dy = 0 * Math.sin(north) + (-1) * Math.cos(north);

    rayon = track_maxlength / 25 * 1.75;

    trackmap_context.globalAlpha = 1;

    x1 = x0 - dx * rayon * 0.3;
    y1 = y0 - dy * rayon * 0.3;
    x2 = x0 + dx * rayon * 0.5;
    y2 = y0 + dy * rayon * 0.5;
    x3 = x0 - dx * rayon * 0.5 - dy * rayon * 0.35;
    y3 = y0 - dy * rayon * 0.5 + dx * rayon * 0.35;
    trackmap_context.beginPath(); //On d�marre un nouveau trac�.
    trackmap_context.fillStyle = "#444444";
    trackmap_context.moveTo(x1, y1);//On se d�place au coin inf�rieur gauche
    trackmap_context.lineTo(x2, y2);
    trackmap_context.lineTo(x3, y3);
    trackmap_context.fill(); //On trace seulement les lignes.
    trackmap_context.closePath();

    x1 = x0 - dx * rayon * 0.3;
    y1 = y0 - dy * rayon * 0.3;
    x2 = x0 + dx * rayon * 0.5;
    y2 = y0 + dy * rayon * 0.5;
    x3 = x0 - dx * rayon * 0.5 + dy * rayon * 0.35;
    y3 = y0 - dy * rayon * 0.5 - dx * rayon * 0.35;
    trackmap_context.beginPath(); //On d�marre un nouveau trac�.
    trackmap_context.fillStyle = "#ffffff";
    trackmap_context.moveTo(x1, y1);//On se d�place au coin inf�rieur gauche
    trackmap_context.lineTo(x2, y2);
    trackmap_context.lineTo(x3, y3);
    trackmap_context.fill(); //On trace seulement les lignes.
    trackmap_context.closePath();
}


// Affiche une fl�che indiquant le sens du circuit
function draw_fleche() {
    k = 0;
    k2 = 1;
    x1 = (-track.x[k] + track_max_x) * track_mult;
    y1 = (-track.y[k] + track_max_y) * track_mult;
    x2 = (-track.x[k2] + track_max_x) * track_mult;
    y2 = (-track.y[k2] + track_max_y) * track_mult;
    x0 = (container_w - ttl - track_w - 17) / 2 + x1;
    y0 = (container_h - track_h) / 2 + y1;

    rayon = track_maxlength / 75;

    // On d�cale le nom du virage � gauche ou � droite
    l = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    if (l != 0) {
        dx = (x2 - x1) / l;
        dy = (y2 - y1) / l;
        side = -1;
        decale_x = -dy * (rayon + track_epaisseur * 1.25) * side/2;
        decale_y = dx * (rayon + track_epaisseur * 1.25) * side/2;

        x0 = x0 + decale_x * 1.125*2;
        y0 = y0 + decale_y * 1.125*2;

        x1 = x0 - decale_x * 0.25;
        y1 = y0 - decale_y * 0.25;
        x2 = x1 + dx * rayon * 2;
        y2 = y1 + dy * rayon * 2;
        x3 = x2 - decale_x * 0.35;
        y3 = y2 - decale_y * 0.35;
        x4 = x0 + dx * rayon * 3.2;
        y4 = y0 + dy * rayon * 3.2;
        x5 = x2 + decale_x * 0.85;
        y5 = y2 + decale_y * 0.85;
        x6 = x5 - decale_x * 0.35;
        y6 = y5 - decale_y * 0.35;
        x7 = x0 + decale_x * 0.25;
        y7 = y0 + decale_y * 0.25;

        trackmap_context.globalAlpha = 1;

        trackmap_fond_context.beginPath(); //On d�marre un nouveau trac�.
        trackmap_fond_context.fillStyle = "#ff0000";
        trackmap_fond_context.moveTo(x1, y1);//On se d�place au coin inf�rieur gauche
        trackmap_fond_context.lineTo(x2, y2);
        trackmap_fond_context.lineTo(x3, y3);
        trackmap_fond_context.lineTo(x4, y4);
        trackmap_fond_context.lineTo(x5, y5);
        trackmap_fond_context.lineTo(x6, y6);
        trackmap_fond_context.lineTo(x7, y7);
        trackmap_fond_context.fill(); //On trace seulement les lignes.
        trackmap_fond_context.closePath();
    }

}


function draw_turn(coul, turn, ldp, side,info) {
    k = Math.floor(donnees.coef_k * ldp);
    d = donnees.coef_k * ldp;
    if (k >= donnees.k_max) k = 0;
    k2 = k + 1;
    if (k2 >= donnees.k_max) k2 = 0;
    x1 = (-track.x[k] + track_max_x) * track_mult;
    y1 = (-track.y[k] + track_max_y) * track_mult;
    x2 = (-track.x[k2] + track_max_x) * track_mult;
    y2 = (-track.y[k2] + track_max_y) * track_mult;

    // On fait une interpolation
    x = (container_w - ttl - track_w - 17) / 2 + x1 + (x2 - x1) * (d - k);
    y = (container_h - track_h) / 2 + y1 + (y2 - y1) * (d - k);

    rayon = track_maxlength / 75;

    // On d�cale le nom du virage � gauche ou � droite
    l = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    if (l != 0) {
        decale_x = -(y2 - y1) / l * (rayon + track_epaisseur * 1.25) * side;
        decale_y = (x2 - x1) / l * (rayon + track_epaisseur * 1.25) * side;
        x += decale_x;
        y += decale_y;

        //trackmap_context.globalAlpha = 1;

        trackmap_fond_turns_context.fillStyle = coul;
        trackmap_fond_turns_context.strokeStyle = coul;
        trackmap_fond_turns_context.font = "bold " + 2 * rayon + "px Arial";
        trackmap_fond_turns_context.textAlign = "center";
        trackmap_fond_turns_context.fillText(turn, x, y + rayon / 1.5);

        trackmap_fond_turns_context.fillStyle = coul;
        trackmap_fond_turns_context.font = "bold " + 1 * rayon + "px Arial";
        trackmap_fond_turns_context.textAlign = "left";
        trackmap_fond_turns_context.fillText(" " + info, x + (turn.length / 2 + 0.5) * 2 * rayon / 2, y + rayon / 1.52);
    }
}


function draw_turn_edit(coul, turn, ldp, side,info) {
    if (trackmap_loaded) {
        k = Math.floor(donnees.coef_k * ldp);
        d = donnees.coef_k * ldp;
        if (k >= donnees.k_max) k = 0;
        k2 = k + 1;
        if (k2 >= donnees.k_max) k2 = 0;
        x1 = (-track.x[k] + track_max_x) * track_mult;
        y1 = (-track.y[k] + track_max_y) * track_mult;
        x2 = (-track.x[k2] + track_max_x) * track_mult;
        y2 = (-track.y[k2] + track_max_y) * track_mult;

        // On fait une interpolation
        x = (container_w - ttl - track_w - 17) / 2 + x1 + (x2 - x1) * (d - k);
        y = (container_h - track_h) / 2 + y1 + (y2 - y1) * (d - k);

        rayon = track_maxlength / 75;

        // On d�cale le nom du virage � gauche ou � droite
        l = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        if (l != 0) {
            decale_x = -(y2 - y1) / l * (rayon + track_epaisseur * 1.25) * side;
            decale_y = (x2 - x1) / l * (rayon + track_epaisseur * 1.25) * side;
            x += decale_x;
            y += decale_y;

            trackmap_context.globalAlpha = 1;

            // Ombrage du texte
            trackmap_context.shadowColor = "black";
            trackmap_context.shadowOffsetX = rayon / 6;
            trackmap_context.shadowOffsetY = rayon / 6;
            trackmap_context.shadowBlur = rayon / 2;

            trackmap_context.fillStyle = coul;
            trackmap_context.strokeStyle = coul;
            trackmap_context.font = "bold " + 2 * rayon + "px Arial";
            trackmap_context.textAlign = "center";
            trackmap_context.fillText(turn, x, y + rayon / 1.5);

            trackmap_context.fillStyle = coul;
            trackmap_context.font = "bold " + 1 * rayon + "px Arial";
            trackmap_context.textAlign = "left";
            trackmap_context.fillText(" " + info, x + (turn.length / 2 + 0.5) * 2 * rayon / 2, y + rayon / 1.52);

            // On enl�ve l'ombrage
            trackmap_context.shadowOffsetX = 0;
            trackmap_context.shadowOffsetY = 0;
            trackmap_context.shadowBlur = 0;
        }
    }
}


function optimize_track_angle() {
    var w = 0;
    var h = 0;
    track = {x: [], y: []};
    angle_optimized = 0;
    track_mult = 0;

    for (angle = Math.PI / 2 ; angle > -Math.PI/800 - Math.PI / 2 ; angle -= Math.PI / 400) {

        // On fait une rotation et on cherche les valeurs min et max
        for (var k = 0; k < donnees.k_max; k++) {
            track.x[k] = donnees.x[k] * Math.cos(angle) - donnees.y[k] * Math.sin(angle);
            track.y[k] = donnees.x[k] * Math.sin(angle) + donnees.y[k] * Math.cos(angle);
            if (k == 0) {
                track_max_x = track.x[0];
                track_min_x = track.x[0];
                track_max_y = track.y[0];
                track_min_y = track.y[0]
            } else {
                if (track.x[k] < track_min_x)
                    track_min_x = track.x[k];
                if (track.y[k] < track_min_y)
                    track_min_y = track.y[k];
                if (track.x[k] > track_max_x)
                    track_max_x = track.x[k];
                if (track.y[k] > track_max_y)
                    track_max_y = track.y[k]
            }
        }

        w = track_max_x - track_min_x;
        h = track_max_y - track_min_y;
        if (w == 0 || h == 0) {
            w = 1;
            h = 1
        }
        mult = cv_w / w;
        mult_h = cv_h / h;
        if (mult_h < mult) mult = mult_h;

        if (mult * 0.98 > track_mult) {
            track_mult = mult;
            angle_optimized = angle
        }
    }

    return angle_optimized
}


// Affiche la direction du vent et les infos m�t�o
function draw_winddir(north, winddir) {

    var w = parseInt($("#trackmap_canvas").css("width"));
    var h = parseInt($("#trackmap_canvas").css("height"));
    //rayon2 = track_maxlength / 75;
    rayon2 = w / 102;

    trackmap_context.globalAlpha = 1;

    if (north != 99 && trackmap_disp_wind) {  // si c'est 99 �a veut dire qu'il n'est pas d�fini
        winddir = angle + north + winddir + Math.PI - Math.PI/2;
        //x0 = track_maxlength / 20;
        //y0 = container_h - track_maxlength / 20;
        x0 = w / 30;
        y0 = h - w / 30;

        dx = 0 * Math.cos(winddir) - (-1) * Math.sin(winddir);
        dy = 0 * Math.sin(winddir) + (-1) * Math.cos(winddir);

        //rayon = track_maxlength / 25 * 1.75;
        rayon = w / 30 * 1.5;

        // On remet l'Ombrage
        /*trackmap_context.shadowColor = "white";
         trackmap_context.shadowOffsetX = rayon2 / 6;
         trackmap_context.shadowOffsetY = rayon2 / 6;
         trackmap_context.shadowBlur = rayon2 / 2;*/

        x1 = x0 - dx * rayon * 0.3;
        y1 = y0 - dy * rayon * 0.3;
        x2 = x0 + dx * rayon * 0.5;
        y2 = y0 + dy * rayon * 0.5;
        x3 = x0 - dx * rayon * 0.5 - dy * rayon * 0.35;
        y3 = y0 - dy * rayon * 0.5 + dx * rayon * 0.35;
        x4 = x0 - dx * rayon * 0.5 + dy * rayon * 0.35;
        y4 = y0 - dy * rayon * 0.5 - dx * rayon * 0.35;
        trackmap_context.beginPath(); //On d�marre un nouveau trac�.
        trackmap_context.fillStyle = "#0077ff";
        trackmap_context.moveTo(x1, y1);//On se d�place au coin inf�rieur gauche
        trackmap_context.lineTo(x4, y4);
        trackmap_context.lineTo(x2, y2);
        trackmap_context.lineTo(x3, y3);
        trackmap_context.fill(); //On trace seulement les lignes.
        trackmap_context.closePath();

        // On enl�ve l'ombrage
        trackmap_context.shadowOffsetX = 0;
        trackmap_context.shadowOffsetY = 0;
        trackmap_context.shadowBlur = 0;
        trackmap_context.inset = 1;

        x1 = x0 - dx * rayon * 0.2;
        y1 = y0 - dy * rayon * 0.2;
        x2 = x0 + dx * rayon * 0.35;
        y2 = y0 + dy * rayon * 0.35;
        x4 = x0 - dx * rayon * 0.35 + dy * rayon * 0.25;
        y4 = y0 - dy * rayon * 0.4 - dx * rayon * 0.25;
        trackmap_context.beginPath(); //On d�marre un nouveau trac�.
        trackmap_context.fillStyle = "#000000";
        trackmap_context.globalCompositeOperation = "destination-out";
        trackmap_context.moveTo(x1, y1);//On se d�place au coin inf�rieur gauche
        trackmap_context.lineTo(x4, y4);
        trackmap_context.lineTo(x2, y2);
        trackmap_context.fill(); //On trace seulement les lignes.
        trackmap_context.closePath();

        trackmap_context.globalCompositeOperation = "source-over";
    }

    // On remet l'Ombrage
    /*trackmap_context.shadowColor = "white";
     trackmap_context.shadowOffsetX = rayon2 / 6;
     trackmap_context.shadowOffsetY = rayon2 / 6;
     trackmap_context.shadowBlur = rayon2 / 2;*/

    trackmap_context.fillStyle = "#0077ff";
    trackmap_context.font = "bold " + 1.7 * rayon2 + "px Arial";
    trackmap_context.textAlign = "left";

    weather = reformat_skies(donnees.skies);

    if (donnees.u == 1) {
        str_speed = (donnees.windspeed * 3.6).toFixed(1) + " km/h";
    } else {
        str_speed = (donnees.windspeed * 3.6 / 1.609344).toFixed(1) + " MPH";
    }
    if ((temperature_mode != 2) && ((donnees.u == 1 && temperature_mode == 0) || (temperature_mode == 1))) {  // systeme metric ou forc� en Celsius dans les options
        weather += " " + donnees.airtemp.toFixed(1) + String.fromCharCode(176) + "C";
        weather += ", track " + donnees.tracktemp.toFixed(1) + String.fromCharCode(176) + "C";
        if (north == 99)  // si c'est 99 �a veut dire qu'il n'est pas d�fini
            weather += ", Winds " + (donnees.winddir / Math.PI * 180).toFixed(0) + String.fromCharCode(176) + " " + reformat_winddir(donnees.winddir / Math.PI * 180) + " " + (donnees.windspeed * 3.6).toFixed(1) + " km/h";
        else
            weather += ", Winds " + reformat_winddir(donnees.winddir / Math.PI * 180) + " " + str_speed;
    } else {
        weather += " " + (donnees.airtemp * 1.8 + 32).toFixed(1) + String.fromCharCode(176) + "F";
        weather += ", track " + (donnees.tracktemp * 1.8 + 32).toFixed(1) + String.fromCharCode(176) + "F";
        if (north == 99)  // si c'est 99 �a veut dire qu'il n'est pas d�fini
            weather += ", Winds " + (donnees.winddir / Math.PI * 180).toFixed(0) + String.fromCharCode(176) + " " + reformat_winddir(donnees.winddir / Math.PI * 180) + " " + (donnees.windspeed * 3.6 / 1.609344).toFixed(1) + " MPH";
        else
            weather += ", Winds " + reformat_winddir(donnees.winddir / Math.PI * 180) + " " + str_speed;
    }
    weather += ", RH " + (donnees.humidity * 100).toFixed(0) + "%";
    weather += ", Fog " + (donnees.fog * 100).toFixed(0) + "%";

    if (trackmap_disp_weather_infos) {
        //trackmap_context.fillText(weather, track_maxlength / 10, container_h - 1.5* rayon2);
        if (north == 99)  // si c'est 99 �a veut dire qu'il n'est pas d�fini
            trackmap_context.fillText(weather, w / 75, h - 1.5 * rayon2);
        else
            trackmap_context.fillText(weather, w / 13, h - 1.5 * rayon2);
    }

    // On enl�ve l'ombrage
    trackmap_context.shadowColor = "black";
    trackmap_context.shadowOffsetX = 0;
    trackmap_context.shadowOffsetY = 0;
    trackmap_context.shadowBlur = 0;
    trackmap_context.inset = 1;
}


// On initialize les couleurs
function init_colorize() {
    if (colorize_drivers_init == 3) {
        var nom;
        if (donnees.teamracing) {
            colorize_ = colorize_team_;
        } else {
            colorize_ = colorize_driver_;
        }
        for (var i = 0; i < 64; i++) {
            if (i in donnees.d) {
                nom = donnees.d[i].name;
                id = donnees.d[i].uid;
                if (donnees.teamracing) {
                    nom = donnees.d[i].tn;
                    id = donnees.d[i].tid;
                }
            }
        }
        colorize_drivers_init = 0;
    }
}
