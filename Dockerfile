FROM php:7.3-apache

LABEL maintainer="Markus.Papenbrock@gmail.com"

ENV JRT_PASSWORD 1234

COPY files/jrt/ /var/www/html/
RUN chown www-data:www-data /var/www/html/datajson \
    && chown www-data:www-data /var/www/html/img
